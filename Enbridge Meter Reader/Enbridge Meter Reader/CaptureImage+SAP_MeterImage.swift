//
//  CaptureImage+SAP_MeterImage.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-04-04.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import SAPFoundation
import SAPOData
import SAPFiori
import SAPCommon
import proxyclasses

class CaptureImage_SAP_MeterImage {
    
    static let sharedInstance = CaptureImage_SAP_MeterImage()
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private var zdmmeterpicsrvEntities: I1<OnlineODataProvider> {
        return self.appDelegate.zdmmeterpicsrvEntities
    }
    
    private var validity = [String: Bool]()
    private var _entity: I1Files?
    var entity: I1Files {
        get {
            return self._entity ?? I1Files()
        }
        set {
            self._entity = newValue
        }
    }
    static var entityCollection = [I1Files]()
    
    private let logger = Logger.shared(named: "I1FilesetMasterViewControllerLogger")
    
    func createImageEntity(_ entity:I1Files) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        LogHelper.shared?.debug("Creating entity in backend",loggerObject: self.logger)
        
        
        let odataProvider = OnlineODataProvider(serviceName: "I1ZDMMETERPICSRVEntities", serviceRoot: URL(string: "\(environmentURL)/\(imageDestination)")!, sapURLSession: AppDelegate.onboardingContext.sapURLSession)
        // Disables version validation of the backend OData service
        // TODO: Should only be used in demo and test applications
        odataProvider.serviceOptions.checkVersion = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.zdmmeterpicsrvEntities = I1(provider: odataProvider)
        // To update entity force to use X-HTTP-Method header
        self.zdmmeterpicsrvEntities.provider.networkOptions.tunneledMethods.append("MERGE")
        //NOTE Swtich to offline now
        
        //Check if this image exists in the local store yet
        let imageSP = LocalStoreImage.returnSP()
        //let imageLS = LocalStoreImage.offlineLS
        _ = LocalStoreImage.offlineLS

        var optSet:[I1Files]? = nil
        do {
            let optEntity = imageSP.entitySet(withName: "FILESET")
            let query = DataQuery().selectAll().filter(I1Files.filename.equal(self.entity.filename!)).from(optEntity)
            optSet = I1Files.array(from: try imageSP.executeQuery(query).entityList())
        }catch {
            //print(error)
            return
        }
        if let set = optSet {
            if set.count > 0 {
                //print("This file exists")
                let file = set.first
                file?.value = self.entity.value
                imageSP.updateEntity(file!) { error in
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if let error = error {
                        self.logger.error("Create entry failed. Error: \(error)", error: error)
                        let alertController = UIAlertController(title: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), message: error.localizedDescription, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .default))
                        OperationQueue.main.addOperation({
                            // Present the alertController
                        })
                        return
                    }
                    self.logger.info("Create entry finished successfully.")
//                    imageLS?.uploadToDB()
                    //imageLS?.downloadFromDB() no need download
                    OperationQueue.main.addOperation({
                        FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    })
                }
            }else{
                let headers = HTTPHeaders()
                headers.setHeader(withName:"OfflineOData.RemoveAfterUpload", value:"true" )
                imageSP.createEntity(self.entity,headers:headers) { error in
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if let error = error {
                        self.logger.error("Create entry failed. Error: \(error)", error: error)
                        let alertController = UIAlertController(title: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), message: error.localizedDescription, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .default))
                        OperationQueue.main.addOperation({
                            // Present the alertController
                        })
                        return
                    }
                    self.logger.info("Create entry finished successfully.")
                    //imageLS?.uploadToDB()
                    //imageLS?.downloadFromDB()
                    OperationQueue.main.addOperation({
                        FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    })
                }
            }
        }
    }
    
    func createEntity(fileName:String?,picreason:String?,zindicator:String?,value:String?) -> I1Files {
        let newEntity = I1Files()
        // Fill the mandatory properties with default values
        if let name = fileName, let reason = picreason, let indicator = zindicator, let imageValue = value {
//            newEntity.filename = CellCreationHelper.defaultValueFor(I1Files.filename)
            newEntity.filename = "\(name)_\(String(describing: picreason!)).jpg"
            newEntity.picreason = reason
            newEntity.zindicator = indicator
            newEntity.value = imageValue
        }
        self.entity = newEntity
        
        // Key properties without default value should be invalid by default for Create scenario
        if newEntity.filename == nil || newEntity.filename!.isEmpty {
            self.validity["FILENAME"] = false
        }
        return newEntity
    }
    
    func shouldPerformCreateEntity(){
        self.createImageEntity(self.entity)
    }
    
    init(){
        logger.logLevel = .debug
    }
    
}
