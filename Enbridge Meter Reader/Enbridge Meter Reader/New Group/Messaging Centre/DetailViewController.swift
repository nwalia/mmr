//
//  DetailViewController.swift
//  Enbridge Meter Reader
//
//  Created by Abramsky, Lauren (CA - Toronto) on 2018-01-10.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var messageFrom: UILabel!
    @IBOutlet weak var messageTo: UILabel!
    @IBOutlet weak var messageSubject: UILabel!
    @IBOutlet weak var messageDateTime: UILabel!
    @IBOutlet weak var messageBody: UILabel!
    var message: Message!
    
    var messageTime: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.tintColor = UIColor(red:1, green:0.72, blue:0.11, alpha:1)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
                
        messageFrom.text = message.from
        messageSubject.text = message.subject
        messageDateTime.text = "\(message.date) at \(message.time)"
        messageBody.text = message.body
        messageTo.text = Utilities.sharedInstance.userName ?? ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
