//
//  SummaryLocationInformationCell.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-11.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit

protocol SummaryLocationInformationCellDelegate {
    func didPressReturnButton(forMeterRead read: MeterRead?)
}

class SummaryLocationInformationCell: UITableViewCell {

    @IBOutlet weak var meterNumberLabel: UILabel!
    @IBOutlet weak var mruLabel: UILabel!
    @IBOutlet weak var meterLocationCodeLabel: UILabel!
    @IBOutlet weak var estimationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLine1Label: UILabel!
    @IBOutlet weak var addressLine2Label: UILabel!
    @IBOutlet weak var keyStackView: UIStackView!
    @IBOutlet weak var specialInstructionStackView: UIStackView!
    @IBOutlet weak var returnButton: UIButton!
    
    var read: MeterRead?
    var delegate: SummaryLocationInformationCellDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func returnButtonPressed(_ sender: UIButton) {
        delegate?.didPressReturnButton(forMeterRead: read)
    }
}
