//
//  LocalStorePeriodic.swift
//  Enbridge Meter Reader
//
//  Created by Jerry on 2018-06-08.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import SAPFoundation
import SAPOfflineOData
import SAPCommon
import SAPOData
import proxyclasses

class LocalStorePeriodic: LocalStoreMeterInfoProtocol,OfflineODataDelegate {
    
    private var offlineSP: M1Ec1<OfflineODataProvider>? = nil
    private let logger = Logger.shared(named: "Local Store Periodic")
    private var isStoreOpened : Bool
    //public var downloaded: Bool
    
    init(urlSession: SAPURLSession) {
        
        var offlineParameters = OfflineODataParameters()
        offlineParameters.enableRepeatableRequests = true
        isStoreOpened = false;
        //downloaded = false
        
        offlineParameters.customHeaders = [
            "MMR_EngineerID" : Utilities.sharedInstance.userName ?? "",
            //"TIME_INTERVAL_START" : /*"2018-04-21T00:00:00"*/ Utilities.sharedInstance.fetchCurrentSystemDate().0,
            //"TIME_INTERVAL_END" :  /*"2018-04-30T00:00:00"*/Utilities.sharedInstance.fetchCurrentSystemDate().1,
            "MMR_OffCycle" : "false",
            "MMR_Periodic" : "true",
            "MMR_OutCard" : "false"
        ] 
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let completePath = path.appending("/" + Utilities.sharedInstance.userName!)
        offlineParameters.storePath =  NSURL.fileURL(withPath: completePath)
        
        offlineParameters.storeName = "Periodic"
        // create offline OData provider
        let offlineODataProvider = try! OfflineODataProvider(
            serviceRoot: URL(string: "\(environmentURL)/\(clickDestination)")!,
            parameters: offlineParameters,
            sapURLSession: urlSession,
            delegate: self
        )
        
        try! offlineODataProvider.add(
            definingQuery: OfflineODataDefiningQuery(
                name: CollectionType.operationSet.rawValue,
                query: "/\(CollectionType.operationSet.rawValue)",
                automaticallyRetrievesStreams: false
            )
        )
        
        self.offlineSP = M1Ec1(provider: offlineODataProvider) // For Read Meter

    }
    
    func destory () {
        offlineSP = nil
    }
    
    func returnSP() -> M1Ec1<OfflineODataProvider> {
        return self.offlineSP!
    }
    
    func download(completionHandler: @escaping (Error?) -> Void = {error in}){
        /* Read Meter Info Offline */
        self.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                LogHelper.shared?.error("Offline open failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                completionHandler(error)
                return
            }
            
            self.isStoreOpened = true
            
            self.offlineSP?.download { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline periodic download failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    completionHandler(error)
                    return
                }
                LogHelper.shared?.info("Offline periodic download ok", loggerObject: self.logger)
                completionHandler(nil)
            }
        }
    }
    
    func upload(completionHandler: @escaping (Error?) -> Void) {
        
        /* Read Meter Info Offline */
        self.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                LogHelper.shared?.error("Offline open failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                completionHandler(error)
                return
            }
            
            self.isStoreOpened = true
            
            self.reTouchRequestEntity()
            
            self.offlineSP?.upload { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline upload failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    completionHandler(error)
                    return
                }
                LogHelper.shared?.info("Offline Periodic upload ok", loggerObject: self.logger)
                self.clearErrorArchive()
                completionHandler(nil)
            }
        }
    }
    
    public func reTouchEvent() {
        do {
            let query = DataQuery().filter(OfflineODataQueryFunction.inErrorState())
            let eventsInError = try self.offlineSP?.fetchOperationSet(matching: query)
                       
            if let events = eventsInError {
                let options = RequestOptions()
                options.sendEmptyUpdate = true
                for event in events {
                    try self.offlineSP?.updateEntity(event, headers: HTTPHeaders.empty, options: options)
                }
            }
        }catch {
            print(error)
        }
        
    }
    
    public func clearErrorArchive() {
        do {
            let ea = self.offlineSP?.entitySet(withName: "ErrorArchive")
            let query = DataQuery().selectAll().from(ea!)
            let errors = try self.offlineSP?.executeQuery(query).entityList()
            if let errorArray =  errors?.toArray() {
                if errorArray.count > 0 {
                    print(errorArray.first ?? "nothing")
                    try self.offlineSP?.deleteEntity(errorArray.first!)
                }
            }
        }catch {
            print (error)
        }
        
    }
    
    public func reTouchRequestEntity(){
        do {
            if let sp = self.offlineSP {
                let errorArchiveSet = sp.entitySet(withName: "ErrorArchive")
                let errorArchiveType: EntityType = errorArchiveSet.entityType
                let requestEntityProp = errorArchiveType.property(withName: "RequestEntity")
                let errorArchiveQuery = DataQuery().from(errorArchiveSet)
                let errors = try sp.executeQuery(errorArchiveQuery).entityList()
                
                for error in errors {
                    try sp.loadProperty(requestEntityProp, into: error)
                    let entity = requestEntityProp.entityValue(from: error)
                    try sp.updateEntity(entity)
                }
                
            }
            
        }catch {
            print (error)
        }
        
    }
    
    public func deleteLocalStore (){
        do {
            try self.offlineSP?.clear()
            self.offlineSP = nil
        }catch let er{
            print ("Failed to delete submitted periodic route : \(er)")
        }
    }
    
    public func closeOfflineStore() {
        if isStoreOpened {
            do {
                try self.offlineSP?.close()
                isStoreOpened = false
            } catch {
                logger.error("Offline Store closing failed")
            }
        }
    }
    
    public func openOfflineStore(){
        if !isStoreOpened {
            self.offlineSP?.open{error in
                guard error == nil else {
                    //print(error.debugDescription)
                    self.logger.error("Offline failed to open")
                    return
                }}
            isStoreOpened = true
        }
    }
    
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateDownloadProgress progress: OfflineODataProgress) {
        ////print("in did update donwload progress");
        LocalStoreManager.dataBytes =  LocalStoreManager.dataBytes + progress.bytesReceived
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateFileDownloadProgress progress: OfflineODataFileDownloadProgress) {
        ////print("in did update file download progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateUploadProgress progress: OfflineODataProgress) {
        //        //print("in did update upload progress");
        LocalStoreManager.dataBytes =  LocalStoreManager.dataBytes + progress.bytesReceived
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, requestDidFail request: OfflineODataFailedRequest) {
        //        //print("in did request fail");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, stateDidChange newState: OfflineODataStoreState) {
        //        //print("in state did change");
        if newState.contains(.opening) {
            LocalStoreManager.status = "Preparing the Ipad data"
        }else if newState.contains(.initializing){
            LocalStoreManager.status = "Initializing the Ipad data"
        }else if newState.contains(.initialCommunication){
            LocalStoreManager.status = "Initializing the communication"
        }else if newState.contains(.fileDownloading){
            LocalStoreManager.status = "Downloading files"
        }else if newState.contains(.downloading){
            LocalStoreManager.status = "Downloading data"
        }else if newState.contains(.uploading){
            LocalStoreManager.status = "Uploading data"
        }else if newState.contains(.open){
            LocalStoreManager.status = "Ipad database is opened"
        }else if newState.contains(.closed){
            LocalStoreManager.status = "Ipad database is closed"
        }
    }
    
}
