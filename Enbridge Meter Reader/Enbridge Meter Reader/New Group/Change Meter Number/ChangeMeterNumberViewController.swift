//
//  MeterChangeViewController.swift
//  Enbridge Meter Reader
//
//  Created by Johann Wentzel on 2017-11-29.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import Foundation
import UIKit

class ChangeMeterNumberViewController: UIViewController {
    
    @IBOutlet weak var capturePhotoButton: UIButton!
    @IBOutlet weak var submitMeterChangeButton: UIButton!
    @IBOutlet weak var newMeterNumberField: ValidatedTextField!
    @IBOutlet weak var meterSizeCodeField: ValidatedTextField!
    @IBOutlet weak var numberOfDialsField: ValidatedTextField!
    @IBOutlet weak var meterNumberLabel: UILabel!
    @IBOutlet weak var addressLine1Label: UILabel!
    @IBOutlet weak var addressLine2Label: UILabel!
    
    var meterNumberValid: Bool!
    var meterSizeCodeValid: Bool!
    var numberOfDialsValid: Bool!
    var submitButtonEnabled: Bool!
    
    var meterImage: UIImage?
    var selectedMeter: MeterRead?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        submitMeterChangeButton.layer.cornerRadius = 10
        submitMeterChangeButton.layer.masksToBounds = true
        capturePhotoButton.layer.cornerRadius = 10
        capturePhotoButton.layer.masksToBounds = true
        
        meterNumberValid = false
        meterSizeCodeValid = false
        numberOfDialsValid = false
        submitButtonEnabled = false
        
        submitMeterChangeButton.setTitleColor(UIColor(red:0.2, green:0.2, blue:0.2, alpha:1), for: .normal)
        submitMeterChangeButton.setTitleColor(UIColor.gray, for: .disabled)
        
        newMeterNumberField.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        meterSizeCodeField.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        numberOfDialsField.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        
        checkAllValid()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // If there was a meter passed with a segue, populate the text fields.
        if (selectedMeter != nil) {
            if let newMeterNumber = selectedMeter?.newMeterNumber, newMeterNumber != "" {
                meterNumberLabel.text = "\((selectedMeter?.newSizeCode)!)\((newMeterNumber))"
            } else {
                meterNumberLabel.text = "\((selectedMeter?.sizeCode)!)\((selectedMeter?.meterNumber)!)"
            }
            addressLine1Label.text = selectedMeter?.fullAddress
            addressLine2Label.text = selectedMeter?.addressLine2
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if meterImage != nil {
            capturePhotoButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
        }
        
        checkAllValid()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
   @objc func checkAllValid(){
        // Check if all the fields are valid. If so, enable the submit button.
        if newMeterNumberField.getValidity()
            && meterSizeCodeField.getValidity()
            && numberOfDialsField.getValidity()
            && meterImage != nil {
            
            if let numberOfDialsFieldtext = Int(numberOfDialsField.text ?? "0") {
                if (4...6).contains(numberOfDialsFieldtext as Int) {
                // enable button
                if !(submitButtonEnabled){
                    submitButtonEnabled = true
                    submitMeterChangeButton.isEnabled = true
                    submitMeterChangeButton.backgroundColor = GlobalConstants.YELLOW
                }
            }else {
                let alertController = UIAlertController(title:"Invalid Input", message: "Number of dials should be between 4-6", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default))
                OperationQueue.main.addOperation({
                    // Present the alertController
                    self.present(alertController, animated: true, completion: nil)
                })
            }
            }
            
        }
        else {
            submitButtonEnabled = false
            submitMeterChangeButton.isEnabled = false
            submitMeterChangeButton.backgroundColor = GlobalConstants.LIGHT_YELLOW
        }
        
    }
    @IBAction func didSubmitMeterChange(_ sender: UIButton) {
        CaptureImage_SAP_MeterImage.sharedInstance.shouldPerformCreateEntity()
        LocalStoreImage.offlineLS?.uploadToDB()
    }
    
    @IBAction func capturePhotoButtonClicked(_ sender: UIButton) {
        meterImage = nil
        CameraCaptureViewController.picreason = "MTC"
        CameraCaptureViewController.zindicator = self.selectedMeter?.checkForWorkOrderType(self.selectedMeter ?? MeterRead())
        CameraCaptureViewController.fileName = self.selectedMeter?.jobID
        performSegue(withIdentifier: "toCameraScreen", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // If segueing to completion codes, pass through service request type and meter read entries
        if let destinationVC = segue.destination as? ServiceRequestScreenViewController {
            destinationVC.currentMeterRead.mmrReadCode = "N"
            destinationVC.currentMeterRead.newMeterNumber = newMeterNumberField.text ?? ""
            destinationVC.currentMeterRead.newSizeCode = meterSizeCodeField.text ?? ""
            destinationVC.currentMeterRead.newMeterNumberOfDials = Int(numberOfDialsField.text!)
            destinationVC.currentMeterRead.reading = nil
            destinationVC.currentMeterRead.updateReadDate()
            if destinationVC.currentMeterRead.typeCode == "ZB" {
                destinationVC.currentMeterRead.postReading = nil
            }
        }
        
        // If segueing to completion codes, pass through service request type and meter read entries
        if let destinationVC = segue.destination as? PeriodicOutcardScreenViewController {
            destinationVC.currentMeterRead.mmrReadCode = "N"
            destinationVC.currentMeterRead.newMeterNumber = newMeterNumberField.text ?? ""
            destinationVC.currentMeterRead.newSizeCode = meterSizeCodeField.text ?? ""
            destinationVC.currentMeterRead.newMeterNumberOfDials = Int(numberOfDialsField.text!)
            destinationVC.currentMeterRead.reading = nil
            destinationVC.currentMeterRead.updateReadDate()
        }
    }
    
    
    //MARK: - Navigation
    @IBAction func unwindToHome(segue: UIStoryboardSegue){
        
        
        if segue.source is CameraCaptureViewController {
            guard let senderVC = segue.source as? CameraCaptureViewController else { return }

            if let imageFromSegue = senderVC.image {
                meterImage = imageFromSegue
                if meterImage != nil {
                    capturePhotoButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
                }
            }
            else {
                //print("returned without image")
            }
            
            checkAllValid()
        
            // Integrations would go here!

        } else {
            
        }
        

    }
}
