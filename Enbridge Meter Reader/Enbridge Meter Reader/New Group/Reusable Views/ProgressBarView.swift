//
//  ProgressBarView.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2017-12-15.
//  Copyright © 2017 Enbridge. All rights reserved.
//

// HOW TO USE:
// 1. Drop a UIView onto your screen and add Auto Layout constraints as usual.
// 2. Set that UIView's custom class to ProgressBarView
// 3. Call PopulateBar() with the route number, the amount completed, and the total amount.
// 4. ???
// 5. Profit!

import UIKit

class ProgressBarView: UIView {
    
    @IBOutlet weak var fillBarContainer: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var barLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("ProgressBar", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    
    func populateBar(route: Int, completed: Int, total: Int){
        
        let completionPercent = Double(completed) / Double(total)
        let totalWidth = Double(fillBarContainer.frame.width)
        let fillBarWidth = CGFloat(completionPercent * totalWidth)
        
        let progressFillBar = UIView(frame: CGRect(x: 0, y: 0, width: fillBarWidth, height: fillBarContainer.bounds.height))
        progressFillBar.backgroundColor = GlobalConstants.PROGRESS_YELLOW
        
        fillBarContainer.addSubview(progressFillBar)
        
        barLabel.text = "MRU \(route) - \(completed) / \(total) meters completed"
    }

}
