//
//  CameraCaptureViewController.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2017-12-11.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit
import AVFoundation

class CameraCaptureViewController: UIViewController, AVCapturePhotoCaptureDelegate {

    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    
    static var fileName : String! {
        didSet{
            //print("File_Name:\(CameraCaptureViewController.fileName)")
        }
    }
    static var picreason: String!
    static var zindicator:String? = "O"
    
    
    var captureSession : AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var capturePhotoOutput: AVCapturePhotoOutput?
    var capturedImage: UIImage?
    var image : UIImage!
    var seguesToNote: Bool?
    var seguedFromErrorAlert: Bool?
    
    // Variables to be passed to the Notes screen if needed.
    var placeholderText: String?
    var maxCharacters: Int?
    var titleText: String?
    var noteMandatory: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        toggleViewFinder(true)
        
        // Hide navigation back button if coming from error alert (second bad read, user must take a photo of the meter)
        if seguedFromErrorAlert ?? false {
            self.navigationItem.setHidesBackButton(true, animated:true);
        } else {
            self.navigationItem.setHidesBackButton(false, animated:true);
        }
        
        // Checks if the app is currently running on a simulator. If so,
        // puts in a default image so the app doesn't crash.
        if (UIDevice.current.isSimulator){
            image = #imageLiteral(resourceName: "EnbridgeLogo")
            return
        }
        
        let captureDevice = AVCaptureDevice.default(for: .video)
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            cameraView.layer.addSublayer(videoPreviewLayer!)
            
            // Get an instance of ACCapturePhotoOutput class
            capturePhotoOutput = AVCapturePhotoOutput()
            capturePhotoOutput?.isHighResolutionCaptureEnabled = true
            // Set the output on the capture session
            captureSession?.addOutput(capturePhotoOutput!)
            
            captureSession?.startRunning()
            
        } catch {
            //print(error)
            
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // If we've already set a default image (ie: running on simulator)
        // we show this message.
        if (image != nil) && UIDevice.current.isSimulator {
            AudioServicesPlaySystemSound(1315);
            let alert = UIAlertController(title: "Oh hi!", message: "I see you're running this in the simulator! I'll put a default photo in so the app doesn't crash.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
                (_: UIAlertAction!) in
                    self.acceptButtonClicked(UIButton())
                }
            ))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func captureButtonClicked(_ sender: UIButton) {
        
        // Make sure capturePhotoOutput is valid
        guard let capturePhotoOutput = self.capturePhotoOutput else { return }
        
        // Get an instance of AVCapturePhotoSettings class
        let photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        
        // Set photo settings
        photoSettings.isAutoStillImageStabilizationEnabled = true
        photoSettings.isHighResolutionPhotoEnabled = true
        photoSettings.flashMode = .off

        capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self)
        
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        guard let imageData = image.jpeg(.lowest) else {
            return "cannot convert into 64base encode"
        }
        let base64String = imageData.base64EncodedString(options: .lineLength64Characters)
        return base64String
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func rejectButtonClicked(_ sender: UIButton) {
        toggleViewFinder(true)
        if (CaptureImage_SAP_MeterImage.entityCollection.count > 0) {
            CaptureImage_SAP_MeterImage.entityCollection.removeLast()
            capturedImage = nil
        }
        
    }
    @IBAction func acceptButtonClicked(_ sender: UIButton) {
//        //print("Accept!")
        CaptureImage_SAP_MeterImage.sharedInstance.shouldPerformCreateEntity()
//        seguedFromErrorAlert ?? false ? CaptureImage_SAP_MeterImage.sharedInstance.shouldPerformCreateEntity() : //print("NOT due to HIGH LOW!")
        if (seguesToNote != nil){
            performSegue(withIdentifier: "addNotes", sender: nil)
        }
        else {
            performSegue(withIdentifier: "unwindToHome", sender: nil)
        }
    }
    
    
    func toggleViewFinder(_ isCapturing: Bool){
        // Toggles the screen between "capture" and "image preview" mode
        if isCapturing {
            rejectButton.isHidden = true
            rejectButton.isEnabled = false
            acceptButton.isHidden = true
            acceptButton.isEnabled = false
            captureButton.isHidden = false
            captureButton.isEnabled = true
            previewImage.isHidden = true
            cameraView.isHidden = false
        }
        else {
            rejectButton.isHidden = false
            rejectButton.isEnabled = true
            acceptButton.isHidden = false
            acceptButton.isEnabled = true
            previewImage.isHidden = false
            captureButton.isHidden = true
            captureButton.isEnabled = false
            cameraView.isHidden = true
        }
    }
    
    // MARK: - Camera
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation() else {return}
        let rawImage = UIImage(data: imageData)
        if let capturedImage = rawImage {
            previewImage.image = capturedImage
            if let fileName = CameraCaptureViewController.fileName, let picreason = CameraCaptureViewController.picreason, let zindicator = CameraCaptureViewController.zindicator {
                CaptureImage_SAP_MeterImage.sharedInstance.entity = CaptureImage_SAP_MeterImage.sharedInstance.createEntity(fileName:  fileName, picreason: picreason, zindicator: zindicator, value: convertImageToBase64(image: capturedImage))
//                CaptureImage_SAP_MeterImage.entityCollection.append(CaptureImage_SAP_MeterImage.sharedInstance.entity)
            }
//            //print(convertImageToBase64(image: previewImage.image!))
            toggleViewFinder(false)
            
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let newImage = previewImage.image {
            image = newImage
        }
        
        if let destinationVC = segue.destination as? NotesViewController {
            destinationVC.image = image
            destinationVC.title = titleText ?? ""
            destinationVC.placeholderText = placeholderText ?? ""
            destinationVC.maxCharacters = maxCharacters ?? 0
            destinationVC.noteMandatory = noteMandatory ?? false
        }
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
       // return UIImageJPEGRepresentation(self, quality.rawValue)
        return self.jpegData(compressionQuality: quality.rawValue)
    }
}


