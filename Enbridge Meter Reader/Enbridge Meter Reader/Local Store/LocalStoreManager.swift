 //
 //  LocalStoreManager.swift
 //  Enbridge Meter Reader
 //
 //  Created by Jerry on 2018-06-04.
 //  Copyright © 2018 Enbridge. All rights reserved.
 //
 
 import Foundation
 import SAPFoundation
 import SAPOfflineOData
 import SAPCommon
 import SAPOData
 import SAPFiori
 import proxyclasses

 class LocalStoreManager {
    
    
    //For outcard/offcycle route
    private var outCardSP: LocalStoreOutcard?
    private var offCycleSP: LocalStoreOffCycle?
    private var periodicSP: LocalStorePeriodic?
    
    //For global access
    static var ShareInstance: LocalStoreManager? = nil
    
    //For internal use
    private let logger = Logger.shared(named: "Local Store Manager")
    //private var operations: Array<M1Operation>
    private var urlSession: SAPURLSession?
    private var periodicKey = "PERIODIC"
    private var offCycleKey = "OFFCYCLE"
    private var outCardKey = "OUTCARD"
    private var inProgressDownload = false
    private var inProgressUpload = false
    public var downloadsuccessed = true
    public var uploadsuccessed = true
    
    //For data path
    private let statusDP = M1Operation.object.path(M1Object.task).path(M1Task.status)
    private let routeIdDP = M1Operation.object.path(M1Object.task).path(M1Task.mmrMru)
    private let cycleNoDP = M1Operation.object.path(M1Object.task).path(M1Task.mmrCycleNumber)
    private let callIdDP = M1Operation.object.path(M1Object.task).path(M1Task.callID)
    private let numberDP = M1Operation.object.path(M1Object.task).path(M1Task.number)
    private let isPeriodicDP = M1Operation.object.path(M1Object.task).path(M1Task.mmrIsPeriodic)
    private let isOffCycleDP = M1Operation.object.path(M1Object.task).path(M1Task.mmrIsOffCycle)
    private let isOutCardDP = M1Operation.object.path(M1Object.task).path(M1Task.mmrIsOutCard)
    private let sequenceNumDP = M1Operation.object.path(M1Object.task).path(M1Task.mmrSequenceNum)
    private let mmrNumDP = M1Operation.object.path(M1Object.task).path(M1Task.mmrMru)
    
    //Completion Handler after download
    private var downloadCompletionHandler : (Error?) -> Void = {_ in }
    
    //Data flow indicator
    static var dataBytes = 0 {
        didSet {
            OperationQueue.main.addOperation {
                SelectRouteTableViewController.progressView.text = String(LocalStoreManager.dataBytes) + " Bytes\n" + LocalStoreManager.status
            }         
        }
    }
    
    static var status = "" {
        didSet {
            OperationQueue.main.addOperation {
                SelectRouteTableViewController.progressView.text = String(LocalStoreManager.dataBytes) + " " + LocalStoreManager.status
            }
        }
    }
    
    
    //For download counts
    static var numLS = 3
    //Getter/Setter for download all local store
    static var curNumLS : Int = Int() {
        didSet {
            if(LocalStoreManager.curNumLS >= LocalStoreManager.numLS){
                LocalStoreManager.ShareInstance?.inProgressDownload = false
                OperationQueue.main.addOperation({
                    SelectRouteTableViewController.refreshControlRef?.endRefreshing()
                })
                if (LocalStoreManager.ShareInstance?.downloadsuccessed)! { // download ok, postACK
                    LocalStoreManager.ShareInstance?.downloadCompletionHandler(nil)  //process data + UI updates
                    GetWorkOrderFromBackEnd_SAP_Click.sharedInstance.postACKStatus()
                    LogHelper.shared?.upload()
                    OperationQueue.main.addOperation({
                        if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                            if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                                selectVC.unloadBlurView()
                            }
                        }
                    })
                }else{// download failed, need to retry
                    LocalStoreManager.ShareInstance?.downloadCompletionHandler(NSError(domain:"", code:404, userInfo:nil))
                    OperationQueue.main.addOperation{
                        let alertController = UIAlertController(title: "Failed to fetch data", message: String(describing: "Failed to sync with backend, click to retry"), preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Retry", style: .default){_ in
                            GetWorkOrderFromBackEnd_SAP_Click.sharedInstance.loadData {
                                OperationQueue.main.addOperation {
                                    if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                                        if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                                            selectVC.tableView.reloadData()
                                        }
                                    }}
                            }
                        })
                        if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                            if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                                selectVC.present(alertController,animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    //For upload counts
    static var uploadLS = 3
    //Getter/Setter for upload all local store
    static var curUploadLS : Int = Int() {
        didSet {
            if(LocalStoreManager.curUploadLS >= LocalStoreManager.uploadLS){
                LocalStoreManager.ShareInstance?.inProgressUpload = false
                LogHelper.shared?.upload()
                if (LocalStoreManager.ShareInstance?.uploadsuccessed)! { // individual upload OK
                    
                    //We do master update if and only if individual upload succeed
                    LocalStoreUpdateStatusServ.shareInstance?.uploadToDB() { error in
                        guard error == nil else {
                            OperationQueue.main.addOperation({
                                let alertController = UIAlertController(title: "Failed to upload data", message: String(describing: "Failed to sync with backend, please pull to refresh. (Data might not be fully uploaded)"), preferredStyle: .alert)
                                alertController.addAction(UIAlertAction(title: "OK", style: .default))
                                if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                                    if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                                        SelectRouteTableViewController.refreshControlRef?.endRefreshing()
                                        selectVC.unloadBlurView()
                                        selectVC.present(alertController, animated: true, completion: {})
                                    }
                                }
                            })
                            return 
                        }
                        OperationQueue.main.addOperation {
                            Model.sharedInstance.submitQueuedRoutes()
                        } // Then we pull the new data after individual update & master update succeed
                        GetWorkOrderFromBackEnd_SAP_Click.sharedInstance.loadData {
                            OperationQueue.main.addOperation({
                                if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                                    if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                                        selectVC.tableView.reloadData()
                                    }
                                }
                            })
                        }
                    }
                }else{ // individual upload FAILED
                    OperationQueue.main.addOperation({
                        let alertController = UIAlertController(title: "Failed to upload data", message: String(describing: "Failed to sync with backend, please pull to refresh. (Data might not be fully uploaded)"), preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .default))
                        if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                            if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                                SelectRouteTableViewController.refreshControlRef?.endRefreshing()
                                selectVC.unloadBlurView()
                                selectVC.present(alertController, animated: true, completion: {})
                            }
                        }
                    })
                }
            }
        }
    }
    
    init(urlSession: SAPURLSession) {
        
        self.outCardSP = LocalStoreOutcard(urlSession: urlSession)
        self.offCycleSP = LocalStoreOffCycle(urlSession: urlSession)
        self.periodicSP = LocalStorePeriodic(urlSession: urlSession)
        self.urlSession = urlSession
        LocalStoreManager.ShareInstance = self
    }
    
    
    func fetchByFresh(completionHandler: @escaping (Error?) -> Void = {error in }) {
        self.downloadCompletionHandler = completionHandler
        if(!inProgressDownload) {
            inProgressDownload = true
            self.refresh{ error in
                guard error == nil else  {
                    LocalStoreManager.ShareInstance?.downloadsuccessed = false
                    LocalStoreManager.curNumLS = LocalStoreManager.curNumLS + 1
                    return
                }
                LocalStoreManager.curNumLS = LocalStoreManager.curNumLS + 1
            }
        }
    }
    
    deinit {
        self.outCardSP?.destory()
        self.offCycleSP?.destory()
        self.periodicSP?.destory()
        self.urlSession = nil
    }
    
    //Public functions
    func getAllWork() -> Array<M1Operation>? {
        
        var optSet:[M1Operation]? = nil
        do {
            let OffCycleOptEntity = offCycleSP?.returnSP().entitySet(withName: "OperationSet")
            let OffCycleQuery = DataQuery().selectAll().from(OffCycleOptEntity!)
            optSet = M1Operation.array(from: try (offCycleSP?.returnSP().executeQuery(OffCycleQuery).entityList())!)
            
            let OutCardOptEntity = outCardSP?.returnSP().entitySet(withName: "OperationSet")
            let OutCardQuery = DataQuery().selectAll().orderBy(mmrNumDP).thenBy(sequenceNumDP).from(OutCardOptEntity!)
            optSet?.append(contentsOf:M1Operation.array(from: try (outCardSP?.returnSP().executeQuery(OutCardQuery).entityList())!))
            
            
            let PeriodicOptEntity = periodicSP?.returnSP().entitySet(withName: "OperationSet")
            let PeriodicQuery = DataQuery().selectAll().from(PeriodicOptEntity!)
            optSet?.append(contentsOf:M1Operation.array(from: try (periodicSP?.returnSP().executeQuery(PeriodicQuery).entityList())!))
            
            
        }catch let error as NSError {
            print(error)
        }
        
        return optSet
    }
    
    func returnOperationWithStatus(type:String, status:String) -> (Error?,[M1Operation]){
        do {
            var optSet:[M1Operation]? = nil
            let nsError = NSError(domain: "", code: 404 ,userInfo:["localizedDescription":"Local store error"])
            switch type {
            case periodicKey:
                let optEntity = periodicSP?.returnSP().entitySet(withName: "OperationSet")
                let query = DataQuery().selectAll().filter(statusDP.equal(status) && isPeriodicDP.equal(true)).from(optEntity!)
                optSet = M1Operation.array(from: try (periodicSP?.returnSP().executeQuery(query).entityList())!)
                break
            case offCycleKey:
                let optEntity = offCycleSP?.returnSP().entitySet(withName: "OperationSet")
                let query = DataQuery().selectAll().filter(statusDP.equal(status) && isOffCycleDP.equal(true)).from(optEntity!)
                optSet = M1Operation.array(from: (try offCycleSP?.returnSP().executeQuery(query).entityList())!)
                break
            case outCardKey:
                let optEntity = outCardSP?.returnSP().entitySet(withName: "OperationSet")
                let query = DataQuery().selectAll().filter(statusDP.equal(status) && isOutCardDP.equal(true)).from(optEntity!)
                optSet = M1Operation.array(from: (try outCardSP?.returnSP().executeQuery(query).entityList())!)
                break
            default:
                ////print("unknown type")
                return (nsError,[])
            }
            return (nil,optSet!)
        }catch let error as NSError {
            return (error,[])
        }
    }
    
    func updateOperation (type:String,operation:M1Operation) throws -> (Int,String) {
        switch type {
        case periodicKey:
            try self.updateOperationInTable(operation:operation,serviceProvider:(periodicSP?.returnSP())!)
            break
        case offCycleKey:
            try self.updateOperationInTable(operation:operation,serviceProvider:(offCycleSP?.returnSP())!)
            break
        case outCardKey:
            try self.updateOperationInTable(operation:operation,serviceProvider:(outCardSP?.returnSP())!)
            break
        default:
            return (404,"Table is not exist")
        }
        return (200,"Update accepted")
    }
    
    func submitPeriodicRoute (completionHandler: @escaping (Error?) -> Void) {
        do {
            if (try periodicSP?.returnSP().hasPendingUpload())! {
                if let periodicsp = periodicSP {
                self.upload(localStore: periodicsp as LocalStoreMeterInfoProtocol) { error in
                    guard error == nil else{
                        LogHelper.shared?.error("Failed to upload pending work (periodic)", loggerObject: self.logger)
                        completionHandler(error)
                        return
                    }
                    self.upload(localStore: periodicsp as LocalStoreMeterInfoProtocol,completionHandler:completionHandler)
                }
                }
            }else {
                if let periodicsp = periodicSP {
                    self.upload(localStore: periodicsp as LocalStoreMeterInfoProtocol,completionHandler:completionHandler)}
            }
        }catch {
            print(error)
        }
        
    }
    
    func submitOutcardRoute(completionHandler: @escaping (Error?) -> Void){
        
        do {
            if (try outCardSP?.returnSP().hasPendingUpload())! {
                if let outCardsp = outCardSP {

                self.upload(localStore: outCardsp as LocalStoreMeterInfoProtocol){error in
                    guard error == nil else{
                        LogHelper.shared?.error("Failed to upload pending work (outcard)", loggerObject: self.logger)
                        completionHandler(error)
                        return
                    }
                    self.upload(localStore: outCardsp as LocalStoreMeterInfoProtocol,completionHandler:completionHandler)
                    }
                    
                }
            }else {
                if let outCardsp = outCardSP {
                self.upload(localStore: outCardsp as LocalStoreMeterInfoProtocol,completionHandler:completionHandler)
                }
                
            }
        }catch {
            print(error)
        }
        
    }
    
    func submitOffCycleRoute(completionHandler: @escaping (Error?) -> Void){
        do {
            if (try offCycleSP?.returnSP().hasPendingUpload())! {
                if let offCyclesp = offCycleSP {

                self.upload(localStore: offCyclesp as LocalStoreMeterInfoProtocol){error in
                    guard error == nil else{
                        LogHelper.shared?.error("Failed to upload pending work (offcycle)", loggerObject: self.logger)
                        completionHandler(error)
                        return
                    }
                    self.upload(localStore: offCyclesp as LocalStoreMeterInfoProtocol,completionHandler:completionHandler)
                    }
                    
                }
            }else {
                if let offCyclesp = offCycleSP {

                self.upload(localStore: offCyclesp as LocalStoreMeterInfoProtocol,completionHandler:completionHandler)
                }
                
            }
        }catch {
            print(error)
        }
    }
    
    func checkOffcycleStatus() -> (Int,Bool) {
        
        let res = false
        let resCode = 500
        
        if let offRoute = Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID] {
            return (200, offRoute.status != .available)
        }
        
        return (resCode,res);
    }
    
    //we only download data in this function
    public func refresh(completionHandler: @escaping (Error?) -> Void = {error in }) {
        LocalStoreManager.curNumLS = 0
        downloadsuccessed = true
        
        if let offcyclesp = offCycleSP {
            self.download(localStore: offcyclesp as LocalStoreMeterInfoProtocol,completionHandler:completionHandler)}
        
         if let outcardsp = outCardSP {
        self.download(localStore: outcardsp as LocalStoreMeterInfoProtocol,completionHandler:completionHandler)
        }
             if let periodicsp = periodicSP {
        self.download(localStore: periodicsp as LocalStoreMeterInfoProtocol,completionHandler: completionHandler)
            }
        
    }
    
    //upload only
    public func offlineToOnlineSync() {
        if !inProgressUpload  {
            if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                    OperationQueue.main.addOperation {
                        selectVC.loadBlurView()
                        FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Device is now syncing with backend", comment: "Device now is syncing with backend"))
                    }
                }
            }
            LocalStoreManager.curUploadLS = 0
            uploadsuccessed = true
            inProgressUpload = true
            
            if let offcyclesp = offCycleSP {
            self.upload(localStore: offcyclesp as LocalStoreMeterInfoProtocol) { error in
                guard error == nil else {
                    print("upload offcycle in offline sync failed")
                    LocalStoreManager.curUploadLS =  LocalStoreManager.curUploadLS + 1
                    self.uploadsuccessed = false
                    return
                }
                LocalStoreManager.curUploadLS =  LocalStoreManager.curUploadLS + 1
                }}
            
            if let outcardsp = outCardSP {
            self.upload(localStore: outcardsp as LocalStoreMeterInfoProtocol) { error in
                guard error == nil else {
                    print("upload outcard in offline sync failed")
                    LocalStoreManager.curUploadLS =  LocalStoreManager.curUploadLS + 1
                    self.uploadsuccessed = false
                    return
                }
                LocalStoreManager.curUploadLS =  LocalStoreManager.curUploadLS + 1
                }}
            
            if let periodicsp = periodicSP {
                self.upload(localStore: periodicsp as LocalStoreMeterInfoProtocol) { error in
                guard error == nil else {
                    print("upload periodic in offline sync failed")
                    LocalStoreManager.curUploadLS =  LocalStoreManager.curUploadLS + 1
                    self.uploadsuccessed = false
                    return
                }
                LocalStoreManager.curUploadLS =  LocalStoreManager.curUploadLS + 1
                }}
            
            LocalStoreNewMeter.offlineLS?.uploadToDB()
            LocalStoreImage.offlineLS?.uploadToDB()
            LocalStoreReSequence.offlineLS?.uploadToDB()
        }
        
    }
    
    
    public func deleteEntity (number:Int?, jobID:String, type: String) -> Bool{
        
        //case offcyle
        do {
            _ = offCycleSP?.returnSP().entitySet(withName: "OperationSet")
            let operation = M1Operation()
            operation.callID = jobID
            operation.number = number
            _ = try offCycleSP?.returnSP().deleteEntity(operation)
            return true
        }catch let error as NSError {
            print(error)
            return false
        }
        
    }
    
    public func closeAllStore(){
        //sync offcycle/outcard
        offCycleSP?.closeOfflineStore()
        outCardSP?.closeOfflineStore()
        periodicSP?.closeOfflineStore()
        
    }
    
    //Private functions
    private func updateOperationInTable(operation:M1Operation,serviceProvider:M1Ec1<OfflineODataProvider>)throws {
        // let options = RequestOptions()
        // options.updateMode = UpdateMode.replace
        let headers = HTTPHeaders()
        headers.setHeader(withName: "OfflineOData.NonMergeable", value: "yes")
        let options = OfflineODataRequestOptions()
        options.sendEmptyUpdate = true
        operation.object?.task?.mmrEngineerID = Utilities.sharedInstance.userName
        try serviceProvider.updateEntity(operation,headers:headers, options:options)
    }
    
    
    private func upload(localStore: LocalStoreMeterInfoProtocol, completionHandler: @escaping (Error?) -> Void = {error in }){
        Persistence.shared.saveRoutes()
        localStore.upload(completionHandler:completionHandler);
    }
    
    private func download(localStore: LocalStoreMeterInfoProtocol,completionHandler: @escaping (Error?) -> Void = {error in}){
        localStore.download(completionHandler:completionHandler);
    }
    
 }
