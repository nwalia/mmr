// # Proxy Compiler 17.12.5-600bdb-20180507

import Foundation
import SAPOData

internal class N1ZDMNEWSERVICESRVEntitiesMetadataParser {
    internal static let options: Int = (CSDLOption.processMixedVersions | CSDLOption.retainOriginalText | CSDLOption.ignoreUndefinedTerms)

    internal static let parsed: CSDLDocument = N1ZDMNEWSERVICESRVEntitiesMetadataParser.parse()

    static func parse() -> CSDLDocument {
        let parser: CSDLParser = CSDLParser()
        parser.logWarnings = false
        parser.csdlOptions = N1ZDMNEWSERVICESRVEntitiesMetadataParser.options
        let metadata: CSDLDocument = parser.parseInProxy(N1ZDMNEWSERVICESRVEntitiesMetadataText.xml, url: "ZDM_NEW_SERVICE_SRV")
        metadata.proxyVersion = "17.12.5-600bdb-20180507"
        return metadata
    }
}
