// # Proxy Compiler 17.12.5-600bdb-20180507

import Foundation
import SAPOData

open class N1ZdmNewService: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    public static var equnr: Property = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "Equnr")

    public static var matnr: Property = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "Matnr")

    public static var ableinh: Property = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "Ableinh")

    public static var vZwstand: Property = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "VZwstand")

    public static var address: Property = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "Address")

    public static var dials: Property = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "Dials")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService)
    }

    open var ableinh: String? {
        get {
            return StringValue.optional(self.optionalValue(for: N1ZdmNewService.ableinh))
        }
        set(value) {
            self.setOptionalValue(for: N1ZdmNewService.ableinh, to: StringValue.of(optional: value))
        }
    }

    open var address: String? {
        get {
            return StringValue.optional(self.optionalValue(for: N1ZdmNewService.address))
        }
        set(value) {
            self.setOptionalValue(for: N1ZdmNewService.address, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> Array<N1ZdmNewService> {
        return ArrayConverter.convert(from.toArray(), Array<N1ZdmNewService>())
    }

    open func copy() -> N1ZdmNewService {
        return CastRequired<N1ZdmNewService>.from(self.copyEntity())
    }

    open var dials: String? {
        get {
            return StringValue.optional(self.optionalValue(for: N1ZdmNewService.dials))
        }
        set(value) {
            self.setOptionalValue(for: N1ZdmNewService.dials, to: StringValue.of(optional: value))
        }
    }

    open var equnr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: N1ZdmNewService.equnr))
        }
        set(value) {
            self.setOptionalValue(for: N1ZdmNewService.equnr, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(equnr: String?) -> EntityKey {
        return EntityKey().with(name: "Equnr", value: StringValue.of(optional: equnr))
    }

    open var matnr: String? {
        get {
            return StringValue.optional(self.optionalValue(for: N1ZdmNewService.matnr))
        }
        set(value) {
            self.setOptionalValue(for: N1ZdmNewService.matnr, to: StringValue.of(optional: value))
        }
    }

    open var old: N1ZdmNewService {
        return CastRequired<N1ZdmNewService>.from(self.oldEntity)
    }

    open var vZwstand: String? {
        get {
            return StringValue.optional(self.optionalValue(for: N1ZdmNewService.vZwstand))
        }
        set(value) {
            self.setOptionalValue(for: N1ZdmNewService.vZwstand, to: StringValue.of(optional: value))
        }
    }
}
