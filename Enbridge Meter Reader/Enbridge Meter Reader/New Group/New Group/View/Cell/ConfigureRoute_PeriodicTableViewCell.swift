//
//  ConfigureRoute_PeriodicTableViewCell.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-12.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit

class ConfigureRoute_PeriodicTableViewCell: UITableViewCell {

    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var addressLine1: UILabel!
    @IBOutlet weak var addressLine2: UILabel!
    @IBOutlet weak var mruLabel: UILabel!
    @IBOutlet weak var meterNumberLabel: UILabel!
    @IBOutlet weak var locationCodeLabel: UILabel!
    @IBOutlet weak var estimationLabel: UILabel!
    @IBOutlet weak var keyStackView: UIStackView!
    @IBOutlet weak var instructionStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
}
