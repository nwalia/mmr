//
//  DashboardViewController+Data.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-09.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

extension DashboardViewController : UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Customer Information") as! CustomerInformationCell
            cell.scrollButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Special Instruction") as! SpecialInstructionCell
            cell.delegate = self
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Order Detail Cell") as! OrderDetailCell
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch editButtonToggle {
        case true:
            if indexPath.row == 0 {
                return 0.0
            }
            if indexPath.row == 1{
                return 0.0
            }
            else{
                return 842.0
            }
        default:
            if indexPath.row == 1 {
                return 120.0
            }
            if indexPath.row == 2{
                return 450.0
            }
            else{
                return UITableView.automaticDimension
            }
        }
    }
}
