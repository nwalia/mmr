//
//  ConfigureRouteViewController.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-03.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit
import SAPCommon

class ConfigureRouteViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reverseSegmentedControl: UISegmentedControl!
    
    static var caller = String()
    static var previousViewController : UIViewController?
    
    @IBOutlet weak var configureButton: UIButton!
    
    var originalRouteOrder: [MeterRead]?
    
    var identifier = ""
    var isEditingOrder = false
    var periodicCell:PeriodicCell?
    private let logger = Logger.shared(named: "Configure Route View")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logger.logLevel = .debug
        
        navigationController?.navigationBar.tintColor = UIColor(red:1, green:0.72, blue:0.11, alpha:1)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        
        // Do any additional setup after loading the view.
        navigationItem.rightBarButtonItem = editButtonItem
        
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        isEditingOrder = editing
        
        editing ? enableDragDrop(): disableDragDrop()
        
        // Disable spring loading while the table view is editing.
        tableView.isSpringLoaded = !editing
    }
    
    private func enableDragDrop() {
        if originalRouteOrder == nil {
            originalRouteOrder = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads
        }
        configureButton.setTitle("Reset to Original Route", for: .normal)
        
        self.tableView.dragDelegate = self
        self.tableView.dropDelegate = self
        self.tableView.dragInteractionEnabled = true
    }
    
    private func disableDragDrop() {
        configureButton.setTitle("Configure & Begin Route", for: .normal)
        self.tableView.dragDelegate = nil
        self.tableView.dropDelegate = nil
        self.tableView.dragInteractionEnabled = false
        Model.sharedInstance.routes[Model.selectedRouteKey]?.resequencedList = (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads)!
        
        Persistence.shared.saveRoutes()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func didPressedSegmentControll(_ sender: UISegmentedControl) {
        if originalRouteOrder == nil {
            originalRouteOrder = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads
        }
        
        switch sender.selectedSegmentIndex {
        case 0:
            Model.sharedInstance.reverseDataModelForSelected(segment: sender.selectedSegmentIndex)
            self.tableView.reloadData()
        case 1:
            Model.sharedInstance.reverseDataModelForSelected(segment: sender.selectedSegmentIndex)
            self.tableView.reloadData()
        default:
            return
        }
    }
    
    @IBAction func didPressConfigure(_ sender: UIButton) {
        
        if isEditingOrder {
            guard originalRouteOrder != nil else { return }
            reverseSegmentedControl.selectedSegmentIndex = 0
            Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads = originalRouteOrder!
            tableView.reloadData()
            return
        }
        switch ConfigureRouteViewController.caller {
        case "periodic":
            //PeriodicCell.changeState = true
            identifier = "toPeriodicDashboard"
        case "offCycle":
            OffCycleCell.hasBeenStarted = true
            identifier = "toDashboard"
            UserDefaults.standard.set("EN-ROUTE", forKey: "OffCycleRouteStatus")
            UserDefaults.standard.set(true, forKey: "didSelectOffCycleCell")
            UserDefaults.standard.synchronize()
        case "outCard":
            Model.sharedInstance.routes[Model.selectedRouteKey]?.status = .inProgress
            identifier = "toPeriodicDashboard"
        default:
            return
        }
        
        // set the selected route's status to "in progress"
        Model.sharedInstance.routes[Model.selectedRouteKey]?.status = .inProgress
        
        guard let meters = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads else {
            return
        }
        for meter in meters {
            if meter.isOffcycle ?? false{
                do {
                    meter.mmrStatus == "CANCEL" ? print("Work order is in \(meter.status) status") : (meter.mmrStatus = "EN-ROUTE")
                    if !AppDelegate.hasMockData {
                        let fetchedEntities = try LocalStoreManager.ShareInstance?.updateOperation(type: RouteType.OFFCYCLE.rawValue, operation: MeterRead.mapTo_Operation(meterRead: meter, withCompletionCode: ""))
                        
                        if fetchedEntities?.0 == 200 {
                            LogHelper.shared?.info("Change Status to ON-ROUTE successfully with work ID:" + meter.jobID, loggerObject: self.logger)
                        }else{
                            LogHelper.shared?.error("Change Status to ON-ROUTE failed with work ID:" + meter.jobID, loggerObject: self.logger)
                        }
                
                    }
                } catch {
                    print("Update Operation Failed: \(error)")
                    LogHelper.shared?.error("Change Status to ON-ROUTE failed with work ID:" + meter.jobID, loggerObject: self.logger)
                }
            }
        }
        if Model.selectedRouteKey == GlobalConstants.OFFCYCLE_ROUTE_ID {
            //Load the blur view to disable user interaction.
            DispatchQueue.main.async {
                if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                    if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                        selectVC.loadBlurView()
                    }
                }
            }
            LocalStoreManager.ShareInstance?.submitOffCycleRoute{error in
                //Load the blur view to disable user interaction.
                OperationQueue.main.addOperation {
                    if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                        if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                            selectVC.unloadBlurView()
                        }
                    }
                }
            }
        }
        
        _ = ConfigureRouteViewController.previousViewController as! SelectRouteTableViewController
        
        Persistence.shared.saveRoutes()
    }
    
    @IBAction func didPressCancel(_ sender: UIBarButtonItem) {
        if originalRouteOrder != nil {
            // route has been edited, revert it back to the original.
            Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads = originalRouteOrder!
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return !isEditingOrder
    }
    
    private func returnRouteId() -> String?{
        return Model.sharedInstance.routes[Model.selectedRouteKey]?.id
    }
    
}
