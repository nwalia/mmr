//
//  MapViewManager.swift
//  MapView
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-19.
//  Copyright © 2017 Deshpande, Ankush (CA - Toronto). All rights reserved.
//

import Foundation
import MapKit

class MapViewManager: NSObject{
    
//    let source = Location.source(latitude: 40.759011, longitude: -73.984472)
//    let destination = Location.destination(latitude: 40.748441, longitude: -73.985564)
    
    
    private func setCoordinates(source: Location, destination: Location) -> (sourceCoordinate:CLLocationCoordinate2D,destinationCoordinate:CLLocationCoordinate2D){
        return (source.setCoordinates,destination.setCoordinates)
    }
    
    private func setMarkers(_ sourceCoordinate:CLLocationCoordinate2D, _ destinationCoordinate:CLLocationCoordinate2D) -> (sourceMarker:MKPlacemark,destinationMarker:MKPlacemark) {
        let sourceLocation = Marker.source(location: sourceCoordinate)
        let destinationLocation = Marker.destination(Location: destinationCoordinate)
        
        return (sourceLocation.placeMarker, destinationLocation.placeMarker)
    }
    
    private func setMapItem(_ sourceMarker: MKPlacemark, _ destinationMarker: MKPlacemark) -> (MKMapItem,MKMapItem) {
        let sourceItem = MapItem.source(marker: sourceMarker)
        let destinationItem = MapItem.destination(marker: destinationMarker)
        
    return (sourceItem.placeMapItem,destinationItem.placeMapItem)
    }
    
    func generateMarkerFor(source: Location, destination: Location) -> (MKMapItem,MKMapItem) {
        return setMapItem(setMarkers(setCoordinates(source: source,destination: destination).sourceCoordinate,setCoordinates(source: source,destination: destination).destinationCoordinate).sourceMarker,setMarkers(setCoordinates(source: source,destination: destination).sourceCoordinate,setCoordinates(source: source,destination: destination).destinationCoordinate).destinationMarker)
    }
    
    func setAnnotation(sourceTitle: String, destinationTitle: String) -> (sourceAnnotation:MKPointAnnotation,destinationAnnotation:MKPointAnnotation)  {
        let sourceAnnotation = MKPointAnnotation()
        let destinationAnnotation = MKPointAnnotation()
        sourceAnnotation.title = sourceTitle
        destinationAnnotation.title = destinationTitle
        
        return (sourceAnnotation,destinationAnnotation)
    }
    
    func addRouteToMap(sourceMapItem:MKMapItem,destinationMapItem:MKMapItem,mapView:MKMapView,transportType:MKDirectionsTransportType = .automobile ) {
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = transportType
        
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if error != nil {
                    //print("Error: \(error)")
                }
                return
            }
            
            let route = response.routes[0]
            
            mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            mapView.setRegion(MKCoordinateRegion(rect), animated: true)
            
        }
    }
}

extension MapViewManager: MKMapViewDelegate  {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        
        return renderer
    }
}

extension MapViewManager {
    static func setUpMapView(minCountForClustering:Int = 3, manager:ClusterManager, mapView:MKMapView, annotations: [Annotation]) {
        
        guard !annotations.isEmpty else { return }
        
        manager.minCountForClustering = minCountForClustering
        
        var latitudes = [Double]()
        var longitudes = [Double]()
        
        for anno in annotations {
            latitudes.append(anno.coordinate.latitude)
            longitudes.append(anno.coordinate.longitude)
        }
        
        latitudes = latitudes.sorted()
        longitudes = longitudes.sorted()
        
        let medianLat = latitudes[latitudes.count / 2]
        let medianLong = longitudes[longitudes.count / 2]
        
        let center = CLLocationCoordinate2D(latitude: medianLat, longitude: medianLong) // region center
        let delta = 0.02 // region span
        
        // Add annotations to the manager.
        
        manager.removeAll()
        manager.add(annotations)
        
        mapView.region = .init(center: center, span: .init(latitudeDelta: delta, longitudeDelta: delta))
    }
}
