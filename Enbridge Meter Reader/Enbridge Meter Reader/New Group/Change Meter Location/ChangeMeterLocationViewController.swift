//
//  ChangeMeterLocationViewController.swift
//  Enbridge Meter Reader
//
//  Created by Johann Wentzel on 2017-11-29.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit

class ChangeMeterLocationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    enum PreviousScreenType {
        case offcycle
        case periodicOutcard
    }
    
    var codes = [LocationCode]()
    var filteredCodes = [LocationCode]()
    var confirmedCode: LocationCode?
    static var previous: PreviousScreenType?
    
    
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        codes = generateDummyCodes()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Get selected location code from whichever screen appeared previously.
        if ChangeMeterLocationViewController.previous == .periodicOutcard {
            confirmedCode = PeriodicDashboardViewController.currentMeterLocation
        }
        else {
            confirmedCode = DashboardViewController.currentMeterLocation
        }
        
        // Highlight the previously selected code in the list.
        if confirmedCode != nil {
            for index in 0..<codes.count {
                if confirmedCode == codes[index] {
                    codes[index].selected = true
                }
                else {
                    codes[index].selected = false
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification){
        guard let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            self.tableViewBottomConstraint.constant = keyboardHeight * -1
        })
    }
    
    // MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering() ? filteredCodes.count : codes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeMeterLocationTableViewCell", for: indexPath) as! ChangeMeterLocationTableViewCell
        var codeForCell : LocationCode
        
        if isFiltering(){
            codeForCell = filteredCodes[indexPath.row]
        }
        else {
            codeForCell = codes[indexPath.row]
        }
        
        cell.backgroundColor = codeForCell.selected ? GlobalConstants.SELECTED_CELL : .white
        cell.locationNumberLabel.text = codeForCell.number + " " + codeForCell.id
        cell.locationDescriptionLabel.text = codeForCell.description
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let codeForCell = isFiltering() ? filteredCodes[indexPath.row] : codes[indexPath.row]
        
        let alert = UIAlertController(title: "\(codeForCell.number) \(codeForCell.id) - \(codeForCell.description)", message: "Are you sure you want to change the location code?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: {(alert: UIAlertAction!) in self.cancelSelection(indexPath)}))
        alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: {(alert: UIAlertAction!) in self.submitLocationChange(codeForCell)}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func submitLocationChange(_ code: LocationCode) {
        // Integration or segue goes here!
        
        confirmedCode = code
        performSegue(withIdentifier: "ExitToServiceRequest", sender: nil)
        NotificationCenter.default.removeObserver(self)
        
    }
    
    func cancelSelection(_ path: IndexPath){
        tableView.deselectRow(at: path, animated: true)
    }
    
    // MARK: - Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(searchBar.text!)
        
        // scroll to top of table view
        if (tableView.numberOfRows(inSection: 0) > 0){
            let topOfTable = IndexPath(row: 0, section: 0)
            tableView.scrollToRow(at: topOfTable, at: .top, animated: true)
        }
    
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            self.tableViewBottomConstraint.constant = 0
            })
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredCodes = codes.filter({(code: LocationCode) -> Bool in
            return (
                // returns true if search text matches number, description, or id.
                code.description.lowercased().contains(searchText.lowercased())
                    || String(code.number).lowercased().contains(searchText.lowercased())
                    || String(code.id).lowercased().contains(searchText.lowercased())
            )
        })
        
        tableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        // potential additional criteria for isFiltering: searchBar.isFocused
        return !searchBarIsEmpty()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Dummy Data
    func generateDummyCodes() -> [LocationCode]{
        let codes = [
            LocationCode(number: "01", id: "AB", description: "Outside Back"),
            LocationCode(number: "02", id: "AF", description: "Outside Front"),
            LocationCode(number: "03", id: "AL", description: "Outside Left"),
            LocationCode(number: "04", id: "AO", description: "Outside Meter"),
            LocationCode(number: "05", id: "AG", description: "Glass Block"),
            LocationCode(number: "06", id: "AR", description: "Outside Right"),
            LocationCode(number: "07", id: "AT", description: "Rooftop"),
            LocationCode(number: "08", id: "BA", description: "BkDr Front Wall"),
            LocationCode(number: "09", id: "BB", description: "BkDr Back Wall"),
            LocationCode(number: "10", id: "BC", description: "BkDr Under Stairs"),
            LocationCode(number: "11", id: "BD", description: "BkDr Dog OK"),
            LocationCode(number: "12", id: "BE", description: "BkDr Basement Cupboard"),
            LocationCode(number: "13", id: "BG", description: "BkDr Under stairs"),
            LocationCode(number: "14", id: "BH", description: "BkDr Cupboard"),
            LocationCode(number: "15", id: "BI", description: "BkDr Bathroom"),
            LocationCode(number: "16", id: "BJ", description: "BkDr Front Room"),
            LocationCode(number: "17", id: "BK", description: "BkDr Kitchen"),
            LocationCode(number: "18", id: "BN", description: "BkDr Bedroom"),
            LocationCode(number: "19", id: "BO", description: "BkDr"),
            LocationCode(number: "20", id: "BS", description: "BkDr Dangerous Stairs"),
            LocationCode(number: "21", id: "BX", description: "BkDr Bad Dog"),
            LocationCode(number: "22", id: "CA", description: "Cellar Dr Front Wall"),
            LocationCode(number: "23", id: "CB", description: "Cellar Dr Back Wall"),
            LocationCode(number: "24", id: "CC", description: "Cellar Dr Under Stairs"),
            LocationCode(number: "25", id: "CD", description: "Cellar Dr Dog OK"),
            LocationCode(number: "26", id: "CE", description: "Cellar Dr Cupboard"),
            LocationCode(number: "27", id: "CJ", description: "Cellar Dr Under Stairs"),
            LocationCode(number: "28", id: "CO", description: "Cellar Dr "),
            LocationCode(number: "29", id: "CX", description: "Cellar Dr Bad Dog"),
            LocationCode(number: "30", id: "FA", description: "FrDr Front Wall"),
            LocationCode(number: "31", id: "FB", description: "FrDr Back Wall"),
            LocationCode(number: "32", id: "FC", description: "FrDr Under Stairs"),
            LocationCode(number: "33", id: "FD", description: "FrDr Dog OK"),
            LocationCode(number: "34", id: "FE", description: "FrDr Basement Cupboard"),
            LocationCode(number: "35", id: "FG", description: "FrDr Under Stairs"),
            LocationCode(number: "36", id: "FH", description: "FrDr Cupboard"),
            LocationCode(number: "37", id: "FI", description: "FrDr Bathroom"),
            LocationCode(number: "38", id: "FK", description: "FrDr Kitchen"),
            LocationCode(number: "39", id: "FN", description: "FrDr Bedroom"),
            LocationCode(number: "40", id: "FO", description: "FrDr"),
            LocationCode(number: "41", id: "FS", description: "FrDr Dangerous Stairs"),
            LocationCode(number: "42", id: "FX", description: "FrDr Bad Dog"),
            LocationCode(number: "43", id: "GA", description: "Garage Dr Front Wall"),
            LocationCode(number: "44", id: "GB", description: "Garage Dr Back Wall"),
            LocationCode(number: "45", id: "GD", description: "Garage Dr Dog OK"),
            LocationCode(number: "46", id: "GE", description: "Garage Dr Cupboard"),
            LocationCode(number: "47", id: "GG", description: "Garage Dr Under Stairs"),
            LocationCode(number: "48", id: "GK", description: "Garage Dr Kitchen"),
            LocationCode(number: "49", id: "GO", description: "Garage Dr"),
            LocationCode(number: "50", id: "GS", description:"Garage Dr Dangerous Stairs"),
            LocationCode(number: "51", id: "HA", description: "Thru Hse Front Door"),
            LocationCode(number: "52", id: "HB", description: "Thru Hse Back Door"),
            LocationCode(number: "53", id: "HC", description: "Thru Hse Under Stairs"),
            LocationCode(number: "54", id: "HK", description: "Thru Hse Kitchen"),
            LocationCode(number: "55", id: "IA", description: "Via Store Front Wall"),
            LocationCode(number: "56", id: "IB", description: "Via Store Back Wall"),
            LocationCode(number: "57", id: "ID", description: "Via Store Dog OK"),
            LocationCode(number: "58", id: "IG", description: "Via Store Under Stairs"),
            LocationCode(number: "59", id: "IH", description: "Via Store Cupboard"),
            LocationCode(number: "60", id: "IK", description: "Via Store Kitchen"),
            LocationCode(number: "61", id: "IN", description: "Via Store Bedroom"),
            LocationCode(number: "62", id: "IO", description: "Via Store"),
            LocationCode(number: "63", id: "IS", description: "Via Store Dangerous Stairs"),
            LocationCode(number: "64", id: "SA", description: "SdDr Front Wall"),
            LocationCode(number: "65", id: "SB", description: "SdDr Back Wall"),
            LocationCode(number: "66", id: "SC", description: "SdDr Under Stairs"),
            LocationCode(number: "67", id: "SD", description: "SdDr Dog OK"),
            LocationCode(number: "68", id: "SE", description: "SdDr Basement Cupboard"),
            LocationCode(number: "69", id: "SG", description: "SdDr Under Stairs"),
            LocationCode(number: "70", id: "SH", description: "SdDr Cupboard"),
            LocationCode(number: "71", id: "SI", description: "SdDr Bathroom"),
            LocationCode(number: "72", id: "SK", description: "SdDr Kitchen"),
            LocationCode(number: "73", id: "SN", description: "SdDr Bedroom"),
            LocationCode(number: "74", id: "SO", description: "SdDr"),
            LocationCode(number: "75", id: "SS", description: "SdDr Dangerous Stairs"),
            LocationCode(number: "76", id: "SX", description: "SdDr Bad Dog"),
            LocationCode(number: "77", id: "TA", description: "TrapDr Front Wall"),
            LocationCode(number: "78", id: "TD", description: "TrapDr Dog OK"),
            LocationCode(number: "79", id: "TO", description: "TrapDr"),
            LocationCode(number: "80", id: "QR", description: "Glass Block Rt"),
            LocationCode(number: "81", id: "QL", description: "Glass Block Left"),
            LocationCode(number: "82", id: "QF", description: "Glass Block Front"),
            LocationCode(number: "83", id: "QB", description:"Glass Block Back"),
            
        ]
        return codes
    }
    
}
