//
//  Persistence.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-05-11.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation

class Persistence {
    static var shared = Persistence()
    
    static let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static var archiveURL: URL {
        get {
            return Persistence.documentsDirectory.appendingPathComponent("/\(Utilities.sharedInstance.userName ?? "noUserId")/ \(Utilities.sharedInstance.userName ?? "noUserId")-routes")
        }
    }
    
    
    
    func saveRoutes(){
        
        let url = Persistence.archiveURL
         //print("PERSISTENCE - Saving with URL \(url)")
        
        do {
                let routeData = try PropertyListEncoder().encode(Model.sharedInstance.routes)
                
                //let success_routeData = NSKeyedArchiver.archiveRootObject(routeData, toFile: url.path)
                
                let codedData = try! NSKeyedArchiver.archivedData(withRootObject: routeData, requiringSecureCoding: false)
                
                do {
                    try codedData.write(to: url)
                    print("PERSISTENCE_routeData - Successful save")
                }catch{
                    print("PERSISTENCE - Save Failed")
                }
            } catch {
            print("PERSISTENCE - Save Failed with error: \(error)")
        }
    }
    
    
    
    
    func loadRoutes(){
        
        Model.clear()
        
        let url = Persistence.archiveURL
         //print("PERSISTENCE: Loading with URL \(url)")
        
//        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: url.path) as? Data else {
//            //print("PERSISTENCE - No data to load")
//            return
//        }
        
        
        
        do {
            
            let fileData = try Data(contentsOf: url)
            guard let data = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(fileData) as? Data else {return}
            
            // ---- Rest part --- //
            do {
                       let loaded = try PropertyListDecoder().decode([String:Route].self, from: data)
                       for (key,value) in loaded {
                           value.populateAnnotations()
                           Model.sharedInstance.routes[key] = value
                       }
                   } catch {
                       //print("PERSISTENCE - Decode failed with error: \(error)")
                   }
            
        } catch {
            print("didn't work")
        }
        
        
        
       
        
    }
}
