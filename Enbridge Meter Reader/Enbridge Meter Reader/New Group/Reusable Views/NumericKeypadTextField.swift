//
//  NumericKeypadTextView.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-05.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit
import Speech

class NumericKeypadTextField: ValidatedTextField, NumKeypadDelegate {
    
    static let sharedInstance = NumericKeypadTextField()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Set frame for UIAlertViewController, up 8 because covering meter number
        self.delegate = self
        self.inputView = NumKeypad(frame: CGRect(x: 0, y: 0, width: 0, height: NumKeypad.recommendedHeight+8))
        self.createToolBar()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.delegate = self
        self.inputView = NumKeypad(frame: CGRect(x: 0, y: 0, width: 0, height: NumKeypad.recommendedHeight))
        self.createToolBar()
    }
    
    
    // Tapped a number or the decimal point.
    func didTapOn(number: String?) {
        if textField(self, shouldChangeCharactersIn: NSRange(location: 0, length: (self.text?.count)!), replacementString: number!) {
            
            self.text?.append(number!)
            self.sendActions(for: .editingChanged)
            
        }
        else if self.text?.count == maxLength && self.selectedTextRange?.start != self.selectedTextRange?.end {
            replace(self.selectedTextRange!, withText: number!)
        }
    }
    
    func createToolBar()  {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let toolbarSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dismissKeyboard))
        toolbar.items = [toolbarSpace, doneBarButton]
        self.inputAccessoryView = toolbar
        self.inputAssistantItem.leadingBarButtonGroups.removeAll()
        self.inputAssistantItem.trailingBarButtonGroups.removeAll()
    }
    
    
    // Tapped backspace.
    func didBackspace() {
        if var text = self.text, text.count > 0 {
            _ = text.remove(at: text.index(before: text.endIndex))
            self.text = text
        }
        self.sendActions(for: .editingChanged)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NumKeypad.delegate = self
    }
    
    @objc func dismissKeyboard(){
        self.resignFirstResponder()
        NumKeypad.delegate = nil
    }    
}

