//
//  MeterReadBar.swift
//  Enbridge Meter Reader
//
//  Created by Abramsky, Lauren (CA - Toronto) on 2018-01-25.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

protocol MeterReadBarDelegate: class {
    // Action that fires when bar is valid
    func barDidValidate(_ isValid: Bool, _ title: String)
    // Action that fires when button is clicked
    func tappedCameraButton(_ title: String)
}

@IBDesignable
class MeterReadBarView: ReusableView {
    
    weak var delegate: MeterReadBarDelegate?
    
    @IBOutlet weak var meterReadBarBackground: UIView!
    @IBOutlet weak var meterReadInput1: NumericKeypadTextField!
    @IBOutlet weak var meterReadInput2: NumericKeypadTextField!
    @IBOutlet weak var meterReadInput3: NumericKeypadTextField!
    @IBOutlet weak var meterReadInput4: NumericKeypadTextField!
    @IBOutlet weak var meterReadInput5: NumericKeypadTextField!
    @IBOutlet weak var meterReadInput6: NumericKeypadTextField!
    @IBOutlet weak var cameraButton: UIButton!
    
    var readDirection: String!
    var numberOfDials = Int()
    
    let keyboardDistance: CGFloat = 150.0
    
    // For figuring out which image was requested
    var wantsImageForMeter1: Bool = false
    
    // Image containers
    var meterImage1: UIImage?
    var meterImage2: UIImage?
    
    // Variables for checking valid fields
    var meterReadBarValid: Bool = false
    var secondReadBarValid: Bool = false
    var completionCodeEnabled: Bool = false
    var secondBarEnabled: Bool = false
    var meterReadBarTitle = String()
    
    override func awakeFromNib() {
        
        // Listening for commands from Dashboard and Service Request
        super.awakeFromNib()
        setUp(self, self.meterReadBarTitle ,self.numberOfDials)
        
    }
    
    
    // Set up meter bar with unique title so it can be identified
    func setUp(_ meterReadBar: MeterReadBarView, _ title: String, _ dials: Int) {
        
        
        numberOfDials = dials
        meterReadBarTitle = title
        
        // Get read direction
        readDirection = UserDefaults.standard.object(forKey: "ReadDirection") as? String
        
        addTargets()
        
        meterReadInput1?.keyboardDistanceFromTextField = keyboardDistance
        meterReadInput2?.keyboardDistanceFromTextField = keyboardDistance
        meterReadInput3?.keyboardDistanceFromTextField = keyboardDistance
        meterReadInput4?.keyboardDistanceFromTextField = keyboardDistance
        if numberOfDials == 5 {
            meterReadInput5?.keyboardDistanceFromTextField = keyboardDistance
        } else if numberOfDials == 6 {
            meterReadInput5?.keyboardDistanceFromTextField = keyboardDistance
            meterReadInput6?.keyboardDistanceFromTextField = keyboardDistance
        }
        
        clearMeterReadEntry()
        
        
        // Hide some meter read inputs to match number of dials (only covering cases for 4-6 dials because that is what currently exists in the system)
        if numberOfDials == 4 {
            if meterReadInput5 == nil && meterReadInput6 == nil {
            }
            else if meterReadInput5 != nil && meterReadInput6 == nil {
                meterReadInput5?.removeFromSuperview()
            }
            else if meterReadInput5 != nil && meterReadInput6 != nil {
                meterReadInput5?.removeFromSuperview()
                meterReadInput6?.removeFromSuperview()
            }
        } else if numberOfDials == 5 {
            if meterReadInput5 == nil && meterReadInput6 == nil {
                
            }
            else if meterReadInput5 != nil && meterReadInput6 == nil {
            }
            else if meterReadInput5 != nil && meterReadInput6 != nil {
                meterReadInput6?.removeFromSuperview()
            }
        }
        
        cameraButton.contentMode = .scaleToFill;
        cameraButton.imageEdgeInsets = UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10)
        
        // Set first responder if periodic so keyboard stays up
        if meterReadBarTitle == "periodicMeterReadBar" {
            setFirstResponder()
        }
    }
    
    // Sets first responder based on read direction
    func setFirstResponder() {
        if readDirection == "Left to Right" {
            meterReadInput1.becomeFirstResponder()
        } else {
            // Based on number of dials
            if numberOfDials == 6 {
                meterReadInput6?.becomeFirstResponder()
            } else if numberOfDials == 5 {
                meterReadInput5?.becomeFirstResponder()
            } else if numberOfDials == 4 {
                meterReadInput4?.becomeFirstResponder()
            }
        }
    }
    
    // Adds custom action to each field
    func addTargets() {
        meterReadInput1?.addTarget(self, action: #selector(jumpAndValidate(_:)), for: .editingChanged)
        meterReadInput2?.addTarget(self, action: #selector(jumpAndValidate(_:)), for: .editingChanged)
        meterReadInput3?.addTarget(self, action: #selector(jumpAndValidate(_:)), for: .editingChanged)
        meterReadInput4?.addTarget(self, action: #selector(jumpAndValidate(_:)), for: .editingChanged)
        if numberOfDials == 5 {
            meterReadInput5?.addTarget(self, action: #selector(jumpAndValidate(_:)), for: .editingChanged)
        } else if numberOfDials == 6 {
            meterReadInput5?.addTarget(self, action: #selector(jumpAndValidate(_:)), for: .editingChanged)
            meterReadInput6?.addTarget(self, action: #selector(jumpAndValidate(_:)), for: .editingChanged)
        }
    }
    
    // Called after editingChanged on each meter read input
    @objc func jumpAndValidate(_ meterInput: UITextField) {
        meterReadInputDidChange(meterInput)
        validateFields()
    }
    
    // Validation for each meter read input, depending on number of dials
    func validateFields(){
        
        // Only meters with 4-6 dials because that's what currently exists in the system
        if numberOfDials == 4 { // either every box has value or none of them has value
            
            if (!(meterReadInput1.text?.isEmpty)!
                && !(meterReadInput2.text?.isEmpty)!
                && !(meterReadInput3.text?.isEmpty)!
                && !(meterReadInput4.text?.isEmpty)!) ||
                ((meterReadInput1.text?.isEmpty)!
                    && (meterReadInput2.text?.isEmpty)!
                    && (meterReadInput3.text?.isEmpty)!
                    && (meterReadInput4.text?.isEmpty)!) {
                
                meterReadBarValid = true
            }
            else {  // partial filled
                meterReadBarValid = false
            }
            
        } else if numberOfDials == 5 {
            
            if (!(meterReadInput1.text?.isEmpty)!
                && !(meterReadInput2.text?.isEmpty)!
                && !(meterReadInput3.text?.isEmpty)!
                && !(meterReadInput4.text?.isEmpty)!
                && !(meterReadInput5.text?.isEmpty)!) ||
                ((meterReadInput1.text?.isEmpty)!
                    && (meterReadInput2.text?.isEmpty)!
                    && (meterReadInput3.text?.isEmpty)!
                    && (meterReadInput4.text?.isEmpty)!
                    && (meterReadInput5.text?.isEmpty)!) {
                
                meterReadBarValid = true
            }
            else {
                meterReadBarValid = false
            }
            
        } else {
            
            if (!(meterReadInput1.text?.isEmpty)!
                && !(meterReadInput2.text?.isEmpty)!
                && !(meterReadInput3.text?.isEmpty)!
                && !(meterReadInput4.text?.isEmpty)!
                && !(meterReadInput5.text?.isEmpty)!
                && !(meterReadInput6.text?.isEmpty)!) ||
                ((meterReadInput1.text?.isEmpty)!
                    && (meterReadInput2.text?.isEmpty)!
                    && (meterReadInput3.text?.isEmpty)!
                    && (meterReadInput4.text?.isEmpty)!
                    && (meterReadInput5.text?.isEmpty)!
                    && (meterReadInput6.text?.isEmpty)!) {
                
                meterReadBarValid = true
            }
            else {
                meterReadBarValid = false
            }
        }
        
        delegate?.barDidValidate(meterReadBarValid, meterReadBarTitle)
    }
    
    // Finds next responder
    func shiftFocus(meterReadInput: UITextField, forward: Bool) {
        
        var shiftDirection: Int
        
        if readDirection == "Left to Right" {
            if forward {
                shiftDirection = 1
            } else {
                shiftDirection = -1
            }
        } else {
            if forward {
                shiftDirection = -1
            } else {
                shiftDirection = 1
            }
        }
        
        if let nextField = meterReadInput.superview?.viewWithTag(meterReadInput.tag + shiftDirection) as? UITextField {
            nextField.becomeFirstResponder()
            
            // selects the contents of the field, if anything is already in it
            nextField.selectedTextRange = nextField.textRange(from: nextField.beginningOfDocument, to: nextField.endOfDocument)
        }
        
    }
    
    func meterReadInputDidChange(_ currentMeterReadInput: UITextField) {
        if (currentMeterReadInput.text?.isEmpty)! {
            shiftFocus(meterReadInput: currentMeterReadInput, forward: false)
        } else {
            shiftFocus(meterReadInput: currentMeterReadInput, forward: true)
        }
    }
    
    @IBAction func didTapCameraButton(_ sender: UIButton) {
        delegate?.tappedCameraButton(meterReadBarTitle)
    }
    
    
    func enableMeterReadBar() {
        
        // Update UI to enable second meter read bar
        meterReadBarBackground.backgroundColor = GlobalConstants.DARK_GRAY
        cameraButton.backgroundColor = GlobalConstants.GREEN
        
        // Enable all meter read inputs
        meterReadInput1.isEnabled = true
        meterReadInput2.isEnabled = true
        meterReadInput3.isEnabled = true
        meterReadInput4.isEnabled = true
        if numberOfDials == 5 {
            if let input5 = meterReadInput5 {
                input5.isEnabled = true
            }
        } else if numberOfDials == 6 {
            if let input5 = meterReadInput5 {
                input5.isEnabled = true
            }
            if let input6 = meterReadInput6 {
                input6.isEnabled = true
            }
        }
        cameraButton.isEnabled = true
        
        // Adding custom action to each field
        addTargets()
        
    }
    
    func disableMeterReadBar(showMeterReadEntry: Bool? = false) {
        // Update UI to enable second meter read bar
        meterReadBarBackground.backgroundColor = GlobalConstants.LIGHT_GRAY
        cameraButton.backgroundColor = GlobalConstants.LIGHT_GREEN
        
        //Set placeholder text to current meter read if param is true
        if showMeterReadEntry == true {
            meterReadInput1.placeholder = meterReadInput1.text!
            meterReadInput2.placeholder = meterReadInput2.text!
            meterReadInput3.placeholder = meterReadInput3.text!
            meterReadInput4.placeholder = meterReadInput4.text!
            if numberOfDials == 5 {
                if let input5 = meterReadInput5 {
                    input5.placeholder = input5.text!
                }
            } else if numberOfDials == 6 {
                if let input5 = meterReadInput5 {
                    input5.placeholder = input5.text!
                }
                if let input6 = meterReadInput6 {
                    input6.placeholder = input6.text!
                }
            }
            clearMeterReadEntry()
        }
        
        // Disable all meter read inputs
        meterReadInput1.isEnabled = false
        meterReadInput2.isEnabled = false
        meterReadInput3.isEnabled = false
        meterReadInput4.isEnabled = false
        if numberOfDials == 5 {
            if let input5 = meterReadInput5 {
                input5.isEnabled = false
            }
        } else if numberOfDials == 6 {
            if let input5 = meterReadInput5 {
                input5.isEnabled = false
            }
            if let input6 = meterReadInput6 {
                input6.isEnabled = false
            }
        }
        cameraButton.isEnabled = false
    }
    
    func getMeterReadEntry() -> Int? {
        
        var meterReadEntry: Int? = nil
        let input1 = meterReadInput1?.text ?? ""
        let input2 = meterReadInput2?.text ?? ""
        let input3 = meterReadInput3?.text ?? ""
        let input4 = meterReadInput4?.text ?? ""
        let input5 = meterReadInput5?.text ?? ""
        let input6 = meterReadInput6?.text ?? ""
        
        if numberOfDials == 4 {
            meterReadEntry = Int(input1 + input2 + input3 + input4)
        } else if numberOfDials == 5 {
            meterReadEntry = Int(input1 + input2 + input3 + input4 + input5)
        } else {
            meterReadEntry = Int(input1 + input2 + input3 + input4 + input5 + input6)
        }
        
        return meterReadEntry
    }
    
    func getMeterReadEntryString() -> String {
        
        var meterReadEntry: String = ""
        
        if numberOfDials == 4 {
            meterReadEntry = meterReadInput1.text! + meterReadInput2.text! + meterReadInput3.text! + meterReadInput4.text!
        } else if numberOfDials == 5 {
            meterReadEntry = meterReadInput1.text! + meterReadInput2.text! + meterReadInput3.text! + meterReadInput4.text! + meterReadInput5.text!
        } else {
            let meterReadEntry1 =  meterReadInput1.text ?? ""
            let meterReadEntry2 =  meterReadInput2.text ?? ""
            let meterReadEntry3 =  meterReadInput3.text ?? ""
            let meterReadEntry4 =  meterReadInput4.text ?? ""
            let meterReadEntry5 =  meterReadInput5.text ?? ""
            meterReadEntry  =  "\(meterReadEntry1)\(meterReadEntry2)\(meterReadEntry3)\(meterReadEntry4)\(meterReadEntry5)\(meterReadInput6.text ?? "")"
            
        }
        
        return meterReadEntry
        
    }
    
    // Clear first meter bar textfields
    func clearMeterReadEntry() {
        meterReadInput1.text = nil
        meterReadInput2.text = nil
        meterReadInput3.text = nil
        meterReadInput4.text = nil
        if numberOfDials == 5 {
            if let input5 = meterReadInput5 {
                input5.text = nil
            }
        } else if numberOfDials == 6 {
            if let input5 = meterReadInput5 {
                input5.text = nil
            }
            if let input6 = meterReadInput6 {
                input6.text = nil
            }
        }
    }
    
    func populateWithValues(_ reading: Int?){
        
        if (reading == nil) { return }
        
        var charArr = Array(String(describing: reading!))
        
        while charArr.count < (numberOfDials) {
            charArr.insert("0", at: 0)
        }
        
        var currentIndex = (numberOfDials) - 1
        
        if numberOfDials >= 6 {
            meterReadInput6.text = String(charArr[currentIndex])
            currentIndex -= 1
        }
        if numberOfDials >= 5 {
            meterReadInput5.text = String(charArr[currentIndex])
            currentIndex -= 1
        }
        if numberOfDials >= 4 {
            meterReadInput4.text = String(charArr[currentIndex])
            currentIndex -= 1
        }
        
        meterReadInput3.text = String(charArr[currentIndex])
        currentIndex -= 1
        meterReadInput2.text = String(charArr[currentIndex])
        currentIndex -= 1
        meterReadInput1.text = String(charArr[currentIndex])
        currentIndex -= 1
        
    }
    
}
