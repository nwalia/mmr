//
//  PeriodicOutcardScreenViewController.swift
//  Enbridge Meter Reader
//
//  Created by Abramsky, Lauren (CA - Toronto) on 2018-01-18.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

protocol PeriodicOutcardDelegate: class {
    func populateCustomerInformation(onDashboard: Bool, hideLine: Bool, newRead: MeterRead, buttonsEnabled: Bool)
}

class PeriodicOutcardScreenViewController: UIViewController, UITextFieldDelegate, MeterReadBarDelegate, CustomerInformationCellDelegate {
    
    @IBOutlet var screenView: UIView!
    @IBOutlet weak var progressBar: ProgressBarView!
    @IBOutlet weak var customerInformation: CustomerInformation!
    @IBOutlet var specialInstructionsStackView: UIStackView!
    @IBOutlet weak var specialInstructionsText: UILabel!
    @IBOutlet weak var troubleCodeButton: UIButton!
    @IBOutlet weak var skipCodeButton: UIButton!
    @IBOutlet weak var specialInstructionsButton: UIButton!
    @IBOutlet weak var meterReadBar: MeterReadBarView!
    @IBOutlet weak var nextMeterButton: UIButton!
    @IBOutlet weak var marginView: UIView!
    
    var currentMeterRead: MeterRead!
    var filteredMeterReads: [MeterRead]?
    var meterReadsOverflow: Bool = false
    var nextMeterButtonEnabled: Bool = false
    var hasEnteredTroubleCodes: Bool = false
    var meterReadBarValid: Bool = false
    var specialInstructions: String = "This is an existing special instruction."
    var numberOfDials: Int!
    var hasEnteredSkipCode: Bool = false
    var meterReadEntry: Int!
    var disabledBar: Bool = false
    var badReadCount: Int = 0
    var continueAction: UIAlertAction!
    var changedMeterNumber: Bool = false
    var readDirection: String!
    var lowParameter: Int!
    var highParameter: Int!
    var tapNextMeterButtonCount: Int = 0
    var firstMeterReadEntry: Int!
    var secondMeterReadEntry: Int!
    var shouldMoveToNextMeter: Bool = false
    var callErrorAlert: Bool = false
    var firstTroubleCodeNumber: String?
    var secondTroubleCodeNumber: String?
    
    // Cointainers for codes
    var firstTroubleCode: TroubleCode?
    var secondTroubleCode: TroubleCode?
    var skipCode: SkipCode?
    
    // Storing images
    var wantsImageForMeter1: Bool = false
    var meterImage1: UIImage?
    var wantsImageForError: Bool = false
    var errorMeterImage: UIImage?
    
    // Digit text fields in error pop up
    var digitTextField1: NumericKeypadTextField!
    var digitTextField2: NumericKeypadTextField!
    var digitTextField3: NumericKeypadTextField!
    
    // Blur effect for alert
    var visualEffectView: UIVisualEffectView!
    
    // Keeping track of previous delegate for reassignment when segueing to Dashboard
    var previousDelegate: PeriodicDashboardDelegate? // Will need to change this to periodic dashboard
    
    var finishedCameraSegue = false
    
    // Delegate for Periodic Outcard Screen
    static weak var delegate: PeriodicOutcardDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Persistence.shared.saveRoutes()
        
        let meterReads = filteredMeterReads != nil ? filteredMeterReads : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads
        
        // Set variables for meter read 
        if currentMeterRead == nil {
            currentMeterRead = meterReads![0]
        }
        lowParameter = currentMeterRead.lowParameter
        highParameter = currentMeterRead.highParameter 
        numberOfDials = currentMeterRead.numberOfDials
        
        badReadCount = 0
        
        // Set up bars
//        meterReadBar.setUp(meterReadBar, "periodicMeterReadBar", numberOfDials)
//        meterReadBar.populateWithValues(currentMeterRead.reading)
        
        // Set up periodic/outcard specific UI (next meter button, skip codes, progress bar)
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .PERIODIC {
            // Periodic route
            progressBar.populate(routeKey: Model.selectedRouteKey, type: .periodic)
            if currentMeterRead.reading == nil && currentMeterRead.skipCode == nil{ //has reading
                disableButton()
            }
        }
        else {
            // Outcard route
            enableButton()
            skipCodeButton.isHidden = true
            progressBar.populate(routeKey: Model.selectedRouteKey, type: .outcard)
        }
        

        // Prepopulate trouble codes
        populateTroubleCodes()
        
        // Prepopulate skip codes
        populateSkipCodes()
        
        // Set trouble code / special instructions buttons text to yellow if values have been selected
        if hasEnteredTroubleCodes {
            troubleCodeButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
        } else {
            troubleCodeButton.setTitleColor(.white, for: .normal)
        }
        
        if currentMeterRead.specialInstructions != nil && currentMeterRead.specialInstructions! != "" {
            specialInstructionsButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
            view.addSubview(specialInstructionsStackView)
            //Update constraints for special instruction
            NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = false
            NSLayoutConstraint(item: specialInstructionsStackView as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 24.0).isActive = true
            NSLayoutConstraint(item: specialInstructionsStackView as Any, attribute: .leading, relatedBy: .equal, toItem: marginView, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
            NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: specialInstructionsStackView, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = true
        } else {
            
            //Special instructions empty, so remove title
            specialInstructionsButton.setTitleColor(UIColor.white, for:.normal)
            specialInstructionsStackView.removeFromSuperview()
            NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = true
            
            // Add to dictionary, so pop up doesn't show if user adds special instructions
            GlobalVariables.specialInstructionsDict[currentMeterRead.fullMeterId] = true
        }
        
        // Show special instructions on screen
        specialInstructionsText.text = currentMeterRead.specialInstructions
        
        // Set completion code text colours for disabled and normal states
        nextMeterButton.setTitleColor(GlobalConstants.DARK_GRAY, for: .normal)
        nextMeterButton.setTitleColor(UIColor.gray, for: .disabled)
        
        // Get read direction
        readDirection = UserDefaults.standard.object(forKey: "ReadDirection") as? String
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // Populate customer information - assigned delegate in awake from nib in Customer Information swift file
        PeriodicOutcardScreenViewController.delegate?.populateCustomerInformation(onDashboard: false, hideLine: false, newRead: currentMeterRead!, buttonsEnabled: true)
        
        CustomerInformation.delegate = self
        meterReadBar.delegate = self
        
        
        // If meter number has been changed, update with new number of dials
        if let newNumDials = currentMeterRead.newMeterNumberOfDials {
            meterReadBar.setUp(meterReadBar, "periodicMeterReadBar", newNumDials)
        } else {
            meterReadBar.setUp(meterReadBar, "periodicMeterReadBar", currentMeterRead.numberOfDials)
        }
        
        // Call awakeFromNib to re-initialize meter read bar, to handle moving to screens with less/more number of dials
        meterReadBar.awakeFromNib()
        self.meterReadBar.subviews.enumerated().forEach{$0 < self.meterReadBar.subviews.count ? $1.removeFromSuperview() : print("Remaining View: \($1.description)")}
        
//        Disable meter read bar if there has been an error photo submitted for this meter read.
//        if currentMeterRead.hasErrorPhoto || currentMeterRead.skipCode != nil{
//            meterReadBar.disableMeterReadBar()
//        }else {
//            meterReadBar.enableMeterReadBar()
//        }
        
        
        //if SkipCode is set, meter read bar needs to be disabled
        if currentMeterRead.skipCode != nil {
            meterReadBar.clearMeterReadEntry()
            currentMeterRead.reading = nil
        }else {
            // If reading exists, populate bar
            if currentMeterRead.reading != nil {
                meterReadBar.populateWithValues(currentMeterRead.reading)
            } else if currentMeterRead.newMeterRead != nil && currentMeterRead.newMeterRead != "" {
                meterReadBar.populateWithValues(Int(currentMeterRead.newMeterRead!))
            }
        }
        
        // Special instructions pop up - add alert if there is an existing service instruction
        if currentMeterRead.specialInstructions != nil
            && currentMeterRead.specialInstructions != ""
            && !checkGlobalDictionary(currentMeterRead.fullMeterId) {
            AudioServicesPlaySystemSound(1315);
            let alert = UIAlertController(title: "Special Instructions", message: "\n\(currentMeterRead.specialInstructions ?? "")", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Acknowledge", style: .default, handler: {(alert: UIAlertAction!) in self.acknowledge()}))
            self.present(alert, animated: true, completion: nil)
        }
        
        //sync meter status with Click
        currentMeterRead.status = Utilities.sharedInstance.checkForNetworkStatus(currentMeterRead)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        if shouldMoveToNextMeter {
            moveToNextMeter()
            shouldMoveToNextMeter = false
        }
        
        if meterReadsOverflow && finishedCameraSegue {
            performSegue(withIdentifier: "routeCompleted", sender: nil)
            finishedCameraSegue = false
        }
    }
    
//    override func willMove(toParentViewController parent: UIViewController?)
//    {
//        super.willMove(toParentViewController: parent)
//        if parent == nil
//        {
//            //print("This VC is 'will' be popped. i.e. the back button was pressed.")
//            OfflineHelper().uploadtoOfflineStore(currentMeter: currentMeterRead)
//        }
//    }

    
    // If meter read bar is valid or skip code entered, enable next meter button
    func barDidValidate(_ isValid: Bool, _ title: String) {
       
        if isValid {
            if let reading = meterReadBar.getMeterReadEntry() {
                currentMeterRead.reading = reading
                currentMeterRead.updateReadDate()
            }
        }
        
        if isValid || hasEnteredSkipCode {
            enableButton()
            
        } else {
            disableButton()
        }
    }
    
    // Segue to camera screen when camera button pressed in customer info xib
    func tappedCameraButton(_ title: String) {
        // If camera button pressed on the first meter bar, put image in slot 1
        wantsImageForMeter1 = true
        performSegue(withIdentifier: "toOCR", sender: nil)
    }
    
    @IBAction func troubleCodeButtonClicked(_ sender: UIButton) {
        NotesViewController.caller = self
        CameraCaptureViewController.fileName = self.currentMeterRead.jobID
        CameraCaptureViewController.zindicator = self.currentMeterRead.checkForWorkOrderType(self.currentMeterRead)
    }
    
    // Will need this function when hooking up periodic meter read screen with periodic dashboard
    override func didMove(toParent parent: UIViewController?) {
     // Reassign delegate if parent is Dashboard
     // (no "if" in here yet, may need to add logic if moving to another screen)
     PeriodicDashboardViewController.delegate = previousDelegate
     
     }
    
    // Enable completion code button
    func enableButton() {
        if !(nextMeterButtonEnabled){
            nextMeterButtonEnabled = true
            nextMeterButton.isEnabled = true
            nextMeterButton.backgroundColor = GlobalConstants.YELLOW
        }
    }
    
    // Disable completion code button
    func disableButton() {
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .OUTCARD {
            return
        }
        nextMeterButtonEnabled = false
        nextMeterButton.isEnabled = false
        nextMeterButton.backgroundColor = GlobalConstants.LIGHT_YELLOW
    }
    
    // Empty method to dismiss acknowledge pop up
    func acknowledge() {
    }
    
    // Check if this meter pop up has been seen by this user before, in this session
    func checkGlobalDictionary(_ meterID: String) -> Bool {
        if GlobalVariables.specialInstructionsDict[meterID] != nil {
            // Value in dictionary, don't show pop up
            return true
        } else {
            // Add to dictionary
            GlobalVariables.specialInstructionsDict[meterID] = true
            return false
        }
    }
    
    // Editing special instructions
    @IBAction func openSpecialInstructionsNotes(_ sender: UIButton) {
        let sb = UIStoryboard(name: "NotesView", bundle: nil)
        let notesVC = sb.instantiateInitialViewController() as! NotesViewController
        notesVC.title = "Special Instructions"
        notesVC.specialInstructionsPassed = currentMeterRead.specialInstructions
        NotesViewController.caller = self
        self.show(notesVC, sender: self)
    }
    
    //MARK: - Navigation
    @IBAction func unwindToHome(segue: UIStoryboardSegue){
        
        if let sourceVC = segue.source as? CameraCaptureViewController{
            //print("Unwound from camera")

            if let imageFromView = sourceVC.image {
                if wantsImageForError {
                    
                    // Save image after second bad read (high/low parameters)
                    //print("assigned image to error slot")
                    errorMeterImage = imageFromView
                    currentMeterRead.hasErrorPhoto = true
                    wantsImageForError = false
                    finishedCameraSegue = true
                    
                    // Move to next meter after bad read image taken
                    shouldMoveToNextMeter = true
                    disabledBar = false
                    
                } else {
                    //print("came from camera")
                }

                // TODO: Support the post-exchange camera option (meterImage2)
            }
        }
        
        if let sourceVC = segue.source as? OCRViewController {
            if let val_Read = sourceVC.valRead {
                if wantsImageForMeter1 && val_Read > -1 {
                    meterReadBar.clearMeterReadEntry()
                    currentMeterRead.reading = val_Read
                    meterReadBar.populateWithValues(val_Read)
                    meterReadBar.validateFields()
                    barDidValidate(meterReadBar.meterReadBarValid, "")
                    currentMeterRead.updateReadDate()
                }
            }
        }
        
        else if segue.source is NotesViewController {
            if let senderVC = segue.source as? NotesViewController {
                
                // Save note as new special instructions, and update UI to reflect changes
                if let note = senderVC.note{
                    // If special instructions (dev lock note) have changed, update boolean so that meter will be sent as "FIELD COMPLETE" (not "MISSED")
                    if note != currentMeterRead.originalSpecialMessage {
                        currentMeterRead.didChangeSpecialMessage = true
                    } else {
                        currentMeterRead.didChangeSpecialMessage = false
                    }
                    currentMeterRead.specialInstructions = note
                    currentMeterRead.updateReadDate()
                    specialInstructionsText.text = currentMeterRead.specialInstructions
                    
                    // If blank special instructions note, remove from UI. Otherwise, add items back.
                    if note == "" {
                        
                        //Update UI and constraints
                        specialInstructionsButton.setTitleColor(UIColor.white, for:.normal)
                        specialInstructionsStackView.removeFromSuperview()
                        NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = true
                        
                    } else {
                        
                        //Update UI
                        specialInstructionsButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
                        view.addSubview(specialInstructionsStackView)
                        
                        //Update constraints
                        NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = false
                        NSLayoutConstraint(item: specialInstructionsStackView as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 24.0).isActive = true
                        NSLayoutConstraint(item: specialInstructionsStackView as Any, attribute: .leading, relatedBy: .equal, toItem: marginView, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
                        NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: specialInstructionsStackView, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = true
                    }
                }
                else{
                    //print("Returned without note")
                }
                
                // Integrations would go here!
                
            }
        }
        else if segue.source is ChangeMeterNumberViewController {
            let sourceVC = segue.source as! ChangeMeterNumberViewController
            
//            //print(sourceVC.newMeterNumberField.text!)
//            //print(sourceVC.meterSizeCodeField.text!)
//            //print(sourceVC.numberOfDialsField.text!)
            
            let newNumDials = sourceVC.numberOfDialsField.text
            if newNumDials != "" {
                currentMeterRead.newMeterNumberOfDials = Int(newNumDials!)
                
                // Call awakeFromNib to re-initialize meter read bar, to handle moving to screens with less/more number of dials
                meterReadBar.awakeFromNib()
                
                // Update  meter read bar with new number of dials
                meterReadBar.setUp(meterReadBar, "firstMeterReadBar", Int(newNumDials!)!)
                
            }
            currentMeterRead.newSizeCode = sourceVC.meterSizeCodeField.text!
            currentMeterRead.newMeterNumber = sourceVC.newMeterNumberField.text!
            currentMeterRead.updateReadDate()
            PeriodicOutcardScreenViewController.delegate?.populateCustomerInformation(onDashboard: false, hideLine: false, newRead: currentMeterRead!, buttonsEnabled: true)
        }
        else if segue.source is ChangeMeterLocationViewController {
            let sourceVC = segue.source as! ChangeMeterLocationViewController
            
            // If location code changed, update boolean so that meter will be sent as "FIELD COMPLETE" (not "MISSED")
            let sourceConfirmedCode = sourceVC.confirmedCode! as LocationCode
            if currentMeterRead.originalLocation != nil {
                if sourceConfirmedCode.number != currentMeterRead.originalLocation?.number {
                    currentMeterRead.didChangeLocation = true
                } else {
                    currentMeterRead.didChangeLocation = false
                }
            }
            
            currentMeterRead.locationCode = sourceConfirmedCode
            PeriodicDashboardViewController.currentMeterLocation = currentMeterRead.locationCode
            currentMeterRead.updateReadDate()
            
            PeriodicOutcardScreenViewController.delegate?.populateCustomerInformation(onDashboard: false, hideLine: false, newRead: currentMeterRead!, buttonsEnabled: true)
        }
        else if segue.source is SelectTroubleCodeViewController {
            let sourceVC = segue.source as! SelectTroubleCodeViewController
            
            troubleCodeButton.setTitle((sourceVC.formattedCodeString ?? "Trouble Code"), for: UIControl.State.normal)
            
            
            if sourceVC.returnedCodes?.count != 0 {
                currentMeterRead.troubleCodes = sourceVC.returnedCodes!
                currentMeterRead.updateReadDate()
                hasEnteredTroubleCodes = true
                troubleCodeButton.setTitleColor(GlobalConstants.YELLOW, for: UIControl.State.normal)
                if sourceVC.returnedCodes?.count == 1 {
                    firstTroubleCode = sourceVC.returnedCodes?[0]
                    secondTroubleCode = nil
                }
                else if sourceVC.returnedCodes?.count == 2 {
                    firstTroubleCode = sourceVC.returnedCodes?[0]
                    secondTroubleCode = sourceVC.returnedCodes?[1]
                }
            } else {
                currentMeterRead.troubleCodes = [TroubleCode]()
                hasEnteredTroubleCodes = false
                troubleCodeButton.setTitleColor(.white, for: UIControl.State.normal)
                firstTroubleCode = nil
                secondTroubleCode = nil
                currentMeterRead.updateReadDate()
            }
            
        }
            
        else if segue.source is SelectCompletionCodeViewController {
            _ = segue.source as! SelectCompletionCodeViewController
            
            //print("Unwound with code \(sourceVC.selectedCode.number)")
            
            //print("Note: \(sourceVC.returnNote ?? "none"), Number of dials: \(sourceVC.newMeterNumberOfDials ?? 0), Meter number: \(sourceVC.newMeterNumber ?? "none"), Size code: \(sourceVC.newMeterSizeCode ?? "none")")
        }
            
        else if segue.source is SelectSkipCodeViewController {
            let sourceVC = segue.source as! SelectSkipCodeViewController
//            //print(sourceVC.confirmedCode?.id ?? "No codes selected")
            
            if sourceVC.confirmedCode == nil {
                hasEnteredSkipCode = false
                currentMeterRead.skipCode = nil
                skipCodeButton.setTitle("Skip Code", for: UIControl.State.normal)
                skipCodeButton.setTitleColor(.white, for: UIControl.State.normal)
                
            } else {
                hasEnteredSkipCode = true
                skipCodeButton.setTitle("Skip Code: \((sourceVC.confirmedCode?.id)!)", for: UIControl.State.normal)
                skipCodeButton.setTitleColor(GlobalConstants.YELLOW, for: UIControl.State.normal)
                currentMeterRead.skipCode = sourceVC.confirmedCode
            }
            
            currentMeterRead.updateReadDate()
            
            if hasEnteredSkipCode {
                enableButton()
                currentMeterRead.status = .completed
            } else {
                currentMeterRead.status = .available
                disableButton()
            }
        }
    }
    
    // When segueing to meter exchange, send over current meter read
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination is ChangeMeterNumberViewController {
            let destinationVC = segue.destination as? ChangeMeterNumberViewController
            destinationVC?.selectedMeter = currentMeterRead
            
        } else if segue.destination is SelectTroubleCodeViewController {
            let destinationVC = segue.destination as? SelectTroubleCodeViewController
            var troubleCodes: [TroubleCode] = []
            if let firstCode = firstTroubleCode {
                troubleCodes.append(firstCode)
            }
            if let secondCode = secondTroubleCode {
                troubleCodes.append(secondCode)
            }
            destinationVC?.returnedCodes = troubleCodes
            
        }
        else if segue.destination is RouteSummaryViewController{
            if RouteSummaryViewController.caller != "periodicCellSummary" {
                 RouteSummaryViewController.caller = "OutcardCell"
            }
        }
            
        else if segue.identifier == "toSkipCode" {
            let destinationVC = segue.destination as? SelectSkipCodeViewController
            destinationVC?.confirmedCode = currentMeterRead.skipCode
        }
        
        else if segue.identifier == "toOCR" {
            let destinationVC = segue.destination as? OCRViewController
            
            //to make the OCR work like offcycle high low should be set to nil, hence commenting the below two lines
            
//            destinationVC?.lowValue = currentMeterRead.lowParameter
//            destinationVC?.highValue = currentMeterRead.highParameter
            
            
            destinationVC?.numDigits = currentMeterRead.numberOfDials
        }
    }
    
    // Segue to meter number exchange when edit meter number button pressed in customer info xib
    func editMeterNumberButtonPressed() {
        performSegue(withIdentifier: "ToChangeMeterNumber", sender: self)
    }
    
    // Segue to meter location list when edit meter location button pressed in customer info xib
    func editMeterLocationButtonPressed() {
        performSegue(withIdentifier: "ToChangeMeterLocation", sender: self)
    }
    
    func populateTroubleCodes(){
        
        if currentMeterRead.troubleCodes.count == 2 {
            firstTroubleCode = currentMeterRead.troubleCodes[0]
            secondTroubleCode = currentMeterRead.troubleCodes[1]
            hasEnteredTroubleCodes = true
            troubleCodeButton.setTitleColor(GlobalConstants.YELLOW, for: UIControl.State.normal)
        }
        else if currentMeterRead.troubleCodes.count == 1 {
            firstTroubleCode = currentMeterRead.troubleCodes[0]
            secondTroubleCode = nil
            hasEnteredTroubleCodes = true
            troubleCodeButton.setTitleColor(GlobalConstants.YELLOW, for: UIControl.State.normal)
        }
        else{
            firstTroubleCode = nil
            secondTroubleCode = nil
            hasEnteredTroubleCodes = false
            troubleCodeButton.setTitleColor(.white, for: UIControl.State.normal)
        }
        
        let codeString = createFormattedCodeString(codes: currentMeterRead.troubleCodes)
        
        troubleCodeButton.setTitle((codeString ?? "Trouble Code"), for: UIControl.State.normal)
        
    }
    
    func createFormattedCodeString(codes: [TroubleCode]) -> String? {
        
        if codes.count == 1 {
            return "Trouble Code: \(String(format: "%02d", codes[0].number))"
            
        }
        else if codes.count == 2 {
            return "Trouble Code: \(String(format: "%02d", codes[0].number))/\(String(format: "%02d", codes[1].number))"
        }
        else {
            return nil
        }
    }
    
    func populateSkipCodes() {
        if currentMeterRead.skipCode == nil {
            hasEnteredSkipCode = false
            skipCodeButton.setTitle("Skip Code", for: UIControl.State.normal)
            skipCodeButton.setTitleColor(.white, for: UIControl.State.normal)
        } else {
            hasEnteredSkipCode = true
            skipCodeButton.setTitle("Skip Code: \((currentMeterRead.skipCode?.id)!)", for: UIControl.State.normal)
            skipCodeButton.setTitleColor(GlobalConstants.YELLOW, for: UIControl.State.normal)
            
        }
    }
    
    
    // ---- EN-886: Set BOOL variable from config file & if it is True, make it as acceptable in case of High/Low ---- //
    @IBAction func didNextMeterButton(_ sender: UIButton) {
        
        // --- Getting value of userHighLow: getting only 2 values from sharedInstance (True or nil)  ---- //
        let userRoleHighLow = Utilities.sharedInstance.userRoleHighLow ?? false
        print(userRoleHighLow)
        
        shouldMoveToNextMeter = true

        // Check if user changed meter number
        if currentMeterRead.newMeterNumber == nil || currentMeterRead.newMeterNumber == "" {
            changedMeterNumber = false
        } else {
            changedMeterNumber = true
            
            // If changed meter number, audit count will always be 0 because there are no high/low params
            currentMeterRead.auditReadCount = 0
        }
        
        // Get meter read entry
        meterReadEntry = meterReadBar.getMeterReadEntry()
        (currentMeterRead.hasErrorPhoto && meterReadEntry == nil) ? (currentMeterRead.reading = currentMeterRead.reading) : (currentMeterRead.reading = meterReadEntry)
        currentMeterRead.updateReadDate()
        
        // PERIODIC: If meter number has been changed, skip code selected, or meter read bar is disabled, user can go to next meter
        // OUTCARD: If meter number changed or has not entered a meter reading, user can go to next meter
        if (Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .PERIODIC && !hasEnteredSkipCode && !disabledBar && !changedMeterNumber)
            || (Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .OUTCARD && !changedMeterNumber && meterReadEntry != nil) {
            
            // Only check against non nil parameters
            // ---- Make all code changes over here --- //

            if highParameter != nil && lowParameter != nil && userRoleHighLow {
                
                // Increase tap next meter button count
                tapNextMeterButtonCount += 1

                if self.errorMeterImage == nil {
                    
                    // If high/low parameters not met (check for rollover), go through error alert flow
                    if highParameter >= lowParameter {
                        
                        // If high > low, check if entry is outside low to high range (bad read)
                        if (meterReadEntry < lowParameter || meterReadEntry > highParameter) {
                            shouldMoveToNextMeter = false
                            increaseBadReadCount()
                            currentMeterRead.reading = nil
                            currentMeterRead.updateReadDate()
                        }
                        
                    } else {
                        
                        // Else low > high (rollover), check if read is inside high to low range (bad read)
                        if (meterReadEntry > highParameter && meterReadEntry < lowParameter) {
                            shouldMoveToNextMeter = false
                            increaseBadReadCount()
                            currentMeterRead.reading = nil
                            currentMeterRead.updateReadDate()
                        }
                    }
                }
            }
            
            // Audit counter for only periodic, not outcard
            if currentMeterRead.type == "Periodic" {
                currentMeterRead.auditReadCount = badReadCount
            } else {
                currentMeterRead.auditReadCount = 0
            }
            
            //print("! type & auditReadCount: \(currentMeterRead.type!) & \(currentMeterRead.auditReadCount!)")
            
        }
        
        // Assign entry in meter read bar as appropriate reading
        if let reading = meterReadEntry {
            
            // If meter number has not changed, assign as "reading"
            // Else, as assign reading as "new meter reading"
            
            if !changedMeterNumber || currentMeterRead.newMeterNumber == nil || (currentMeterRead.newMeterNumber ?? "").isEmpty {
                currentMeterRead.reading = reading
                currentMeterRead.updateReadDate()
                currentMeterRead.newMeterRead = ""
            } else {
                currentMeterRead.newMeterRead = meterReadBar.getMeterReadEntryString()
                currentMeterRead.reading = nil
                currentMeterRead.updateReadDate()
            }
        }
        
        // Move to next meter
        if shouldMoveToNextMeter {
            moveToNextMeter()
            shouldMoveToNextMeter = false
        }
    }
    
    func moveToNextMeter() {
        
        let meterReads = (filteredMeterReads != nil ? filteredMeterReads : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads)!
        
        // Get current read index in route
        let currentReadIndex = meterReads.firstIndex{
            if $0 === currentMeterRead {
                return true
            }
            return false
        }
        
        // Move to next meter
        if shouldMoveToNextMeter {
            

            if  (meterReadBar.getMeterReadEntry() != nil) || (currentMeterRead.skipCode != nil) || (currentMeterRead.reading != nil && meterReadBar.getMeterReadEntry() != nil ) || notNilAndNotEmpty(item: currentMeterRead.newMeterNumber ?? "") || notNilAndNotEmpty(item: currentMeterRead.newSizeCode ?? "") || (currentMeterRead.newMeterNumberOfDials != nil) || currentMeterRead.didChangeLocation || currentMeterRead.didChangeSpecialMessage {
                
                // Set meter as completed
                self.currentMeterRead.status = .completed
                self.currentMeterRead.completed = true
                self.currentMeterRead.mmrStatus = "FIELD COMPLETE"
                self.currentMeterRead.updateReadDate()
                Model.sharedInstance.checkRouteCompleted(routeId: Model.selectedRouteKey)
                
                errorMeterImage = nil
                
                // Update last completed meter/MRU for outcard
                if Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .OUTCARD {
                    Model.sharedInstance.routes[Model.selectedRouteKey]?.lastMeterCompleted = meterReads[currentReadIndex!].routeId
                    Model.sharedInstance.routes[Model.selectedRouteKey]?.cycleNo = meterReads[currentReadIndex!].cycleNo
                }
                
            } else {
                if currentMeterRead.hasErrorPhoto {
                    if currentMeterRead.mmrStatus == "FIELD COMPLETE" {
                        let popup = UIAlertController(title: "Warning", message: "This Work Order was 'Field Completed' with an implausible read and a meter photo. It can not be changed to 'MISSED'.", preferredStyle: .alert)
                        popup.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { [weak self] _ in
                            self?.currentMeterRead.status = .completed
                            self?.currentMeterRead.completed = true
                            self?.currentMeterRead.mmrStatus = "FIELD COMPLETE"
                            self?.currentMeterRead.updateReadDate()
                            
                            OfflineHelper().uploadtoOfflineStore(currentMeter: self!.currentMeterRead)
                            Persistence.shared.saveRoutes()
                            
                            
                            // Increase index of meter in route
                            if let index = currentReadIndex, index <= meterReads.count && index + 1 < meterReads.count {
                                self?.currentMeterRead = meterReads[(currentReadIndex!) + 1]
                                // Reload page
                                self?.meterReadBar.awakeFromNib()
                                self?.viewDidLoad()
                                self?.viewWillAppear(false)
                            }else{
                                self?.meterReadsOverflow = true
                                self?.performSegue(withIdentifier: "routeCompleted", sender: nil)
                                return
                            }
                        }))
                        
                        self.present(popup,animated: true)
                        return
                        
                    }else{
                    }
                }else{
                    // Set meter as missed
                    self.currentMeterRead.status = .missed
                    self.currentMeterRead.completed = false
                    self.currentMeterRead.mmrStatus = "MISSED"
                }
            }
            
            OfflineHelper().uploadtoOfflineStore(currentMeter: currentMeterRead)
            LocalStoreImage.offlineLS?.uploadToDB()
            Persistence.shared.saveRoutes()
        
            
            // Increase index of meter in route
            if let index = currentReadIndex, index <= meterReads.count && index + 1 < meterReads.count {
                currentMeterRead = meterReads[(currentReadIndex!) + 1]
            } else {
                meterReadsOverflow = true
                return
            }
            
            // Reload page
            meterReadBar.awakeFromNib()
            self.viewDidLoad()
            self.viewWillAppear(false)
            
        }// end should move to next meter
        
    } // end move to next meter func
    
    // Returns if an optional string is not nil and not empty
    func notNilAndNotEmpty(item: String?) -> Bool {
        if item != nil && !(item ?? "").isEmpty {
            return true
        } else {
            return false
        }
    }
    
    // Increase bad read count and take appropriate action based on number of bad reads
    func increaseBadReadCount() {
        // Increase read count of bad reads
        badReadCount += 1
        
        // If first bad read, error alert with enter meter number
        if badReadCount == 1 {
            
            firstMeterReadEntry = meterReadEntry
            view.resignFirstResponder()
            errorAlert()
            
            // If second bad read, pop up directs them to take a photo
        } else if badReadCount == 2 {
            
            secondMeterReadEntry = meterReadEntry
            if firstMeterReadEntry == secondMeterReadEntry {
                badReadCount = 1
            }
            
            disabledBar = true
            meterReadBar.disableMeterReadBar(showMeterReadEntry: true)
            AudioServicesPlaySystemSound(1315);
            let alert = UIAlertController(title: "High/Low Parameters", message: "Your second read is not within the high/low parameters. Please take a picture of the meter to proceed.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Open Camera", style: .default, handler: {(alert: UIAlertAction!) in self.openCamera()}))
            self.present(alert, animated: true, completion: nil)
            
        }
        
        // Reading was within high/low paramaters on second try, set audit count to 1
        if tapNextMeterButtonCount == 2 && secondMeterReadEntry == nil {
            badReadCount = 1
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if  identifier == "routeCompleted" && meterReadsOverflow {
            return true
        }else if identifier == "toTroubleCode" || identifier == "toSkipCode" || identifier == "ToChangeMeterLocation" || identifier == "ToChangeMeterNumber" || identifier == "toOCR" {
            return true
        }
        
        return false
    }
    
    
    // Open camera after second meter read entry does not meet parameters, no back button
    func openCamera() {
        wantsImageForError = true
        let sb = UIStoryboard(name: "CameraCapture", bundle: nil)
        let cameraVC = sb.instantiateInitialViewController() as! CameraCaptureViewController
        cameraVC.seguedFromErrorAlert = true
        CameraCaptureViewController.fileName = self.currentMeterRead.jobID
        CameraCaptureViewController.picreason = "HL"
        CameraCaptureViewController.zindicator = self.currentMeterRead.checkForWorkOrderType(self.currentMeterRead)
        self.show(cameraVC, sender: self)
    }
    
    // Unique jump and validate method for digits, additionally checks when to enable Continue button
    @objc func digitsJumpAndValidate (_ meterInput: UITextField) {
        
        meterReadBar.jumpAndValidate(meterInput)
        
        if digitTextField1.text?.count == 0 || digitTextField2.text?.count == 0 || digitTextField3.text?.count == 0 {
            continueAction.isEnabled = false
        } else {
            continueAction.isEnabled = true
        }
        
    }
    
    // Creates custom 3 digit error alert when meter entry does not meet parameters
    func errorAlert() {
        
        // Create blur effect behind the alert (to hide the meter number)
        let blurEffect = UIBlurEffect(style: .light)
        visualEffectView = UIVisualEffectView(effect: blurEffect)
        visualEffectView.frame = (screenView.superview?.frame)!
        
        AudioServicesPlaySystemSound(1315);
        let alert = UIAlertController(title: "High/Low Parameters", message: "Reading does not meet the high/low parameters. Enter the last 3 digits of the meter number.", preferredStyle: UIAlertController.Style.alert)
        
        // Set up frame and view for 3 digit textfields in alert
        let digitsFrame = CGRect(x:0, y:110, width:270, height:60);
        let digitsView: UIView = UIView(frame: digitsFrame);
        
        // Set up 3 digit textfields
        let digitFrame1 = CGRect(x:30, y:0, width:65, height:60);
        digitTextField1 = NumericKeypadTextField(frame: digitFrame1);
        formatDigitTextField(textField: digitTextField1, placeholder: "0", borderStyle: UITextField.BorderStyle.roundedRect, font: UIFont(name:"Arial-BoldMT", size: 48)!, textAlignment: .center, tag: 1, maxLength: 1);
        
        let digitFrame2 = CGRect(x:105, y:0, width:65, height:60);
        digitTextField2 = NumericKeypadTextField(frame: digitFrame2);
        formatDigitTextField(textField: digitTextField2, placeholder: "0", borderStyle: UITextField.BorderStyle.roundedRect, font: UIFont(name:"Arial-BoldMT", size: 48)!, textAlignment: .center, tag: 2, maxLength: 1);
        
        let digitFrame3 = CGRect(x:180, y:0, width:65, height:60);
        digitTextField3 = NumericKeypadTextField(frame: digitFrame3);
        formatDigitTextField(textField: digitTextField3, placeholder: "0", borderStyle: UITextField.BorderStyle.roundedRect, font: UIFont(name:"Arial-BoldMT", size: 48)!, textAlignment: .center, tag: 3, maxLength: 1);
        
        // Adding custom action to each field
        digitTextField1.addTarget(self, action: #selector(digitsJumpAndValidate(_:)), for: .editingChanged)
        digitTextField2.addTarget(self, action: #selector(digitsJumpAndValidate(_:)), for: .editingChanged)
        digitTextField3.addTarget(self, action: #selector(digitsJumpAndValidate(_:)), for: .editingChanged)
        
        // Add digit textfields as subviews
        digitsView.addSubview(digitTextField1);
        digitsView.addSubview(digitTextField2);
        digitsView.addSubview(digitTextField3);
        
        // Add to alert
        alert.view.addSubview(digitsView);
        
        // Custom alert height to include 3 digit text fields
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view as Any, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 240)
        alert.view.addConstraint(height);
        
        // Create Continue button and add to alert
        continueAction = UIAlertAction(title: "Continue", style: .default, handler: {(alert: UIAlertAction!) in self.errorContinue()})
        alert.addAction(continueAction)
        continueAction.isEnabled = false
        
        view.addSubview(visualEffectView)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // Close alert function that is used throughout error flow alerts, setting first responder
    func close() {
        meterReadBar.setFirstResponder()
    }
    
    // Format alert digit text fields in alert for entering last 3 meter number digits
    func formatDigitTextField(textField: NumericKeypadTextField,placeholder: String, borderStyle: UITextField.BorderStyle, font: UIFont, textAlignment: NSTextAlignment, tag: Int, maxLength: Int) {
        
        textField.placeholder = placeholder
        textField.borderStyle = borderStyle
        textField.font = font
        textField.textAlignment = textAlignment
        textField.tag = tag
        textField.maxLength = maxLength
        
    }
    
    // Called when user clicks Continue button on High/Low Parameter Pop Up
    func errorContinue() {
        
        visualEffectView.removeFromSuperview()
        
        let last3Digits = Int(digitTextField1.text! + digitTextField2.text! + digitTextField3.text!)
        
        // Compare user-entered 3 digits with meter number
        let meterNumberLast3Digits: Int!
        meterNumberLast3Digits = Int((currentMeterRead.meterNumber.trimmingCharacters(in: CharacterSet.whitespaces)).suffix(3))
        

        // If 3 digits are correct, alert and clear the meter read / let user do second read
        if last3Digits == meterNumberLast3Digits {
            
            // Pop up telling user they're at the correct location
            AudioServicesPlaySystemSound(1315);
            let alert = UIAlertController(title: "Enter Second Read", message: "You are at the correct meter. Please enter your second read.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {(alert: UIAlertAction!) in self.close()}))
            self.present(alert, animated: true, completion: nil)
            
            meterReadBar.clearMeterReadEntry()
            currentMeterRead.reading = nil
            currentMeterRead.updateReadDate()
            disableButton()
            
            
        // Else, 3 digits are wrong, alert and clear the meter / let user do second read
        } else {
            
            // Pop up asking user to check they're at the correct location
            AudioServicesPlaySystemSound(1315);
            let alert = UIAlertController(title: "Incorrect Entry", message: "Your entry does not match with the last 3 digits of the meter number. Please ensure you are at the correct address and location.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .default, handler: {(alert: UIAlertAction!) in self.close()}))
            self.present(alert, animated: true, completion: nil)
            
            // Clear meter read and reset bad read count, because user is at the wrong meter
            meterReadBar.clearMeterReadEntry()
            currentMeterRead.reading = nil
            currentMeterRead.updateReadDate()
            disableButton()
            badReadCount = 0
            tapNextMeterButtonCount = 0
            
        }

    }
    
    
    
    
    
}


