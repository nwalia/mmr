//
//  OCRUtility.swift
//  Meter Reader
//
//  Created by Shahzada, Kaspar (CA - Toronto) on 2018-07-15.
//  Copyright © 2018 Shahzada, Kaspar (CA - Toronto). All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Vision
import CoreML

class OCRUtility {
    //MARK: - OCR Parameters
    var numDigits: Int? = nil // Number of digits to detect filter
    var highValue: Int = Int.max // High threshold filter
    var lowValue: Int = Int.min // Low threshold filter
    var readNumbers: [String:Int] = [:] // Storage for all numbers read
    var scanCount: Int = 0
    
    //MARK: - Vision Variables
    var detectedWordBounds: VNTextObservation? = nil // Rectangle around most recent read and valid numbers
    let model = Model_9000()
    var normROI: CGRect = CGRect()
    
    //MARK: - Publicly Used Functions
    init(){
        
    }
    
    
    init(windowSize: CGRect, ROI: CGRect, resolution: CGSize, numDigits: Int?, highValue: Int?, lowValue: Int?){
        if numDigits != nil {
            self.numDigits = numDigits!
        }
        
        if highValue != nil {
            self.highValue = highValue!
        }
        
        if lowValue != nil {
            self.lowValue = lowValue!
        }
        
        self.normROI = ROI.toNormalized(windowSize: windowSize, resolution: resolution)
        
    }
    
    
    // Gets the highest recognized number from OCR
    func getBestGuess() -> (String, Int)?{
        if readNumbers.count > 0 {
            if let maxNumber = readNumbers.max(by:{a, b in a.value < b.value}){
                return (maxNumber.key, maxNumber.value)
            }
        }
        return nil
    }
    
    // Gets the highest recognized number from OCR
    func getTextOutline() -> VNTextObservation?{
        if let dwb = self.detectedWordBounds{
            return dwb
        }
        return nil
    }
    
    
    //MARK: - OCR Functions
    // Setup Vision request for feature extraction
    func setupVisionRequest(image: CGImage) -> [VNDetectTextRectanglesRequest]{
        // setup request (sets callback to use)
        let textRequest = VNDetectTextRectanglesRequest(completionHandler: {(request, error) in
            guard let results = request.results else {
                print("No OCR Result")
                return
            }
            
            self.OCRSolve(results: results.map({$0 as? VNTextObservation}), cgImage: image)
        }
        )
        
        textRequest.regionOfInterest = self.normROI
        textRequest.reportCharacterBoxes = true // make sure it gives char location info, not just word
        return [textRequest]
    }
    
    // Kick-off feature extraction (asynchronous)
    func startOCRRequest(sampleBuffer: CMSampleBuffer, window: CGRect) {
        guard let cgImage = imageFromSampleBuffer(sampleBuffer: sampleBuffer) else{
            return
        }
        
        var image = UIImage.init(cgImage: cgImage, scale: CGFloat(1.0), orientation: .up)
        image = image.imageRotatedByDegrees(oldImage: image, deg: 90)
        
        let ri = image.size.width/image.size.height
        let rw = window.width/window.height
        var w: CGFloat = 0
        var h: CGFloat = 0
        if (rw > ri){
            w = image.size.width
            h = image.size.width/rw
        } else {
            w = image.size.height/rw
            h = image.size.height
        }
        let cropRect = CGRect(x: (image.size.width-w)/2, y: (image.size.height-h)/2, width: w, height: h)
        let tempImage = self.cropUIImages(image: image, rects: [cropRect], swapping: false)
        let scaledImage = self.scaleUIImages(images: tempImage, scaledToSize: CGSize(width: window.size.width, height: window.size.height))[0]
        
        var requestOptions:[VNImageOption : Any] = [:]
        if let camData = CMGetAttachment(sampleBuffer, key: kCMSampleBufferAttachmentKey_CameraIntrinsicMatrix, attachmentModeOut: nil) {
            requestOptions = [.cameraIntrinsics:camData]
        }
        
        // Kick-Off image recognition
        let imageRequestHandler = VNImageRequestHandler(cgImage: scaledImage.cgImage!, orientation: CGImagePropertyOrientation.up, options: requestOptions)
        do {
            try imageRequestHandler.perform(setupVisionRequest(image: scaledImage.cgImage!))
        } catch {
            print(error)
        }
    }
    
    
    // Callback for Feature Extraction Results (including reading out the numbers)
    func OCRSolve(results: [VNTextObservation?], cgImage: CGImage){
        let image = UIImage.init(cgImage: cgImage, scale: CGFloat(1.0), orientation: .up)
        
        detectedWordBounds = nil
        for region in results {
            if let boxes = region?.characterBoxes {
                if boxes.count == self.numDigits {
                    detectedWordBounds = region
                    var rawBoxes: [VNRectangleObservation] = [VNRectangleObservation]()
                    for box in boxes {
                        rawBoxes.append(box)
                    }
                    
                    var convertedBoxes:[CGRect] = [CGRect]()
                    for rawBox in rawBoxes{
                        convertedBoxes.append(rawBox.toCGRect(size: image.size as CGSize))
                        //                        print(rawBox.toCGRect(size: image.size as CGSize))
                    }
                    convertedBoxes = convertedBoxes.sorted(by: { $0.origin.x < $1.origin.x })
                    
                    
                    let tempImage = self.cropUIImages(image: image, rects: convertedBoxes, swapping: false)
                    let digitImages = self.scaleUIImages(images: tempImage, scaledToSize: CGSize(width: 20, height: 32))
//                    for digitImage in digitImages{
//                        UIImageWriteToSavedPhotosAlbum(digitImage, nil, nil, nil)
//                    }
                    //                    // do predictions
                    if let detectedNumber = readNumberFromImages(images: digitImages) { // needs to be updated to proper boxes
                        if let count = readNumbers[detectedNumber] {
                            self.readNumbers[detectedNumber] = count + 1
                        }else{
                            self.readNumbers[detectedNumber] = 1
                        }
                    }
                    
                }
            }
        }
    }
    
    
    // Parse/Read number found in an ordered set of images
    func readNumberFromImages(images: [UIImage]) -> String?{
        var outputString = ""
        for image in images {
            if let pixelBuffer = image.pixelBuffer(){
                let output = try? model.prediction(image: pixelBuffer)
                outputString += output!.classLabel
            }
        }
        print(outputString)
        if (outputString.count == self.numDigits && Int(outputString)! > self.lowValue && Int(outputString)! < self.highValue){
            return outputString
        }
        return nil
    }
}


//MARK: - Image Manipulation Tools
extension OCRUtility {
    func scaleUIImages(images:[UIImage], scaledToSize newSize:CGSize) -> [UIImage] {
        var scaledImages: [UIImage] = []
        scaledImages.reserveCapacity(images.count)
        for image in images {
            UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
            image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: newSize.width, height: newSize.height)))
            let scaledImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            scaledImages.append(scaledImage)
        }
        return scaledImages
    }
    
    func cropUIImages(image: UIImage, rects: [CGRect], swapping: Bool) -> [UIImage] {
        var croppedImages: [UIImage] = []
        for rect in rects {
            let cgimage = image.cgImage!
            if swapping==true {
                guard let imageRef: CGImage = cgimage.cropping(to: rect.swapXY(size: image.size)) else{
                    print("Cropping Error")
                    return []
                }
                let croppedImage: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
                croppedImages.append(croppedImage)
            }else {
                guard let imageRef: CGImage = cgimage.cropping(to: rect) else{
                    print("Cropping Error")
                    return []
                }
                let croppedImage: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
                croppedImages.append(croppedImage)
            }
        }
        return croppedImages
    }
    
    func imageFromSampleBuffer(sampleBuffer : CMSampleBuffer) -> CGImage? {
        // Get a CMSampleBuffer's Core Video image buffer for the media data
        let  imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        // Lock the base address of the pixel buffer
        CVPixelBufferLockBaseAddress(imageBuffer!, CVPixelBufferLockFlags.readOnly)
        
        // Get the number of bytes per row for the pixel buffer
        let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer!)
        
        // Get the number of bytes per row for the pixel buffer
        let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer!)
        // Get the pixel buffer width and height
        let width = CVPixelBufferGetWidth(imageBuffer!)
        let height = CVPixelBufferGetHeight(imageBuffer!)
        
        // Create a device-dependent RGB color space
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        // Create a bitmap graphics context with the sample buffer data
        var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Little.rawValue
        bitmapInfo |= CGImageAlphaInfo.premultipliedFirst.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
        //let bitmapInfo: UInt32 = CGBitmapInfo.alphaInfoMask.rawValue
        let context = CGContext.init(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo)
        // Create a Quartz image from the pixel data in the bitmap graphics context
        let image = context?.makeImage()
        
        // Unlock the pixel buffer
        CVPixelBufferUnlockBaseAddress(imageBuffer!, CVPixelBufferLockFlags.readOnly)
        return (image)
    }
}

extension CGRect{
    func toNormalized(windowSize: CGRect, resolution: CGSize) -> CGRect{

        let xCord = self.origin.x / windowSize.width
        let yCord = self.origin.y / windowSize.height
        let width = self.width / windowSize.width
        let height = self.height / windowSize.height
        return CGRect(x: xCord, y: yCord, width: width, height: height)
        
    }
    
    func swapXY(size: CGSize) -> CGRect{
        return CGRect(x: self.minY, y: size.width - self.minX, width: self.height, height: -self.width)
    }
}

extension VNTextObservation {
    func toCGRect(windowSize: CGRect, padding: CGFloat) -> CGRect?{
        
        guard let boxes = self.characterBoxes else {
            return nil
        }
        
        var maxX: CGFloat = 9999.0
        var minX: CGFloat = 0.0
        var maxY: CGFloat = 9999.0
        var minY: CGFloat = 0.0
        
        for char in boxes {
            if char.bottomLeft.x < maxX {
                maxX = char.bottomLeft.x
            }
            if char.bottomRight.x > minX {
                minX = char.bottomRight.x
            }
            if char.bottomRight.y < maxY {
                maxY = char.bottomRight.y
            }
            if char.topRight.y > minY {
                minY = char.topRight.y
            }
        }
        
        let xCord = maxX * windowSize.width - padding/2
        let yCord = (1 - minY) * windowSize.height - padding/2
        let width = (minX - maxX) * windowSize.width + padding
        let height = (minY - maxY) * windowSize.height + padding
        
        return CGRect(x: xCord, y: yCord, width: width, height: height)
    }
}

extension VNRectangleObservation {
    func toCGRect(size: CGSize) -> CGRect{
        let boundingBox = self.boundingBox
        let xCord = boundingBox.origin.x * size.width
        let yCord = (1-boundingBox.origin.y) * size.height
        let width = boundingBox.width * size.width
        let height = -1 * boundingBox.height * size.height
        return CGRect(x: xCord, y: yCord, width: width, height: height)
    }
}

extension UIImage {
    func imageRotatedByDegrees(oldImage: UIImage, deg degrees: CGFloat) -> UIImage {
        //Calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: oldImage.size.width, height: oldImage.size.height))
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat.pi / 180)
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat.pi / 180))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        bitmap.draw(oldImage.cgImage!, in: CGRect(x: -oldImage.size.width / 2, y: -oldImage.size.height / 2, width: oldImage.size.width, height: oldImage.size.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func pixelBuffer() -> CVPixelBuffer? {
        let width = self.size.width
        let height = self.size.height
        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue,
                     kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
        var pixelBuffer: CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault,
                                         Int(width),
                                         Int(height),
                                         kCVPixelFormatType_OneComponent8,
                                         attrs,
                                         &pixelBuffer)
        
        guard let resultPixelBuffer = pixelBuffer, status == kCVReturnSuccess else {
            return nil
        }
        
        CVPixelBufferLockBaseAddress(resultPixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
        let pixelData = CVPixelBufferGetBaseAddress(resultPixelBuffer)
        
        let grayColorSpace = CGColorSpaceCreateDeviceGray()
        guard let context = CGContext(data: pixelData,
                                      width: Int(width),
                                      height: Int(height),
                                      bitsPerComponent: 8,
                                      bytesPerRow: CVPixelBufferGetBytesPerRow(resultPixelBuffer),
                                      space: grayColorSpace,
                                      bitmapInfo: CGImageAlphaInfo.none.rawValue) else {
                                        return nil
        }
        
        context.translateBy(x: 0, y: height)
        context.scaleBy(x: 1.0, y: -1.0)
        
        UIGraphicsPushContext(context)
        self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        UIGraphicsPopContext()
        CVPixelBufferUnlockBaseAddress(resultPixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
        
        return resultPixelBuffer
    }
}
