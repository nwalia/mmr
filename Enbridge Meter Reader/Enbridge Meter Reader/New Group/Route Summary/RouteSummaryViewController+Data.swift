//
//  RouteSummaryViewController+Data.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-10.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

extension RouteSummaryViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Summary Route Info") as! SummaryRouteInfoCell
            cell.populate()
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryOrderDetailCell") as! SummaryOrderDetailCell
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
             // Set top summary section height smaller for periodic (only 1 line for MRU)
             if Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .PERIODIC {
                return 208.0
             } else {
                return 240.0
            }
        }
        else if indexPath.row == 1{
            // Set reads table section height smaller for periodic (has resequence button in section below)
            if Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .PERIODIC {
                return 416.0
            } else {
                return 466.0
            }
            
        }
        else{
            return UITableView.automaticDimension
        }
    }
}
