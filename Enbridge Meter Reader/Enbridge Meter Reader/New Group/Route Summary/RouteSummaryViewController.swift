import Foundation
import UIKit
import AudioToolbox
import SAPCommon
import SAPOfflineOData
import SAPOData
import proxyclasses

class RouteSummaryViewController: UIViewController{
    
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var progressBar: ProgressBarView!
    @IBOutlet weak var timeElapsedBar: TimeElapsedView!
    
    @IBOutlet weak var buttonContainer: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var requestResequence: UIButton!

    
    
    static var toggle: Bool = false
    var numberOfCell = Int()
    var numberOfClick = 0
    var shouldShowNetworkToolbar = false
    
    static var submissionCompleted = false
    static var caller: String?
    static var routeSummeryShareInstance : RouteSummaryViewController?
    private let logger = Logger.shared(named: "Route Summary View")
    
    var selectedRead: MeterRead?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logger.logLevel = .debug
        
        navigationController?.navigationBar.tintColor = UIColor(red:1, green:0.72, blue:0.11, alpha:1)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        
        // Do any additional setup after loading the view, typically from a nib.
        self.numberOfCell = 2
        self.infoTableView.rowHeight = UITableView.automaticDimension
        self.infoTableView.estimatedRowHeight = UITableView.automaticDimension
        
        timeElapsedBar.populate(routeId: Model.selectedRouteKey)
        
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.completionTime != nil {
            timeElapsedBar.stopTimer()
        }
        
        changeState()
        
        // Set UI based on peridic/outcard (request resequence button, progress bar)
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .OUTCARD {
            timeElapsedBar.isHidden = true
            requestResequence.removeFromSuperview()
            self.timeElapsedBar.isHidden = true
            NSLayoutConstraint(item: submitButton as Any, attribute: .top, relatedBy: .equal, toItem: lineView, attribute: .bottom, multiplier: 1.0, constant: 30.0).isActive = true
            NSLayoutConstraint(item: buttonContainer as Any, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 130).isActive = true
            progressBar.populate(routeKey: Model.selectedRouteKey, type: .outcard, shouldRefreshWithNetworkChange: true)
        } else {
            progressBar.populate(routeKey: Model.selectedRouteKey, type: .periodic, shouldRefreshWithNetworkChange: true)
        }
        
        RouteSummaryViewController.routeSummeryShareInstance = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        infoTableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.requestResequence?.isEnabled = checkForResequence()
        
        // For periodic, check for resequence boolean on route and set resequence button UI accordingly
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .PERIODIC {
            if Model.sharedInstance.routes[Model.selectedRouteKey]!.resequence {
                requestResequence.setTitleColor(UIColor.enbridgeYellow, for: .normal)
                requestResequence.setTitle("Resequence Confirmed", for: .normal)
            } else {
                requestResequence.setTitleColor(.white, for: .normal)
                requestResequence.setTitle("Request Resequence", for: .normal)
            }
        }
        
    }
    
    func checkForResequence() -> Bool {
        var status : Bool?
        if let totalMeters = Model.sharedInstance.routes[Model.selectedRouteKey]?.totalMeters {
            if totalMeters <= (Model.sharedInstance.routes[Model.selectedRouteKey]?.totalReads)! {
                for meter in Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads ?? [MeterRead()] {
                    if meter.status == .completed || meter.reading != nil || (meter.troubleCodes.count != 0) || notNilAndNotEmpty(item: meter.newMeterNumber ?? "") || notNilAndNotEmpty(item: meter.newSizeCode ?? "") || (meter.newMeterNumberOfDials != nil) || meter.didChangeLocation || meter.didChangeSpecialMessage || meter.skipCode?.id != nil {
                        status = true
                    } else {
                        status = false
                        break
                    }
                }
            }else {
                status = false
            }
        }else{
            status = false
        }
        status! ? (self.requestResequence.backgroundColor = UIColor.darkGray) : (self.requestResequence.backgroundColor = UIColor.lightGray)
        return status!
    }
    
    func changeState() {
        guard let currentRoute = Model.sharedInstance.routes[Model.selectedRouteKey] else { return }
        
        if currentRoute.status == .completed || currentRoute.status == .pending {
            switch RouteSummaryViewController.caller! {
            case "periodicCellViewSubmission":
                self.submitButton.isEnabled = false
                self.submitButton.setTitleColor(UIColor.gray, for: .disabled)
                self.submitButton.backgroundColor = UIColor(red: 254/255, green: 196/255, blue: 78/255, alpha: 1.0)
                self.requestResequence.isEnabled = false
                self.requestResequence.backgroundColor = .gray
                
            case "OutcardCell":
                self.submitButton.isEnabled = false
                self.submitButton.setTitleColor(UIColor.gray, for: .disabled)
                self.submitButton.backgroundColor = UIColor(red: 254/255, green: 196/255, blue: 78/255, alpha: 1.0)
                self.requestResequence.isEnabled = false
                self.requestResequence.backgroundColor = .gray
                
            case "outCardCellSummary":
                self.submitButton.isEnabled = false
                self.submitButton.setTitleColor(UIColor.gray, for: .disabled)
                self.submitButton.backgroundColor = UIColor(red: 254/255, green: 196/255, blue: 78/255, alpha: 1.0)
                self.requestResequence.isEnabled = false
                self.requestResequence.backgroundColor = .gray
                
            default:
                return
            }
            
        }
    }
    
    func notNilAndNotEmpty(item: String?) -> Bool {
        if item != nil && !(item ?? "").isEmpty {
            return true
        } else {
            return false
        }
    }
    
    // Pressed submit route for periodic or outcard route
    @IBAction func didPressSubmit(_ sender: UIButton) {
        
        submitAlert()
        //presentNotificationWith(title: "Route Submitted", message: "\n Your route is being submitted.\n\n Charge your device.", sender: sender)
        
       
//        guard let r: Reachability = Reachability.networkReachabilityForInternetConnection() else { return }
//
//        if r.isReachable {
//            Model.sharedInstance.routes[Model.selectedRouteKey]?.status = .completed
//            guard let meters = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads else {
//                return
//            }
//            for meter in meters {
//                PostWork_SAP_CompleteWorkOrder.sharedInstance.post(meterRead: meter, shouldOnlySetStatus: false, newStatus: "FIELD COMPLETE")
//            }
//        }
//        else {
//            Model.sharedInstance.routes[Model.selectedRouteKey]?.status = .pending
//            shouldShowNetworkToolbar = true
//        }
//
//
//        Model.sharedInstance.routes[Model.selectedRouteKey]?.status = r.isReachable ? .completed : .pending
//        Model.sharedInstance.routes[Model.selectedRouteKey]?.setCompletedTime()
        
        
    }
    
    // Alert to confirm submitting periodic or outcard route
    func submitAlert() {
        
        AudioServicesPlaySystemSound(1315);
        let alertController = UIAlertController(title: "Submit Route", message: "Are you sure you want to submit your data for this route?" , preferredStyle: .alert)
        
        let ConfirmAction = UIAlertAction(title: "Confirm", style: .default, handler: { action in
            
//            if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
//                if let selectVC = tabsController.viewControllers![0].childViewControllers.first as? SelectRouteTableViewController{
//                    OperationQueue.main.addOperation {
//                        selectVC.loadBlurView()
//                    }
//                }
//            }
            
            DispatchQueue.main.async {
                SelectRouteTableViewController.refreshControlRef?.endRefreshing()
                if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                    if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                        selectVC.loadBlurView()
                    }
                }
            }
            
            let alertController = UIAlertController(title: "Route Submitted", message: "\n Your route is being submitted.\n\n Charge your device.", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Okay", style: .default) { [weak self] action in
                
                self?.submitRoute()
                
            }
            
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                // ...
            }
        })
        
        let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { action in
            // do nothing here
        })
        
        alertController.addAction(CancelAction)
        alertController.addAction(ConfirmAction)
        
        self.present(alertController, animated: true) {
            // ...
        }
            
        
    }
    
    
    func submitNewSequence() {
        guard let meters = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads else {
            return
        }
        do {
            let batch = RequestBatch()
            let options = OfflineODataRequestOptions()
            options.transactionID = try TransactionID(stringLiteral:meters.first?.routeId ?? "N/A")
            for (index,meter) in meters.enumerated() {
                let seq = Reseq()
                seq.mmrMru = meter.routeId
                seq.mmrMro =  meter.jobID
                seq.mmrMeternum = meter.meterNumber
                seq.mmrMetersizecode = meter.sizeCode
                seq.mmrOldseqnum = meter.mmrSequenceNum
                seq.mmrNewseqnum = "\(index + 1)"
                
                if let date = meter.requiredDate {
                    let isoDate = date + "+0000"
                    let dateFormatter = ISO8601DateFormatter()
                    let date = dateFormatter.date(from:isoDate)!
                    let calendar = Calendar.current
                    let components = calendar.dateComponents([.year, .month, .day, .hour], from: date)
                    let finalDate = calendar.date(from:components)
                    seq.mmrDuedate = LocalDateTime.from(utc: finalDate ?? Date())
                }
                
                batch.addChanges((LocalStoreReSequence.offlineLS?.createSequenceInTable(seq: seq, transID: meter.routeId))!)
            }
            try LocalStoreReSequence.offlineSP?.processBatch(batch)
            LocalStoreReSequence.offlineLS?.uploadToDB()
        }catch {
            print(error)
        }
        
        
    }
    
    func fabricateNewSequenceValue(index:Int) -> String {
        var positionString = ""
        let position = index + 1
        if String(position).count == 1{
             positionString = "0000000\(position)"
        }else if String(position).count == 2 {
             positionString = "000000\(position)"
        }else if String(position).count == 3 {
             positionString = "00000\(position)"
        }else if String(position).count == 4 {
             positionString = "0000\(position)"
        }else if String(position).count == 5 {
             positionString = "000\(position)"
        }else if String(position).count == 6 {
             positionString = "00\(position)"
        }else if String(position).count == 7 {
             positionString = "0\(position)"
        }
        
        return positionString
        
    }
    
    // Submitting periodic or outcard route, calls post work service to SCP
    func submitRoute() {
        //unsure if this mess up Johann's offline code that hasn't been merged yet (submitting pending routes once connection is restored - Model.sharedInstance.submitQueuedRoutes())
        guard let r: Reachability = Reachability.networkReachabilityForInternetConnection() else { return }
        
        guard (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads) != nil else {
            return
        }

         Model.sharedInstance.routes[Model.selectedRouteKey]?.status = .pending
         Model.sharedInstance.routes[Model.selectedRouteKey]?.setCompletedTime()
        
        RouteSummaryViewController.toggle ? self.submitNewSequence() : print("No Re-sequencing Done")
        
        if r.isReachable {
            if let route = Model.sharedInstance.routes[Model.selectedRouteKey] {
                submit(forRoute: route, withID: route.id) { error in
                    var selVC:SelectRouteTableViewController? = nil
                    if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                        if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                            selVC = selectVC
                            OperationQueue.main.addOperation {
                                selectVC.unloadBlurView()
                            }
                        }
                    }
                    guard error == nil else {
                        let alertController = UIAlertController(title: "Submission Failed", message: "Failed to submit this route, please try again later", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .default))
                        OperationQueue.main.addOperation {
                            selVC?.present(alertController, animated: true, completion: {})
                        }
                        return
                    }
                    // Remove selected route from device IF the status == completed
                    Model.sharedInstance.routes.removeValue(forKey: Model.selectedRouteKey)
                    self.timeElapsedBar.resetTimer(Model.selectedRouteKey)
                    
                   
                    
                    OperationQueue.main.addOperation {
                        selVC?.tableView.reloadData()
                    }
                    
                    Persistence.shared.saveRoutes()
                }
            }

        }
        else {
            shouldShowNetworkToolbar = true
            if let route = Model.sharedInstance.routes[Model.selectedRouteKey] {
                submit(forRoute: route, withID: route.id)
            }
            OperationQueue.main.addOperation {
                if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                    if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                      selectVC.unloadBlurView()
                    }
                }
            }
            Persistence.shared.saveRoutes()
        }
         _ = navigationController?.popToRootViewController(animated: true)
        
        
    }
    
    
    private func updateTable(meter:MeterRead, withStatus status : String) {
        do {
            meter.mmrStatus = status
            LogHelper.shared?.info("Updating meter with status" + meter.jobID + ", " + status, loggerObject: self.logger)
            if meter.isOutCard ?? false && !AppDelegate.hasMockData {
                _ = try LocalStoreManager.ShareInstance?.updateOperation(type: RouteType.OUTCARD.rawValue, operation: MeterRead.mapTo_Operation(meterRead: meter, withCompletionCode: ""))
            }else if meter.isPeriodic ?? false && !AppDelegate.hasMockData {
                _ = try LocalStoreManager.ShareInstance?.updateOperation(type: RouteType.PERIODIC.rawValue, operation: MeterRead.mapTo_Operation(meterRead: meter, withCompletionCode: ""))
            }
            LogHelper.shared?.info("Updating meter with status successfully with work ID:" + meter.jobID, loggerObject: self.logger)
        } catch {
            print("Update Operation Failed: \(error)")
            LogHelper.shared?.error("Updating meter with status failed" + meter.jobID + ", " + status, loggerObject: self.logger)
        }
    }
    
    private func submit(forRoute route:Route, withID id: String?, completionHandler: @escaping (Error?) -> Void = {_ in }) {
        let this_route = route.type
        switch this_route! {
        case RouteType.OUTCARD:
            var mruSet = Set<String>()
            for meterRead in route.meterReads{
                mruSet.insert(meterRead.routeId)
            }
            submitOutcardPeriodic(mru: mruSet.joined(separator: ",") ,completionHandler: completionHandler)
        case RouteType.PERIODIC:
            submitOutcardPeriodic(mru: route.meterReads.first?.routeId,completionHandler: completionHandler)
        default:
            return
        }
    }
    
    private func submitOutcardPeriodic(mru: String?, completionHandler: @escaping (Error?) -> Void = {_ in }){
        if mru != nil{
            do{
                let entry = MassUpdateTasks()
                let engineer = Engineer()
                let mruList = MRUs()
                let task = Task()
                
                engineer.id = Utilities.sharedInstance.userName
                
                mruList.mru = mru
                
                task.status = "MISSED"
                task.mrUs = mruList
                
                entry.engineer = engineer
                entry.task = task
                entry.mru = mru
                
                try LocalStoreUpdateStatusServ.shareInstance!.createEntryInTable(entry: entry)
                LocalStoreUpdateStatusServ.shareInstance?.uploadToDB(completionHandler: completionHandler)
            }catch {
                print(error)
            }
        }
    }
    
    // Remove submitted periodic or outcard route from device and return to select route screen
    // (this function likely needs to be called from Model.sharedInstance.submitQueuedRoutes() as well)
    func removeSubmittedRoute(routeKey: String) {
        Model.sharedInstance.routes.removeValue(forKey: routeKey)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTables"), object: nil)
        _ = navigationController?.popToRootViewController(animated: true)
        
    }
    

    @IBAction func didPressRequestResequence(_ sender: UIButton) {
        presentNotificationWith(title: "Request Resequence", message: "The order of your route completed today will become your new standard route.", sender: sender)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPeriodicOutcardScreen" {
            guard selectedRead != nil else { return }
            let destinationVC = segue.destination as! PeriodicOutcardScreenViewController
            destinationVC.currentMeterRead = selectedRead
        }
    }
}

extension RouteSummaryViewController: SummaryOrderDetailCellDelegate {
    func didLaunchSegue(withMeterRead read: MeterRead) {
        selectedRead = read
        performSegue(withIdentifier: "toPeriodicOutcardScreen", sender: self)
    }
    
    
}




