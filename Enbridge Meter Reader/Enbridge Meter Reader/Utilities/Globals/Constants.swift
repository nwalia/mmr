//
// Constants.swift
// Northwind
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 01/11/17
//

import Foundation
import SAPFoundation

struct Constants {

    static let appId = "com.enbridge.newservice"
    static let connectionId = "NorthWind"
    private static let sapcpmsUrlString = "\(newServiceURL)/gw/odata/SAP/POST_NEWSERVICE;v=1/ZDM_NEW_SERVICE_COLL" /**"https://l350014-iflmap.hcisbp.ca1.hana.ondemand.com"**/
    static let sapcpmsUrl = URL(string: sapcpmsUrlString)!
    static let appUrl = Constants.sapcpmsUrl.appendingPathComponent(connectionId)
    static let configurationParameters = SAPcpmsSettingsParameters(backendURL: Constants.sapcpmsUrl, applicationID: Constants.appId)
}
