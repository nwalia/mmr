//
//  SelectSkipCodeViewController.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-19.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit
import AudioToolbox

class SelectSkipCodeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    let model = SkipCode.fullList
    var filteredList: [SkipCode]?
    var selectedCode: SkipCode?
    var confirmedCode: SkipCode?
    var formattedCodeString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        // For green editing checkmarks
        tableView.isEditing = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        formattedCodeString = "Skip Code: \(confirmedCode?.id ?? "NONE")"
    }
    
    func segueOut(){
        self.confirmedCode = self.selectedCode
        if(self.selectedCode?.id == "0"){
            self.confirmedCode = nil
        }
        self.performSegue(withIdentifier: "toPreviousScreen", sender: self)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification){
        guard let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            self.tableViewBottomConstraint.constant = keyboardHeight * -1
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Table View

extension SelectSkipCodeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
}

extension SelectSkipCodeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectSkipCodeTableViewCell") as! SelectSkipCodeTableViewCell
        
        let codeForCell = isFiltering() ? filteredList![indexPath.row] : model[indexPath.row]
        
        if codeForCell == confirmedCode {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            cell.backgroundColor = GlobalConstants.SELECTED_CELL
        }
        else {
            tableView.deselectRow(at: indexPath, animated: false)
            cell.backgroundColor = .white
        }
        
        cell.numberLabel.text = codeForCell.id
        cell.descriptionLabel.text = codeForCell.description
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering() ? (filteredList?.count ?? 0) : model.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedCode = isFiltering() ? filteredList![indexPath.row] : model[indexPath.row]
        
//        if confirmedCode != nil && selectedCode! == confirmedCode {
//            DispatchQueue.main.async {
//                let alert = UIAlertController(title: "\(self.selectedCode?.id ?? "NO ID") - \(self.selectedCode?.description ?? "NO DESCRIPTION")", message: "Are you sure you want to remove the skip code?", preferredStyle: .alert)
//                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//                let confirmAction = UIAlertAction(title: "Remove", style: .default,
//                                                  handler: {(UIAlertAction) -> Void in
//                                                    self.selectedCode = nil
//                                                    self.segueOut()
//                })
//
//                alert.addAction(cancelAction)
//                alert.addAction(confirmAction)
//                self.present(alert, animated: true, completion: nil)
//            }
//        }
//        else if confirmedCode != nil {
//            DispatchQueue.main.async {
//                AudioServicesPlaySystemSound(1315);
//                let alert = UIAlertController(title: "\(self.selectedCode?.id ?? "NO ID") - \(self.selectedCode?.description ?? "NO DESCRIPTION")", message: "Are you sure you want to change the skip code?", preferredStyle: .alert)
//                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//                let confirmAction = UIAlertAction(title: "Confirm", style: .default,
//                                                  handler: {(UIAlertAction) -> Void in
//                                                    self.segueOut()
//                })
//
//                alert.addAction(cancelAction)
//                alert.addAction(confirmAction)
//                self.present(alert, animated: true, completion: nil)
//            }
//        }
//        else {
            segueOut()
        //}
    }
}

// MARK: - Search

extension SelectSkipCodeViewController: UISearchBarDelegate {

    
    func isFiltering() -> Bool {
        return !(searchBar.text?.isEmpty)!
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredList = model.filter({(code: SkipCode) -> Bool in
            return (
                code.description.lowercased().contains(searchText.lowercased())
                    || String(code.id).lowercased().contains(searchText.lowercased())
            )
        })
        
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(searchText)
        
        if (tableView.numberOfRows(inSection: 0) > 0) {
            let topOfTable = IndexPath(row: 0, section: 0)
            tableView.scrollToRow(at: topOfTable, at: .top, animated: true)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            self.tableViewBottomConstraint.constant = 0
        })
    }
    
    
}
