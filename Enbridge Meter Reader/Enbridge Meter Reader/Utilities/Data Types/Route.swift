//
//  Route.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-03-12.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import MapKit

enum RouteStatus: String, Codable {
    case available
    case inProgress
    case completed
    case pending
}

class Route: Codable {
    var id: String
    var type: RouteType?
    var meterReads: [MeterRead]
    var resequencedList : [MeterRead]
    var mapAnnotations: [Annotation]
    var completionTime: Date?
    var status: RouteStatus
    var uploading: Bool?
    var blockCell: Bool?
    var uploadText: String?
    var resequence: Bool
    var region: String?
    var lastMeterCompleted: String? // For the outcard progress bar
    var cycleNo: String?
    var totalMeters: Int?
    var isSorted:Bool?
    
    private enum CodingKeys: String, CodingKey {
        case id, type, meterReads, completionTime, status, resequence, region, lastMeterCompleted, cycleNo, resequenceList, totalMeters, isSorted
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        type = try container.decode(RouteType?.self, forKey: .type)
        meterReads = try container.decode(Array<MeterRead>.self, forKey: .meterReads)
        completionTime = try container.decode(Date?.self, forKey: .completionTime)
        status = try container.decode(RouteStatus.self, forKey: .status)
        resequence = try container.decode(Bool.self, forKey: .resequence)
        resequencedList = try container.decode(Array<MeterRead>.self, forKey: .resequenceList)
        region = try container.decode(String?.self, forKey: .region)
        lastMeterCompleted = try container.decode(String?.self, forKey: .lastMeterCompleted)
        cycleNo = try container.decode(String?.self, forKey: .cycleNo)
        totalMeters = try container.decode(Int?.self, forKey: .totalMeters)
        isSorted = try container.decode(Bool?.self, forKey: .isSorted)
        mapAnnotations = [Annotation]()
        
        
        //print("REQUIREDDDDDD INTITITTIITITTII")
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(type, forKey: .type)
        try container.encode(meterReads, forKey: .meterReads)
        try container.encode(completionTime, forKey: .completionTime)
        try container.encode(status, forKey: .status)
        try container.encode(resequence, forKey: .resequence)
        try container.encode(resequencedList, forKey: .resequenceList)
        try container.encode(region, forKey: .region)
        try container.encode(lastMeterCompleted, forKey: .lastMeterCompleted)
        try container.encode(cycleNo, forKey: .cycleNo)
        try container.encode(totalMeters, forKey: .totalMeters)
        try container.encode(isSorted, forKey: .isSorted)
        
    }
        
    var completedReads: Int {
        get{
            var total = 0
            for read in meterReads {
                if read.status == .completed {
                    total += 1
                }
            }
            return total
        }
    }
    
    var pendingReads: Int {
        var total = 0
        for read in meterReads {
            if read.status == .pending {
                total += 1
            }
        }
        return total
    }
    
    var completedOrSkippedReads: Int {
        var total = 0
        for read in meterReads {
            if read.status == .completed || read.skipCode != nil {
                total += 1
            }
        }
        return total
    }
    
    var totalReads: Int {
        get { 
            return meterReads.count
        }
    }
    
    var totalSkips: Int {
        get {
            var total = 0
            for read in meterReads {
                if read.skipCode != nil {
                    total += 1
                }
            }
            return total
        }
    }
    
    var totalMissed: Int {
        var total = 0
        for read in meterReads {
            if read.status != .completed && read.status != .pending && read.skipCode == nil {
                total += 1
            }
        }
        return total
    }

//    public var debugDescription: String {
//        return "Route ID: \(id) \n Completed: \(completedReads) \n Meter reads: \(meterReads) \n Type: \(String(describing: type!)) \n Resequence: \(resequence))"
//    }
    
    init(){
        self.id = "NONE"
        self.meterReads = [MeterRead]()
        self.resequencedList = [MeterRead]()
        mapAnnotations = [Annotation]()
        self.status = .available
        self.resequence = false
        self.uploading = false
        self.uploadText = ""
        self.blockCell = false
        self.isSorted = false
        if self.meterReads.count > 0 {
            self.lastMeterCompleted = meterReads[0].routeId
            self.cycleNo = meterReads[0].cycleNo
        }
        
         //print("REQUIREDDDDDD INTITITTIITITTII")
    }
    
    init(id: String){
        self.id = id
        self.meterReads = [MeterRead]()
        self.resequencedList = [MeterRead]()
        mapAnnotations = [Annotation]()
        self.status = .available
        self.resequence = false
        self.uploading = false
        self.uploadText = ""
        self.blockCell = false
        self.isSorted = false
        if self.meterReads.count > 0 {
            self.lastMeterCompleted = meterReads[0].routeId
            self.cycleNo = meterReads[0].cycleNo
        }
    }
    
    func populateAnnotations(){
        var newAnnotationSet = [Annotation]()
        for read in meterReads {
            if read.latitude != nil && read.longitude != nil {
                let newAnnotation = Annotation()
                newAnnotation.coordinate = CLLocationCoordinate2D(latitude: read.latitude!, longitude: read.longitude!)
                newAnnotation.style = .image(UIImage(named: "Cluster"))
                newAnnotationSet.append(newAnnotation)
            }
        }
        
        self.mapAnnotations = newAnnotationSet
    }
    
    func setCompletedTime(){
        if self.completionTime == nil {
            self.completionTime = Date()
        }
    }
}
