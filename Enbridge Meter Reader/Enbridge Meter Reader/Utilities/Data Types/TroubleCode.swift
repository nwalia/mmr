//
//  TroubleCode.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-03.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

class TroubleCode: Codable {
    let number: Int
    let description: String
    let noteMandatory: Bool
    let photoMandatory: Bool
    var selected: Bool
    var note: String?
    var image: UIImage?
    
    init(number: Int, description: String, noteMandatory: Bool, photoMandatory: Bool){
        self.number = number
        self.noteMandatory = noteMandatory
        self.description = description
        self.photoMandatory = photoMandatory
        self.selected = false
    }
    
    enum CodingKeys: String, CodingKey {
        case `self`, number, description, noteMandatory, photoMandatory, selected, note, imageData
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        number = try container.decode(Int.self, forKey: .number) 
        description = try container.decode(String.self, forKey: .description)
        noteMandatory = try container.decode(Bool.self, forKey: .noteMandatory)
        photoMandatory = try container.decode(Bool.self, forKey: .photoMandatory)
        selected = try container.decode(Bool.self, forKey: .selected)
        note = try container.decode(String?.self, forKey: .note)
//        if let imageData = try container.decodeIfPresent(Data.self, forKey: .imageData) {
//            image = NSKeyedUnarchiver.unarchiveObject(with: imageData) as? UIImage
//        } else {
//            image = nil
//        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(number, forKey: .number)
        try container.encode(description, forKey: .description)
        try container.encode(noteMandatory, forKey: .noteMandatory)
        try container.encode(photoMandatory, forKey: .photoMandatory)
        try container.encode(selected, forKey: .selected)
        try container.encode(note, forKey: .note)
        
//        if let imageToEncode = image {
//            let imageData = NSKeyedArchiver.archivedData(withRootObject: imageToEncode)
//            try container.encode(imageData, forKey: .imageData)
//        }
    }
}
