//
//  OutCardCell.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-17.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit
import MapKit

class OutCardCell: UITableViewCell {

    @IBOutlet weak var locationLabel: UILabel?
    @IBOutlet weak var outCardMapView: MKMapView!
    @IBOutlet weak var greyView: UIView?
    @IBOutlet weak var summaryButton: UIButton?
    @IBOutlet weak var returnToRoute: UIButton?
    @IBOutlet weak var viewRouteSubmission: UIButton?
    @IBOutlet weak var beginRoute: UIButton?
    @IBOutlet weak var progressBar: ProgressBarView?
    @IBOutlet weak var missesLabel: UILabel?
    @IBOutlet weak var blurView: UIView?
    @IBOutlet weak var uploadText: UILabel?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    
    let manager = ClusterManager()
    
//    public static var shared: OutCardCell = {
//        let OutCardCellObj = OutCardCell()
//        return OutCardCellObj
//    }()
//    
//    class func returnInstance() -> OutCardCell {
//        return shared
//    }
    
    // hasBeenStarted = false will show the begin route button
    // hasBeenStarted = true will hide the begin route button 
    var hasBeenStarted = false
    var submitted = false
    
   // static var showViewRouteSubmissionButtonFlag = false
    var routeKey = ""
    var uploading = false
    var uploadingText = ""
    var blockCell = false
    var associatedRoute: Route? {
        didSet {
            MapViewManager.setUpMapView(minCountForClustering: 10, manager: manager, mapView: outCardMapView, annotations: (associatedRoute?.mapAnnotations)!)

            progressBar?.populate(routeKey: routeKey, type: .outcard)
            if associatedRoute?.status == .available {
                missesLabel?.text = ""
            }
            else {
                missesLabel?.text = "\(associatedRoute?.totalMissed ?? 0) Misses"
            }
            
            locationLabel?.text = "\(associatedRoute?.region ?? "")"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        MapView.sharedInstance.disableUserInteration(mapView: outCardMapView)
        outCardMapView?.delegate = self
    
    }
    

    
    
    override func setNeedsLayout() {
        if hasBeenStarted {
            if beginRoute != nil {
                beginRoute?.isHidden = true
            }
            if self.returnToRoute != nil {
                self.returnToRoute?.isHidden = false
            }
            if self.summaryButton != nil {
                self.summaryButton?.isHidden = false
            }
        }else{
            if beginRoute != nil {
                beginRoute?.isHidden = false
            }
            if self.returnToRoute != nil {
                self.returnToRoute?.isHidden = true
            }
            if self.summaryButton != nil {
                self.summaryButton?.isHidden = true
            }
        }
        
        if Model.sharedInstance.routes[routeKey]?.status == .completed ||
            Model.sharedInstance.routes[routeKey]?.status == .pending {
           // showViewRouteSubmissionButton()
        }
        if uploading {
            activityIndicator?.startAnimating()
        }else{
            activityIndicator?.stopAnimating()
        }
        blurView?.isHidden = !blockCell
        uploadText?.text = uploadingText
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func didSelectBeginRoute(_ sender: UIButton) {
        ConfigureRouteViewController.caller = "outCard"
        RouteSummaryViewController.caller = "OutcardCell"
        Model.selectedRouteKey = routeKey

    }
    
    @IBAction func didSelectViewRouteSubmitted(_ sender: UIButton) {
        Model.selectedRouteKey = routeKey
        RouteSummaryViewController.submissionCompleted = true
        RouteSummaryViewController.caller = "OutcardCell"
    }
    
    override func layoutSubviews() {
//        if OutCardCell.showViewRouteSubmissionButtonFlag{
//            showViewRouteSubmissionButton()
//        }x`
    }
    
    @IBAction func didPressRouteSummary(_ sender: UIButton) {
        Model.selectedRouteKey = routeKey
    }
    
    @IBAction func didPressReturnToRoute(_ sender: UIButton) {
        Model.selectedRouteKey = routeKey
    }
    
//    func showViewRouteSubmissionButton() {
//        self.returnToRoute.isHidden = true
//        self.summaryButton.isHidden = true
//        self.beginRoute.isHidden = true
//        self.viewRouteSubmission.isHidden = true
//        self.viewRouteSubmission.backgroundColor = UIColor(red: 254/255, green: 196/255, blue: 78/255, alpha: 1.0)
//        self.greyView.isHidden = false
//    }
}
