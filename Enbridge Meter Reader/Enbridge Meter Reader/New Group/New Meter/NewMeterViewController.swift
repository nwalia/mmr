//
//  NewMeterViewController.swift
//  Enbridge Meter Reader
//
//  Created by Johann Wentzel on 2017-11-29.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit

class NewMeterViewController: UIViewController,MeterReadBarDelegate {

    @IBOutlet weak var addressTextField: ValidatedTextField!
    @IBOutlet weak var meterNumberTextField: NumericKeypadTextField!
    @IBOutlet weak var meterSizeCodeTextField: ValidatedTextField!
    @IBOutlet weak var numberOfDialsTextField: NumericKeypadTextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var enterMeterNumber: MeterReadBarView!

    var meterReadEntryValid: Bool = false
    var submitButtonEnabled: Bool = false
    var currentMeterRead: MeterRead!

    // Image containers
    var meterImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enterMeterNumber?.delegate = self
        enterMeterNumber?.awakeFromNib()
        enterMeterNumber?.setUp(enterMeterNumber, "firstMeterReadBar", 6)
        
        addressTextField?.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        meterNumberTextField?.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        meterSizeCodeTextField?.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        numberOfDialsTextField?.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        
        submitButtonEnabled = false

        checkAllValid()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkAllValid()
    }
    
    func barDidValidate(_ isValid: Bool, _ title: String) {
        if isValid {
            meterReadEntryValid = true
        } else {
            meterReadEntryValid = false
        }
        checkAllValid()
    }
    

        // Action that fires when button is clicked
    func tappedCameraButton(_ title: String) {
         performSegue(withIdentifier: "toOCR", sender: nil)
    }
    
    @objc private func checkAllValid(){
        if let address = addressTextField, let meterNumber = meterNumberTextField, let meterSize = meterSizeCodeTextField, let dialDigits = numberOfDialsTextField {
            if address.getValidity()
                && meterNumber.getValidity()
                && meterSize.getValidity()
                && dialDigits.getValidity()
                && meterReadEntryValid {
                submitButtonEnabled = true
                submitButton.isEnabled = true
                submitButton.backgroundColor = UIColor(red:1, green:0.72, blue:0.11, alpha:1)
            }
            else {
                submitButtonEnabled = false
                submitButton.isEnabled = false
                submitButton.backgroundColor = UIColor(red:1, green:0.8, blue:0.38, alpha:1)
            }
        }
    }

    @IBAction func submitButtonClicked(_ sender: UIButton) {
        // NewMeter_SAP_NewService.sharedInstance.postNewService()
//        NewMeter_SAP_NewService.sharedInstance.getToken(viewController: self, meterNumber: self.meterNumberTextField.text!, meterSizeCode: meterSizeCodeTextField.text!, numberOfDials: numberOfDialsTextField.text!, enterMeterNumber: enterMeterNumber.getMeterReadEntryString(), addressText: addressTextField.text!)
        
        // MARK: SAP IOS SDK
        NewMeter_SAP_NewService.meterNumber = self.meterNumberTextField.text!
        NewMeter_SAP_NewService.meterSizeCode = meterSizeCodeTextField.text!
        NewMeter_SAP_NewService.numberOfDials = numberOfDialsTextField.text!
        NewMeter_SAP_NewService.enterMeterNumber = enterMeterNumber.getMeterReadEntryString()
        NewMeter_SAP_NewService.address = addressTextField.text!
        NewMeter_SAP_NewService.sharedInstance.createEntity(viewController:self)
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // TODO: segue things, once the screen has been stitched in
        if segue.identifier == "toOCR" {
            let destinationVC = segue.destination as? OCRViewController
            destinationVC?.lowValue = currentMeterRead.lowParameter
            destinationVC?.highValue = currentMeterRead.highParameter
            destinationVC?.numDigits = currentMeterRead.numberOfDials
        }
    }
    
    //MARK: - Navigation
    @IBAction func unwindToHome(segue: UIStoryboardSegue){
//        if let sourceVC = segue.source as? CameraCaptureViewController{
//            //print("Unwound from camera")
//            if let imageFromView = sourceVC.image {
//                //print("assigned image to slot 1")
//                meterImage = imageFromView
//            }
//            //print("came from camera")
//        }
        if let sourceVC = segue.source as? OCRViewController {
            if let val_Read = sourceVC.valRead {
                if val_Read > -1 {
                    enterMeterNumber.clearMeterReadEntry()
                    enterMeterNumber.populateWithValues(val_Read)
                    enterMeterNumber.validateFields()
                    barDidValidate(enterMeterNumber.meterReadBarValid, "")
                }
            }
            print("Unwound from OCR")
        }
    }
}
