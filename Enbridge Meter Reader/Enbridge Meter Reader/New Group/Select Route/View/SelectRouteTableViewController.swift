//
//  SelectRouteTableViewController.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-17.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit
import SAPFiori
import MapKit

class SelectRouteTableViewController: UITableViewController, SAPFioriLoadingIndicator {
    var loadingIndicator: FUILoadingIndicatorView?
    static var error : Error?
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static var refreshControlRef : UIRefreshControl?
    static var blurEffectView : UIVisualEffectView?
    private var blurViewCounter = 0
    static let progressView = FUIProcessingIndicatorView(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
    
    
    var segueToPerform = ""
    var shouldShowNetworkToolbar = false
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadBlurView()

        
        Model.sharedInstance.selectRouteRefreshDelegate = self
        
         // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        // set UI attributes
        navigationController?.navigationBar.tintColor = UIColor(red:1, green:0.72, blue:0.11, alpha:1)
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        
        // Add refreshcontrol UI
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        SelectRouteTableViewController.refreshControlRef = refreshControl
        
        Persistence.shared.loadRoutes()
      
        
        // Set up observer when notification called in AppDelegate reload select route table when a service request is cancelled or unscheduled
        NotificationCenter.default.addObserver(self, selector: #selector(removeUnscheduledFromTable), name: NSNotification.Name(rawValue: "reloadTables"), object: nil)
        // When route is empty because unscheduled only work order in route
        NotificationCenter.default.addObserver(self, selector: #selector(removeUnscheduledFromTable), name: NSNotification.Name(rawValue: "emptyRoute"), object: nil)
        
        
        // new integration getwork call
        GetWorkOrderFromBackEnd_SAP_Click.sharedInstance.loadData{
            OperationQueue.main.addOperation({
                self.hideFioriLoadingIndicator()
                if (SelectRouteTableViewController.error != nil) {
                    //print("! NO NEW DATA / LOADING DATA FAILED: \(SelectRouteTableViewController.error?.localizedDescription)")
                    
                } else {
                    self.hideFioriLoadingIndicator()
                    self.tableView.reloadData()
                }
            })
        }
        
        
    }
    
    
    func loadBlurView () {
        if blurViewCounter == 0 {
            LocalStoreManager.dataBytes = 0
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
            SelectRouteTableViewController.blurEffectView = UIVisualEffectView(effect: blurEffect)
            SelectRouteTableViewController.blurEffectView!.frame = (self.navigationController?.view.frame)!
            SelectRouteTableViewController.blurEffectView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.navigationController?.view.addSubview(SelectRouteTableViewController.blurEffectView!)
            //self.navigationController?.view.addSubview((self.navigationController?.view.activityIndicatorView)!)
            
            //progressView.textLabel = UILabel(frame: CGRect(origin: CGPoint(x:100.0,y:100.0), size: CGSize(width: 100.0, height: 100.0)))
            let pv = SelectRouteTableViewController.progressView
            pv.center = (SelectRouteTableViewController.blurEffectView?.center)!
            pv.textLabel.isHidden = false
            pv.textLabel.font = pv.textLabel.font.withSize(20)
            pv.textLabel.numberOfLines = 2
            pv.show()
            pv.startAnimating()
            self.navigationController?.view.addSubview(pv)
        }
        blurViewCounter = blurViewCounter + 1
    }
    
    func unloadBlurView() {
        blurViewCounter = blurViewCounter - 1
        if blurViewCounter <= 0 {
            SelectRouteTableViewController.progressView.stopAnimating()
            SelectRouteTableViewController.progressView.dismiss(animated: false)
            
            SelectRouteTableViewController.blurEffectView?.removeFromSuperview()
            self.navigationController?.view.activityIndicatorView.stopAnimating()
            blurViewCounter = 0
        }
        
    }
    
    
    // Function called by notification in AppDelegate to remove unschedueld work from select route table when a service request is cancelled or  unscheduled
    @objc func removeUnscheduledFromTable() {
        self.tableView.reloadData()
    }

    
    override func viewDidAppear(_ animated: Bool) {

        if Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID] != nil {
            for offcycle in Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]!.meterReads {
                print("! Meter number: \(offcycle.meterNumber)")
                //print("! Read date: \(offcycle.mmrReadDate)")
            }
        }
       
        
        locationManager.requestWhenInUseAuthorization()
        
        if segueToPerform != "" {
            performSegue(withIdentifier: segueToPerform, sender: self)
            segueToPerform = ""
        }
        
        self.tableView.reloadData()
        
        if shouldShowNetworkToolbar {
            appDelegate.showNetworkToolbar(isOnline: false)
            shouldShowNetworkToolbar = false
        }
    }
    

    // Removed messages from the day before
    func removeOldMessages() {
        
        // Count message index
        var index = 0
        for message in messages {
            
            // Convert date String to Date() using formatter
            //print ("string date: \(message.date)")
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "MMMM dd, yyyy"
            let date = formatter.date(from: message.date)
            
            // If message is from yesterday or earlier, remove it
            if let messageDate = date, isDateYesterdayOrEarlier(date: messageDate) {
                messages.remove(at: index)
            }
            // Increment index as go through messages list
            index = index + 1
        }
    }
    
    // If date is yesterday or earlier, return true
    func isDateYesterdayOrEarlier(date: Date) -> Bool {
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        if date <= yesterday {
            return true
        } else {
            return false
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Model.sharedInstance.routes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        switch indexPath.row {
//        case 0:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "periodic", for: indexPath) as! PeriodicCell
//
//            return cell
//        case 1:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "offCycle", for: indexPath) as! OffCycleCell
//
//            return cell
//        case 2:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "outCard", for: indexPath) as! OutCardCell
//
//            return cell
//        default:
//            return UITableViewCell()
//        }
        
        // -- Here value changes --- //
        // --- Debug values here --- //
        
        let routeKeys = Array(Model.sharedInstance.routes.keys)
        if indexPath.row >= routeKeys.count { return UITableViewCell()}  // in case receive an cancel notification here..
        let currentRouteKey = routeKeys[indexPath.row]
        
        guard let currentRoute = Model.sharedInstance.routes[currentRouteKey] else { return UITableViewCell() }
        
        switch(currentRoute.type!){
        case .PERIODIC:
            let cell = tableView.dequeueReusableCell(withIdentifier: "periodic", for: indexPath) as! PeriodicCell
            cell.routeKey = currentRoute.id
            cell.associatedRoute = currentRoute
            cell.mruNumber?.text = currentRoute.id
            cell.uploading = currentRoute.uploading ?? false
            cell.uploadingText = currentRoute.uploadText ?? ""
            cell.blockCell = currentRoute.blockCell ?? false
            if(currentRoute.status == .completed || currentRoute.status == .pending){
                cell.submitted = true
            }else{
                cell.submitted = false
            }
            cell.hasBeenStarted = currentRoute.status != .available
            return cell
            
        case .OFFCYCLE:
            let cell = tableView.dequeueReusableCell(withIdentifier: "offCycle", for: indexPath) as! OffCycleCell
            let newAnnotation = Annotation()
            cell.uploading = currentRoute.uploading ?? false
            cell.blockCell = currentRoute.blockCell ?? false
            cell.uploadingText = currentRoute.uploadText ?? ""
            OffCycleCell.hasBeenStarted = currentRoute.status != .available
            
            
            if (indexPath.row < currentRoute.meterReads.count) {
//                newAnnotation.coordinate = CLLocationCoordinate2D(latitude:
//                    (currentRoute.meterReads[indexPath.row].latitude?)!,longitude: currentRoute.meterReads[indexPath.row].longitude?)
            }else {
                newAnnotation.coordinate = CLLocationCoordinate2D(latitude:0,longitude:0)
            }

            cell.routeKey = currentRoute.id
            cell.associatedRoute = currentRoute
            return cell
        
        case .OUTCARD:
            let cell = tableView.dequeueReusableCell(withIdentifier: "outCard", for: indexPath) as! OutCardCell
            cell.uploading = currentRoute.uploading ?? false
            cell.uploadingText = currentRoute.uploadText ?? ""
            cell.blockCell = currentRoute.blockCell ?? false    
            cell.routeKey = currentRoute.id
            cell.associatedRoute = currentRoute
            
            cell.hasBeenStarted = currentRoute.status != .available
            return cell

        }
        
        
    }
    
    @IBAction func unwindToSelectRoute(segue: UIStoryboardSegue) {
        if let configureRouteVC = segue.source as? ConfigureRouteViewController {
            segueToPerform = configureRouteVC.identifier
        }
        else if let routeSummaryVC = segue.source as? RouteSummaryViewController {
            if routeSummaryVC.shouldShowNetworkToolbar {
                shouldShowNetworkToolbar = true
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "outCardCellSummary"?:
//            let destinationVC = segue.destination as! RouteSummaryViewController
            RouteSummaryViewController.caller = "outCardCellSummary"
        case "periodicCellSummary"?:
            RouteSummaryViewController.caller = "periodicCellSummary"
        case "periodicCellViewSubmission"?:
            RouteSummaryViewController.caller = "periodicCellViewSubmission"
        case "periodicCellBeginRoute"?:
            ConfigureRouteViewController.previousViewController = self
            let Vc = segue.destination as? ConfigureRouteViewController
            Vc?.periodicCell = (tableView.dequeueReusableCell(withIdentifier: "periodic") as! PeriodicCell)
        case "offCycleCellBeginRoute"?:
            ConfigureRouteViewController.previousViewController = self
        case "outCardCellBeginRoute"?:
            ConfigureRouteViewController.previousViewController = self
        default:
            return
        }
    }
    
    @objc func refresh() {
        //self.loadBlurView()
        if Reachability.networkReachabilityForInternetConnection()?.isReachable ?? false {
            LocalStoreManager.ShareInstance?.offlineToOnlineSync()
        }else {
            let alertController = UIAlertController(title: "No Internet Connection", message: String(describing: "Device maybe offline"), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default){ _ in
                SelectRouteTableViewController.refreshControlRef?.endRefreshing()
            })
            self.present(alertController, animated: true)
        }
    }
}

extension SelectRouteTableViewController: ModelDelegate {

    func didRefreshModel() {
        tableView.reloadData()
    }
}
