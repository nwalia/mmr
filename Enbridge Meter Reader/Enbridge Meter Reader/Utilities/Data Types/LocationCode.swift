//
//  LocationCode.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-17.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation

struct LocationCode: Codable, Equatable {
    var number: String
    var id: String
    var description: String
    var isOutside: Bool
    var outsideCode: String
    var selected: Bool
    
    static let codeMappings = [
        "AB": ("01", "Outside Back"),
        "AF": ("02", "Outside Front"),
        "AL": ("03", "Outside Left"),
        "AO": ("04", "Outside Meter"),
        "AG": ("05", "Glass Block"),
        "AR": ("06", "Outside Right"),
        "AT": ("07", "Rooftop"),
        "BA": ("08", "BkDr Back Wall"),
        "BB": ("09", "BkDr Back Wall"),
        "BC": ("10", "BkDr Under stairs"),
        "BD": ("11", "BkDr Dog OK"),
        "BE": ("12", "BkDr Basement Cupboard"),
        "BG": ("13", "BkDr Under stairs"),
        "BH": ("14", "BkDr Cupboard"),
        "BI": ("15", "BkDr Bathroom"),
        "BJ": ("16", "BkDr Front Room"),
        "BK": ("17", "BkDr Kitchen"),
        "BN": ("18", "BkDr Bedroom"),
        "BO": ("19", "BkDr"),
        "BS": ("20", "BkDr Dangerous stairs"),
        "BX": ("21", "BkDr Bad Dog"),
        "CA": ("22", "Cellar Dr Front Wall"),
        "CB": ("23", "Cellar Dr Back Wall"),
        "CC": ("24", "Cellar Dr Under Stairs"),
        "CD": ("25", "Cellar Dr Dog OK"),
        "CE": ("26", "Cellar Dr Cupboard"),
        "CJ": ("27", "Cellar Dr Under Stairs"),
        "CO": ("28", "Cellar Dr"),
        "CX": ("29", "Cellar Dr Bad Dog"),
        "FA": ("30", "FrDr Front Wall"),
        "FB": ("31", "FrDr Back Wall"),
        "FC": ("32", "FrDr Under Stairs"),
        "FD": ("33", "FrDr Dog OK"),
        "FE": ("34", "FrDr Basement Cupboard"),
        "FG": ("35", "FrDr Under Stairs"),
        "FH": ("36", "FrDr Cupboard"),
        "FI": ("37", "FrDr Bathroom"),
        "FK": ("38", "FrDr Kitchen"),
        "FN": ("39", "FrDr Bedroom"),
        "FO": ("40", "FrDr"),
        "FS": ("41", "FrDr Dangerous Stairs"),
        "FX": ("42", "FrDr Bad Dog"),
        "GA": ("43", "Garage Dr Front Wall"),
        "GB": ("44", "Garage Dr Back Wall"),
        "GD": ("45", "Garage Dr Dog OK"),
        "GE": ("46", "Garage Dr Cupboard"),
        "GG": ("47", "Garage Dr Under Stairs"),
        "GK": ("48", "Garage Dr Kitchen"),
        "GO": ("49", "Garage Dr"),
        "GS": ("50", "Garage Dr Dangerous Stairs"),
        "HA": ("51", "Thru Hse Front Door"),
        "HB": ("52", "Thru Hse Back Door"),
        "HC": ("53", "Thru Hse Under Stairs"),
        "HK": ("54", "Thru Hse Kitchen"),
        "IA": ("55", "Via Store Front Wall"),
        "IB": ("56", "Via Store Back Wall"),
        "ID": ("57", "Via Store Dog OK"),
        "IG": ("58", "Via Store Under Stairs"),
        "IH": ("59", "Via Store Cupboard"),
        "IK": ("60", "Via Store Kitchen"),
        "IN": ("61", "Via Store Bedroom"),
        "IO": ("62", "Via Store"),
        "IS": ("63", "Via Store Dangerous Stairs"),
        "SA": ("64", "SdDr Front Wall"),
        "SB": ("65", "SdDr Back Wall"),
        "SC": ("66", "SdDr Under Stairs"),
        "SD": ("67", "SdDr Dog OK"),
        "SE": ("68", "SdDr Basement Cupboard"),
        "SG": ("69", "SdDr Under Stairs"),
        "SH": ("70", "SdDr Cupboard"),
        "SI": ("71", "SdDr Bathroom"),
        "SK": ("72", "SdDr Kitchen"),
        "SN": ("73", "SdDr Bedroom"),
        "SO": ("74", "SdDr"),
        "SS": ("75", "SdDr Dangerous Stairs"),
        "SX": ("76", "SdDr Bad Dog"),
        "TA": ("77", "TrapDr Front Wall"),
        "TD": ("78", "TrapDr Dog OK"),
        "TO": ("79", "TrapDr"),
        "QR": ("80", "Glass Block Rt"),
        "QL": ("81", "Glass Block Left"),
        "QF": ("82", "Glass Block Front"),
        "QB": ("83", "Glass Block Back")
    ]
    
    
    
    init(){
        number = "-1"
        id = "-1"
        description = "none"
        isOutside = false
        outsideCode = "-1"
        selected = false
    }
    
    init(number: String, id: String, description: String){
        self.number = number
        self.id = id
        self.description = description
        self.isOutside = (id.first == "A")
        if self.isOutside {
            self.outsideCode = "OS"
        } else {
            self.outsideCode = "IS"
        }
        self.selected = false
    }
    
    init(_ code: String){
        (self.number, self.description) = LocationCode.codeMappings[code] ?? ("-1","No location code found")
        self.id = code
        self.isOutside = (self.id.first == "A")
        if self.isOutside {
            self.outsideCode = "OS"
        } else {
            self.outsideCode = "IS"
        }
        self.selected = false
    }
    
    static func ==(a: LocationCode, b: LocationCode) -> Bool {
        return a.id == b.id
    }
}
