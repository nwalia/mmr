//
//  ServiceRequestScreenViewController.swift
//  Enbridge Meter Reader
//
//  Created by Lauren Abramsky on 2017-11-29.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
import SAPCommon

protocol ServiceRequestDelegate: class {
    func populateCustomerInformation(onDashboard: Bool, hideLine: Bool, newRead: MeterRead, buttonsEnabled: Bool)
}


class ServiceRequestScreenViewController: UIViewController, UITextFieldDelegate, MeterReadBarDelegate {
    
    
    // Customer Info Delegate for Service Request
    static weak var delegate: ServiceRequestDelegate?
    
    // Keeping track of previous delegate for reassignment when segueing to Dashboard
    var previousDelegate: Any?
    
    // If the user has set a filter and/or searched on the previous screen, we keep track of that set here.
    var filteredMeterReads: [MeterRead]?
    
    // Temp variables to test different UI's - to replace with integrations
    @IBOutlet weak var marginView: UIView!
    var currentMeterRead: MeterRead!
    var serviceRequestType: String = "ZT"
    var serviceRequestTypeDescription: String = ""
    var noKey: Bool = false
    var noTime: Bool = false
    var numberOfDials: Int!
    var specialInstructions: String = "This is an existing special instruction."
    var hasTroubleCode: Bool = false
    var code: String = "Pad Lock"
    var readDirection: String!
    var firstMeterReadValid: Bool = false
    var secondMeterReadValid: Bool = false
    
    
    // Variables for checking valid fields
    var meterReadBarValid: Bool = false
    var secondReadBarValid: Bool = false
    var completionCodeEnabled: Bool = false
    var secondBarEnabled: Bool = false
    
    // Cointainers for codes
    var firstTroubleCode: TroubleCode?
    var secondTroubleCode: TroubleCode?
    
    // trigger to segue out
    var shouldLoadNextMeter = false
    
    private let logger = Logger.shared(named: "Serivce Request Screen View")
    
    // Top section of UI
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var serviceRequestBar: UIView!
    @IBOutlet weak var customerInformation: CustomerInformation!
    @IBOutlet weak var serviceRequestInstructions: UIStackView!
    @IBOutlet weak var serviceRequestBarText: UILabel!
    @IBOutlet weak var progressBar: ProgressBarView!
    
    // Special instructions and buttons
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var beginWorkOrderButton: UIButton!
    @IBOutlet weak var troubleCodeButton: UIButton!
    @IBOutlet var specialInstructionsStackView: UIStackView!
    @IBOutlet weak var specialInstructionsButton: UIButton!
    @IBOutlet weak var specialInstructionsText: UILabel!
    @IBOutlet weak var completionCodeButton: UIButton!
    @IBOutlet weak var serviceRequestInstructionsLabel: UILabel!
    
    // Meter read variables
    @IBOutlet weak var meterReadBar: MeterReadBarView!
    @IBOutlet weak var preExchangeText: UILabel!
    @IBOutlet weak var postExchangeText: UILabel!
    @IBOutlet weak var secondMeterReadBar: MeterReadBarView!
    
    // For figuring out which image was requested
    var wantsImageForMeter1: Bool = false
    
    // Image containers
    var meterImage1: UIImage?
    var meterImage2: UIImage?
    
    // Checking if trouble codes were already entered
    var hasEnteredTroubleCodes: Bool = false
    
    // Used when meter is called after user has opened service request screen
    var didOpenMeter: Bool = false
    
    // UI Constraints to vary depending on bars that are showing
    var completionCodeButtonToSecondBarConstraint: NSLayoutConstraint?
    var preExchangeTextToFirstBarConstraint: NSLayoutConstraint?
    var firstBarToTroubleCodeButtonConstraint: NSLayoutConstraint?
    var completionCodeButtonToFirstBarConstraint: NSLayoutConstraint?
    var preExchangeTextToTroubleCodeButtonConstraint: NSLayoutConstraint?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logger.logLevel = .debug
        
        Persistence.shared.saveRoutes()
        
        firstBarToTroubleCodeButtonConstraint = NSLayoutConstraint(item: meterReadBar as Any, attribute: .top, relatedBy: .equal, toItem: troubleCodeButton, attribute: .bottom, multiplier: 1.0, constant: 36.0)
        completionCodeButtonToFirstBarConstraint = NSLayoutConstraint(item: completionCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: meterReadBar, attribute: .bottom, multiplier: 1.0, constant: 36.0)
        completionCodeButtonToSecondBarConstraint = NSLayoutConstraint(item: completionCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: secondMeterReadBar, attribute: .bottom, multiplier: 1.0, constant: 36.0)
        preExchangeTextToFirstBarConstraint = NSLayoutConstraint(item: preExchangeText as Any, attribute: .bottom, relatedBy: .equal, toItem: meterReadBar, attribute: .top, multiplier: 1.0, constant: -18.0)
        preExchangeTextToTroubleCodeButtonConstraint = NSLayoutConstraint(item: preExchangeText as Any, attribute: .top, relatedBy: .equal, toItem: troubleCodeButton, attribute: .bottom, multiplier: 1.0, constant: 36.0)
        
        
        
        // Set variables for meter read
        serviceRequestType = currentMeterRead.typeCode!
        code = currentMeterRead.mmrNotificationCode ?? ""
        noKey = currentMeterRead.hasKey ?? false
        if currentMeterRead.appointmentTime != nil {
            noTime = false
        } else {
            noTime = true
        }
        numberOfDials = currentMeterRead.numberOfDials
        let jobId = currentMeterRead.jobID
        let gridNumber = currentMeterRead.gridNumber
        
        // Service request instructions should display due date, wams comments long, and permanent service note.
        var serviceRequestInstructionsText = ""
        
        // Format required/due date
        if currentMeterRead.requiredDate != nil {
            let requiredDateShort = currentMeterRead.requiredDate?.split(separator: "T").first
//            let requiredDateDropLast = currentMeterRead.requiredDate!.dropLast(10)
//            let requiredDateShort = requiredDateDropLast.dropFirst(9)
            //let requiredDateShort = requiredDateString.substring(to: requiredDateString.index(requiredDateString.startIndex, offsetBy: 4))
            serviceRequestInstructionsText += "Complete by \(requiredDateShort ?? ""). "
        }
        // Add wams comments long
        if currentMeterRead.wamsCommentsLong != nil {
            serviceRequestInstructionsText += "\(currentMeterRead.wamsCommentsLong!). "
        }
        // Add PSN
        if currentMeterRead.permServNote != nil {
            serviceRequestInstructionsText += " \(currentMeterRead.permServNote!)"
        }
        serviceRequestInstructionsLabel.text = serviceRequestInstructionsText
        if currentMeterRead.specialInstructions != "" && !self.view.subviews.contain(specialInstructionsStackView) {
            serviceRequestInstructionsLabel.text = serviceRequestInstructionsText
            //Update UI
            specialInstructionsButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
            view.addSubview(specialInstructionsStackView)
            
            //Update constraints
            NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = false
            NSLayoutConstraint(item: specialInstructionsStackView as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 24.0).isActive = true
            NSLayoutConstraint(item: specialInstructionsStackView as Any, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
            NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: specialInstructionsStackView, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = true
        }
        
        
        //meterReadBar.clearMeterReadEntry()
        //secondMeterReadBar.clearMeterReadEntry()
        
        // Set up meter read bars (only battery exchange has 2)
        //        meterReadBar.setUpMeterBar("firstMeterReadBar", numberOfDials)
        //        if serviceRequestType == "ZB" {
        //            secondMeterReadBar.setUpMeterBar("secondMeterReadBar", numberOfDials)
        //        } else {
        //            hideSecondMeterReadBar()
        //        }
        //
        // Populate progress bar with values
        progressBar.populate(type: .offcycle, shouldRefreshWithNetworkChange: true)
        
        // Get trouble codes and modify button if one exists.
        populateTroubleCodes()
        
        // Style the bar based on service request type
        let barStyle = ServiceRequestColorStyle(code: serviceRequestType)
        
        serviceRequestBar.backgroundColor = barStyle.backgroundColor
        serviceRequestBar.layer.borderColor = barStyle.borderColor.cgColor
        serviceRequestBar.layer.borderWidth = CGFloat(barStyle.borderWidth)
        serviceRequestTypeDescription = currentMeterRead.mmrNotificationCode ?? "" //barStyle.description
        
        
        // If meter is cancelled/unscheduled, customize bar text. Otherwise, normal bar text.
        if currentMeterRead.cancelled {
            serviceRequestBarText.text = "WORK ORDER CANCELLED"
            
        } else if currentMeterRead.unscheduled {
            serviceRequestBarText.text = "WORK ORDER UNSCHEDULED"
            
        } else {
            // If type is redlock and code exists, then display in bar text
            if serviceRequestType == "YD" && code != "" {
                //##### uncomment this line to add the MMR_RedLockType #######
                serviceRequestBarText.text = "\(serviceRequestType) - \(serviceRequestTypeDescription) - \(currentMeterRead.mmrRedLockType!)  /  Work Order \(jobId)  /  Grid \(gridNumber)"
            } else {
                serviceRequestBarText.text = "\(serviceRequestType) - \(serviceRequestTypeDescription)  /  Work Order \(jobId)  /  Grid \(gridNumber)"
            }
        }
        
        // Set corner radius where needed on UI
        troubleCodeButton.layer.cornerRadius = 7
        specialInstructionsButton.layer.cornerRadius = 7
        completionCodeButton.layer.cornerRadius = 10
        
        // Set trouble code / special instructions buttons text to yellow if values have been selected
        if hasEnteredTroubleCodes {
            troubleCodeButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
        } else {
            troubleCodeButton.setTitleColor(.white, for: .normal)
        }
        
        if currentMeterRead.specialInstructions != nil && currentMeterRead.specialInstructions! != "" {
            specialInstructionsButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
        } else {
            
            //Special instructions empty, so remove title
            specialInstructionsButton.setTitleColor(UIColor.white, for:.normal)
            specialInstructionsStackView.removeFromSuperview()
            NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = true
            
            // Add to dictionary, so pop up doesn't show if user adds special instructions
            GlobalVariables.specialInstructionsDict[currentMeterRead.fullMeterId] = true
        }
        
        // Show special instructions on screen
        specialInstructionsText.text = currentMeterRead.specialInstructions
        
        // Set completion code text colours for disabled and normal states
        completionCodeButton.setTitleColor(GlobalConstants.DARK_GRAY, for: .normal)
        completionCodeButton.setTitleColor(UIColor.gray, for: .disabled)
        
        
        
        // Set up observer when notification called in AppDelegate reload service request screen when a service request is cancelled or unscheduled or new work is added
        NotificationCenter.default.addObserver(self, selector: #selector(loadServiceRequestScreen), name: NSNotification.Name(rawValue: "newWork"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadServiceRequestScreen), name: NSNotification.Name(rawValue: "reloadTables"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadServiceRequestScreen), name: NSNotification.Name(rawValue: "emptyRoute"), object: nil)
        
        
    }
    
    // Function called by notification in AppDelegate to reload service request screen when a service request is cancelled or unscheduled
    @objc func loadServiceRequestScreen() {
        didOpenMeter = true
        viewWillAppear(true)
        viewDidLoad()
        viewDidAppear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // unhides UI elements on reload
        secondMeterReadBar.isHidden = false
        preExchangeText.isHidden = false
        postExchangeText.isHidden = false
        //checkForNetworkStatus(currentMeterRead)
        currentMeterRead.status = Utilities.sharedInstance.checkForNetworkStatus(currentMeterRead)
        beginWorkOrderButton.isEnabled = currentMeterRead.status == .available
        
        // Update constraints
        firstBarToTroubleCodeButtonConstraint?.isActive = false
        completionCodeButtonToFirstBarConstraint?.isActive = false
        completionCodeButtonToSecondBarConstraint?.isActive = true
        preExchangeTextToFirstBarConstraint?.isActive = true
        preExchangeTextToTroubleCodeButtonConstraint?.isActive = true
        
        if shouldLoadNextMeter {
            
            let meterReadSet = (filteredMeterReads != nil ? filteredMeterReads : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads)!
            
            let currentReadIndex = meterReadSet.firstIndex{
                if $0 === currentMeterRead {
                    return true
                }
                return false
            }
            
            if let index = currentReadIndex, index <= meterReadSet.count && index+1 < meterReadSet.count {
                currentMeterRead = meterReadSet[(currentReadIndex!)+1]
                self.numberOfDials = currentMeterRead.numberOfDials
                DashboardViewController.currentMeterLocation = currentMeterRead.locationCode
                
            }
            else {
                // We're at the end of the route, pop to dashboard
                self.navigationController?.popViewController(animated: true)
            }
            
            self.viewDidLoad()            
            shouldLoadNextMeter = false
            
        }
        
        // Populate customer information - assigned delegate in awake from nib in Customer Information swift file
        if currentMeterRead.status == .completed || currentMeterRead.cancelled || currentMeterRead.unscheduled {
            
            // If meter read is cancelled, load without edit buttons but do not hide the line
            ServiceRequestScreenViewController.delegate?.populateCustomerInformation(onDashboard: true, hideLine: false, newRead: currentMeterRead!, buttonsEnabled: (currentMeterRead.status != .available))
            
        } else {
            ServiceRequestScreenViewController.delegate?.populateCustomerInformation(onDashboard: false, hideLine: false, newRead: currentMeterRead!, buttonsEnabled: (currentMeterRead.status != .available))
            
        }
        
        
        
        if meterReadBar != nil {
            
            meterReadBar.delegate = self
            
            // Call awakeFromNib to re-initialize meter read bar, to handle moving to screens with less/more number of dials
            currentMeterRead.newMeterNumberOfDials != nil ? (meterReadBar.numberOfDials = currentMeterRead.newMeterNumberOfDials ?? currentMeterRead.numberOfDials) : (meterReadBar.numberOfDials = currentMeterRead.numberOfDials)
            meterReadBar.awakeFromNib()
            self.meterReadBar.subviews.enumerated().forEach{$0 < self.meterReadBar.subviews.count ? $1.removeFromSuperview() : print("Remaining View: \($1.description)")}
            
            // If meter number has been changed, update with new number of dials
            if let newNumDials = currentMeterRead.newMeterNumberOfDials {
                meterReadBar.setUp(meterReadBar, "firstMeterReadBar", newNumDials)
            } else {
                meterReadBar.setUp(meterReadBar, "firstMeterReadBar", numberOfDials)
            }
            
            // If reading exists, populate bar
            if currentMeterRead.reading != nil {
                meterReadBar.populateWithValues(currentMeterRead.reading)
            }
            
            // Handle battery exchange case with second meter read bar
            if serviceRequestType == "ZB" {
                secondMeterReadBar.delegate = self
                
                // Call awakeFromNib to re-initialize meter read bar, to handle moving to screens with less/more number of dials
                secondMeterReadBar.awakeFromNib()
                self.secondMeterReadBar.subviews.enumerated().forEach{$0 < self.secondMeterReadBar.subviews.count ? $1.removeFromSuperview() : print("Remaining View: \($1.description)")}
                
                // If meter number has been changed, update second meter read bar with new number of dials
                if let newNumDials = currentMeterRead.newMeterNumberOfDials {
                    meterReadBar.clearMeterReadEntry()
                    meterReadBar.disableMeterReadBar()
                    secondMeterReadBar.setUp(meterReadBar, "secondMeterReadBar", newNumDials)
                } else {
                    secondMeterReadBar.setUp(secondMeterReadBar, "secondMeterReadBar", numberOfDials)
                }
                
                // If reading exists, populate bar
                if currentMeterRead.postReading != nil {
                    secondMeterReadBar.populateWithValues(currentMeterRead.postReading)
                }
                
            } else {
                // Not battery exchange, hide second bar
                hideSecondMeterReadBar()
            }
        }
        
        
        // If bar background is black or red, set text colour to white
        if serviceRequestType == "YE" || serviceRequestType == "YC" || serviceRequestType == "YD" {
            serviceRequestBarText.textColor = .white
        } else {
            serviceRequestBarText.textColor = .black
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Special instructions pop up - add alert if there is an existing service instruction
        
        if currentMeterRead.specialInstructions != nil
            && currentMeterRead.specialInstructions != ""
            && !checkGlobalDictionary(currentMeterRead.fullMeterId)
            && !currentMeterRead.cancelled
            && !currentMeterRead.unscheduled {
            AudioServicesPlaySystemSound(1315);
            let alert = UIAlertController(title: "Special Instructions", message: "\n\(currentMeterRead.specialInstructions!)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Acknowledge", style: .default, handler: {(alert: UIAlertAction!) in self.acknowledge()}))
            self.present(alert, animated: true, completion: nil)
        }
        
        if currentMeterRead.status == .available {
            troubleCodeButton.isEnabled = false
            specialInstructionsButton.isEnabled = false
            disableButton()
            if let meterRead = meterReadBar {
                meterRead.disableMeterReadBar()
                if serviceRequestType == "ZB" {
                    secondMeterReadBar.disableMeterReadBar()
                } else {
                    hideSecondMeterReadBar()
                }
            }
            
            troubleCodeButton.backgroundColor = GlobalConstants.LIGHT_GRAY
            specialInstructionsButton.backgroundColor = GlobalConstants.LIGHT_GRAY
            beginWorkOrderButton.setTitle("Begin Work Order", for: .normal)
            beginWorkOrderButton.isEnabled = true
        }
        else if currentMeterRead.status == .inProgress {
            beginWorkOrderButton.isEnabled = false
            beginWorkOrderButton.setTitle("Work in Progress", for: .normal)
            troubleCodeButton.backgroundColor = GlobalConstants.LIGHT_GRAY
            specialInstructionsButton.backgroundColor = GlobalConstants.LIGHT_GRAY
            troubleCodeButton.backgroundColor = GlobalConstants.GRAY
            specialInstructionsButton.backgroundColor = GlobalConstants.GRAY
        }
        
        // disable all input into the screen if the service has already been completed
        disableOnCompleted()
        
    }
    
    
    // Empty validate function because offcycle always has completion code button enabled
    func barDidValidate(_ isValid: Bool, _ title: String) {
        if isValid {
            currentMeterRead.reading = meterReadBar.getMeterReadEntry()
            currentMeterRead.postReading = secondMeterReadBar.getMeterReadEntry()
            
            currentMeterRead.updateReadDate()
        }
    }
    
    
    // All service request types other than ZB - Battery Order only need one meter read bar
    func hideSecondMeterReadBar() {
        
        // Remove unneccessary elements
        if preExchangeText != nil {
            preExchangeText.isHidden = true
        }
        if postExchangeText != nil {
            postExchangeText.isHidden = true
        }
        
        if secondMeterReadBar != nil {
            secondMeterReadBar.isHidden = true
        }
        
        // Update constraints
        firstBarToTroubleCodeButtonConstraint?.isActive = true
        completionCodeButtonToFirstBarConstraint?.isActive = true
        completionCodeButtonToSecondBarConstraint?.isActive = false
        preExchangeTextToFirstBarConstraint?.isActive = false
        preExchangeTextToTroubleCodeButtonConstraint?.isActive = false
        
    }
    
    // Special case for ZB - Battery Order: 2 meter read bars
    func enableSecondMeterReadBar() {
        
        // Update UI to enable second meter read bar
        secondMeterReadBar.enableMeterReadBar()
        postExchangeText.textColor = GlobalConstants.DARK_GRAY
        
    }
    
    // Enable completion code button
    func enableButton() {
        if !(completionCodeEnabled){
            completionCodeEnabled = true
            completionCodeButton.isEnabled = true
            completionCodeButton.backgroundColor = GlobalConstants.YELLOW
        }
    }
    
    // Disable completion code button
    func disableButton() {
        completionCodeEnabled = false
        completionCodeButton.isEnabled = false
        completionCodeButton.backgroundColor = GlobalConstants.LIGHT_YELLOW
    }
    
    // Empty method to dismiss acknowledge pop up
    func acknowledge() {
    }
    
    // Check if this meter pop up has been seen by this user before, in this session
    func checkGlobalDictionary(_ meterID: String) -> Bool {
        if GlobalVariables.specialInstructionsDict[meterID] != nil {
            // Value in dictionary, don't show pop up
            return true
        } else {
            // Add to dictionary
            GlobalVariables.specialInstructionsDict[meterID] = true
            return false
        }
    }
    //click on enterCompletion Code
    @IBAction func didClickEnterCompletionCode(_ sender: UIButton) {
        validateForEmptyTextFields(inMeterView: self.meterReadBar)
        // secondMeterReadBar
        guard let secondBar = self.secondMeterReadBar else{
            return
        }
        validateForEmptyTextFields(inMeterView: secondBar)
    }
    
    func validateForEmptyTextFields(inMeterView bar: MeterReadBarView) {
        let dials = bar.numberOfDials
        switch true {
        case dials == 4 || dials == 5 || dials == 6:
            if dials == 4 {
                if let input1 = self.meterReadBar.meterReadInput1, let input2 = self.meterReadBar.meterReadInput2, let input3 = self.meterReadBar.meterReadInput3, let input4 = self.meterReadBar.meterReadInput4 {
                    if input1.text == "" && input2.text == "" && input3.text == "" && input4.text == "" {
                        currentMeterRead.reading = nil
                    }
                }
            }else if dials == 5{
                if let input1 = self.meterReadBar.meterReadInput1, let input2 = self.meterReadBar.meterReadInput2, let input3 = self.meterReadBar.meterReadInput3, let input4 = self.meterReadBar.meterReadInput4, let input5 = self.meterReadBar.meterReadInput5 {
                    if input1.text == "" && input2.text == "" && input3.text == "" && input4.text == ""  && input5.text == "" {
                        currentMeterRead.reading = nil
                    }
                }
            }else if dials == 6{
                if let input1 = self.meterReadBar.meterReadInput1, let input2 = self.meterReadBar.meterReadInput2, let input3 = self.meterReadBar.meterReadInput3, let input4 = self.meterReadBar.meterReadInput4, let input5 = self.meterReadBar.meterReadInput5, let input6 = self.meterReadBar.meterReadInput6 {
                    if input1.text == "" && input2.text == "" && input3.text == "" && input4.text == ""  && input5.text == ""  && input6.text == "" {
                        currentMeterRead.reading = nil
                    }
                }
            }
        default:
            return
        }
    }
    
    
    // Editing special instructions
    @IBAction func openSpecialInstructionsNotes(_ sender: UIButton) {
        let sb = UIStoryboard(name: "NotesView", bundle: nil)
        let notesVC = sb.instantiateInitialViewController() as! NotesViewController
        notesVC.title = "Special Instructions"
        notesVC.specialInstructionsPassed = currentMeterRead.specialInstructions
        NotesViewController.caller = self
        notesVC.currentMeter = currentMeterRead
        self.show(notesVC, sender: self)
    }
    
    //MARK: - Navigation
    @IBAction func unwindToHome(segue: UIStoryboardSegue){
        if let sourceVC = segue.source as? OCRViewController {
            print("Unwound from camera")
            
            if let val_Read = sourceVC.valRead {
                if wantsImageForMeter1 && val_Read > -1 {
                    print("assigned image to slot 1")
                    meterReadBar.clearMeterReadEntry()
                    currentMeterRead.reading = val_Read
                    meterReadBar.populateWithValues(val_Read)
                    currentMeterRead.updateReadDate()
                }
                else if val_Read > -1 {
                    print("assigned image to slot 2")
                    secondMeterReadBar.clearMeterReadEntry()
                    currentMeterRead.postReading = val_Read
                    secondMeterReadBar.populateWithValues(val_Read)
                    currentMeterRead.updateReadDate()
                }
            }
            // TODO: Support the post-exchange camera option (meterImage2)
            
        }
            
        else if segue.source is NotesViewController {
            if let senderVC = segue.source as? NotesViewController {
                
                // Save note as new special instructions, and update UI to reflect changes
                if let note = senderVC.note{
                    currentMeterRead.specialInstructions = note
                    currentMeterRead.updateReadDate()
                    specialInstructionsText.text = currentMeterRead.specialInstructions
                    
                    // If blank special instructions note, remove from UI. Otherwise, add items back.
                    if note == "" {
                        
                        //Update UI and constraints
                        specialInstructionsButton.setTitleColor(UIColor.white, for:.normal)
                        specialInstructionsStackView.removeFromSuperview()
                        NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = true
                        
                    } else {
                        
                        //Update UI
                        specialInstructionsButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
                        view.addSubview(specialInstructionsStackView)
                        
                        //Update constraints
                        NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = false
                        NSLayoutConstraint(item: specialInstructionsStackView as Any, attribute: .top, relatedBy: .equal, toItem: customerInformation, attribute: .bottom, multiplier: 1.0, constant: 24.0).isActive = true
                        NSLayoutConstraint(item: specialInstructionsStackView as Any, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
                        NSLayoutConstraint(item: troubleCodeButton as Any, attribute: .top, relatedBy: .equal, toItem: specialInstructionsStackView, attribute: .bottom, multiplier: 1.0, constant: 36.0).isActive = true
                    }
                }
                else{
                    //print("Returned without note")
                }
                
                // Integrations would go here!
                
            }
        }
            
        else if segue.source is ChangeMeterNumberViewController {
            let sourceVC = segue.source as! ChangeMeterNumberViewController
            
            //            //print(sourceVC.newMeterNumberField.text!)
            //            //print(sourceVC.meterSizeCodeField.text!)
            //            //print(sourceVC.numberOfDialsField.text!)
            
            let newNumDials = sourceVC.numberOfDialsField.text
            if newNumDials != "" {
                currentMeterRead.newMeterNumberOfDials = Int(newNumDials!)
                
                // Call awakeFromNib to re-initialize meter read bar, to handle moving to screens with less/more number of dials
                meterReadBar.awakeFromNib()
                
                // Update  meter read bar with new number of dials
                meterReadBar.setUp(meterReadBar, "firstMeterReadBar", Int(newNumDials!)!)
                
                if serviceRequestType == "ZB" {
                    
                    // Call awakeFromNib to re-initialize meter read bar, to handle moving to screens with less/more number of dials
                    secondMeterReadBar.awakeFromNib()
                    
                    // If meter number has been changed, update second meter read bar with new number of dials
                    meterReadBar.clearMeterReadEntry()
                    meterReadBar.disableMeterReadBar()
                    secondMeterReadBar.setUp(meterReadBar, "secondMeterReadBar", Int(newNumDials!)!)
                }
            }
            
            currentMeterRead.newSizeCode = sourceVC.meterSizeCodeField.text!
            currentMeterRead.newMeterNumber = sourceVC.newMeterNumberField.text!
            currentMeterRead.updateReadDate()
            if currentMeterRead.cancelled || currentMeterRead.unscheduled {
                ServiceRequestScreenViewController.delegate?.populateCustomerInformation(onDashboard: true, hideLine: true, newRead: currentMeterRead!, buttonsEnabled: true)
            } else {
                ServiceRequestScreenViewController.delegate?.populateCustomerInformation(onDashboard: false, hideLine: false, newRead: currentMeterRead!, buttonsEnabled: true)
            }
            
        }
        else if segue.source is ChangeMeterLocationViewController {
            let sourceVC = segue.source as! ChangeMeterLocationViewController
            //            //print((sourceVC.confirmedCode?.id)!)
            currentMeterRead.locationCode = sourceVC.confirmedCode! as LocationCode
            
            if currentMeterRead.originalLocation != nil {
                if sourceVC.confirmedCode!.number != currentMeterRead.originalLocation?.number {
                    currentMeterRead.didChangeLocation = true
                } else {
                    currentMeterRead.didChangeLocation = false
                }
            }
            
            DashboardViewController.currentMeterLocation = currentMeterRead.locationCode
            currentMeterRead.updateReadDate()
            if currentMeterRead.cancelled || currentMeterRead.unscheduled {
                ServiceRequestScreenViewController.delegate?.populateCustomerInformation(onDashboard: true, hideLine: true, newRead: currentMeterRead!, buttonsEnabled: true)
            } else {
                ServiceRequestScreenViewController.delegate?.populateCustomerInformation(onDashboard: false, hideLine: false, newRead: currentMeterRead!, buttonsEnabled: true)
            }
        }
        else if segue.source is SelectTroubleCodeViewController {
            let sourceVC = segue.source as! SelectTroubleCodeViewController
            
            troubleCodeButton.setTitle((sourceVC.formattedCodeString ?? "Trouble Code"), for: UIControl.State.normal)
            
            if sourceVC.returnedCodes?.count != 0 {
                currentMeterRead.troubleCodes = sourceVC.returnedCodes!
                hasEnteredTroubleCodes = true
                currentMeterRead.updateReadDate()
                troubleCodeButton.setTitleColor(GlobalConstants.YELLOW, for: UIControl.State.normal)
                if sourceVC.returnedCodes?.count == 1 {
                    firstTroubleCode = sourceVC.returnedCodes?[0]
                    secondTroubleCode = nil
                }
                else if sourceVC.returnedCodes?.count == 2 {
                    firstTroubleCode = sourceVC.returnedCodes?[0]
                    secondTroubleCode = sourceVC.returnedCodes?[1]
                }
            } else {
                currentMeterRead.troubleCodes = [TroubleCode]()
                hasEnteredTroubleCodes = false
                troubleCodeButton.setTitleColor(.white, for: UIControl.State.normal)
            }
            
        }
            
        else if segue.source is SelectCompletionCodeViewController {
            let sourceVC = segue.source as! SelectCompletionCodeViewController
            
            //print("Unwound with code \(sourceVC.selectedCode.number)")
            
            //print("Note: \(sourceVC.returnNote ?? "none"), Number of dials: \(sourceVC.newMeterNumberOfDials ?? 0), Meter number: \(sourceVC.newMeterNumber ?? "none"), Size code: \(sourceVC.newMeterSizeCode ?? "none")")
            
            currentMeterRead.completionCode = sourceVC.selectedCode
            currentMeterRead.updateReadDate()
            //currentMeterRead.newSizeCode = sourceVC.newMeterSizeCode
            currentMeterRead.mmrActivityText = sourceVC.returnNote ?? ""
            //            currentMeterRead.newMeterNumber = sourceVC.newMeterNumber
            //            currentMeterRead.newMeterNumberOfDials = sourceVC.newMeterNumberOfDials
            //            if currentMeterRead.newMeterNumber == nil || (currentMeterRead.newMeterNumber ?? "").isEmpty {
            //                if let reading = meterReadBar.getMeterReadEntry() {
            //                    currentMeterRead.reading = reading
            //                } else {
            //                    currentMeterRead.reading = nil
            //                }
            //            } else {
            //                currentMeterRead.newMeterRead = meterReadBar.getMeterReadEntryString() ?? ""
            //            }
            //            //currentMeterRead.reading = meterReadBar.getMeterReadEntry() ?? 0
            
            guard let r: Reachability = Reachability.networkReachabilityForInternetConnection() else { return }
            if r.isReachable {
                currentMeterRead.status = .completed
            }
            else {
                currentMeterRead.status = .pending
                DashboardViewController.shouldShowNetworkToolbar = true
            }
            
            Model.sharedInstance.checkRouteCompleted(routeId: GlobalConstants.OFFCYCLE_ROUTE_ID)
            
            shouldLoadNextMeter = true
            
        }
    }
    
    func populateTroubleCodes(){
        
        if currentMeterRead.troubleCodes.count == 2 {
            firstTroubleCode = currentMeterRead.troubleCodes[0]
            secondTroubleCode = currentMeterRead.troubleCodes[1]
            hasEnteredTroubleCodes = true
            troubleCodeButton.setTitleColor(GlobalConstants.YELLOW, for: UIControl.State.normal)
        }
        else if currentMeterRead.troubleCodes.count == 1 {
            firstTroubleCode = currentMeterRead.troubleCodes[0]
            secondTroubleCode = nil
            hasEnteredTroubleCodes = true
            troubleCodeButton.setTitleColor(GlobalConstants.YELLOW, for: UIControl.State.normal)
        }
        else {
            firstTroubleCode = nil
            secondTroubleCode = nil
            hasEnteredTroubleCodes = false
            troubleCodeButton.setTitleColor(.white, for: UIControl.State.normal)
        }
        
        let codeString = createFormattedCodeString(codes: currentMeterRead.troubleCodes)
        
        troubleCodeButton.setTitle((codeString ?? "Trouble Code"), for: UIControl.State.normal)
        
    }
    
    func createFormattedCodeString(codes: [TroubleCode]) -> String? {
        
        if codes.count == 1 {
            return "Trouble Code: \(String(format: "%02d", codes[0].number))"
            
        }
        else if codes.count == 2 {
            return "Trouble Code: \(String(format: "%02d", codes[0].number))/\(String(format: "%02d", codes[1].number))"
        }
        else {
            return nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tappedCameraButton(_ title: String) {
        // If camera button pressed on the first meter bar, put image in slot 1, otherwise slot 2
        if title == "firstMeterReadBar" {
            wantsImageForMeter1 = true
        } else {
            wantsImageForMeter1 = false
        }
        performSegue(withIdentifier: "toOCR", sender: nil)
    }
    
    @IBAction func troubleCodeButtonClicked(_ sender: UIButton) {
        NotesViewController.caller = self
        CameraCaptureViewController.zindicator = self.currentMeterRead.checkForWorkOrderType(self.currentMeterRead)
        CameraCaptureViewController.fileName = self.currentMeterRead.jobID
    }
    
    override func didMove(toParent parent: UIViewController?) {
        
        // Reassign delegate if parent is Dashboard (no if in here yet, may need to add logic if moving anywhere else)
        DashboardViewController.customerInformationDelegate = previousDelegate as? DashboardDelegate
        PeriodicDashboardViewController.delegate = previousDelegate as? PeriodicDashboardDelegate
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // If segueing to completion codes, pass through service request type and meter read entries
        if let destinationVC = segue.destination as? SelectCompletionCodeViewController {
            destinationVC.type = currentMeterRead.typeCode!
            destinationVC.numberOfDials = currentMeterRead.numberOfDials
            destinationVC.meterReadEntry = meterReadBar.getMeterReadEntryString()
            destinationVC.currentMeter = self.currentMeterRead
            if let newNumDials = currentMeterRead.newMeterNumberOfDials {
                destinationVC.newMeterNumberOfDials = newNumDials
            }
            if currentMeterRead.typeCode! == "ZB" {
                if currentMeterRead.postReading != nil {
                    // destinationVC.secondMeterReadEntry = String(postReading)
                    destinationVC.secondMeterReadEntry = secondMeterReadBar.getMeterReadEntryString()
                }
                //currentMeterRead.postReading = secondMeterReadBar.getMeterReadEntry()
            }
            
            if meterReadBar.getMeterReadEntry() != nil {
                
                // If meter number has not changed, assign as "reading"
                // Else, as assign reading as "new meter reading"
                
                currentMeterRead.updateReadDate()
                
                if currentMeterRead.newMeterNumber == nil || (currentMeterRead.newMeterNumber ?? "").isEmpty {
                    //currentMeterRead.reading = reading
                    //Utilities.currentMeter.reading = reading
                    currentMeterRead.newMeterRead = ""
                    Utilities.currentMeter.newMeterRead = ""
                } else {
                    currentMeterRead.newMeterRead = meterReadBar.getMeterReadEntryString()
                    Utilities.currentMeter.newMeterRead = meterReadBar.getMeterReadEntryString()
                    currentMeterRead.reading = nil
                    Utilities.currentMeter.reading = nil
                }
            }
            
        } else if segue.destination is SelectTroubleCodeViewController {
            let destinationVC = segue.destination as? SelectTroubleCodeViewController
            var troubleCodes: [TroubleCode] = []
            if currentMeterRead.troubleCodes.count != 0 {
                if let firstCode = firstTroubleCode {
                    troubleCodes.append(firstCode)
                }
                if let secondCode = secondTroubleCode {
                    troubleCodes.append(secondCode)
                }
            }
            destinationVC?.returnedCodes = troubleCodes
            
        } else if segue.destination is ChangeMeterNumberViewController {
            let destinationVC = segue.destination as? ChangeMeterNumberViewController
            destinationVC?.selectedMeter = currentMeterRead
            
        }
        else if segue.identifier == "toOCR" {
            let destinationVC = segue.destination as? OCRViewController
            destinationVC?.lowValue = currentMeterRead.lowParameter
            destinationVC?.highValue = currentMeterRead.highParameter
            destinationVC?.numDigits = numberOfDials
        }
    }
    
    // Disable all buttons and fields when meter is completed
    func disableOnCompleted(){
        if currentMeterRead.status == .completed
        {
            troubleCodeButton.isEnabled = false
            specialInstructionsButton.isEnabled = false
            disableButton()
            meterReadBar.populateWithValues(currentMeterRead.reading)
            meterReadBar.disableMeterReadBar(showMeterReadEntry: true)
            troubleCodeButton.backgroundColor = GlobalConstants.LIGHT_GRAY
            specialInstructionsButton.backgroundColor = GlobalConstants.LIGHT_GRAY
            beginWorkOrderButton.backgroundColor = GlobalConstants.LIGHT_YELLOW
            beginWorkOrderButton.isEnabled = false
            beginWorkOrderButton.setTitle("Work Order Complete", for: .normal)
            ServiceRequestScreenViewController.delegate?.populateCustomerInformation(onDashboard: false, hideLine: false, newRead: currentMeterRead!, buttonsEnabled: false)
            if serviceRequestType == "ZB" {
                secondMeterReadBar.disableMeterReadBar(showMeterReadEntry: true)
            } else {
                hideSecondMeterReadBar()
            }
        }
    }
    
    // MARK: - Begin Work Order
    
    // Called when user taps "Begin Work" button
    @IBAction func beginWorkOrderButtonPressed(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Begin work order", message: "I confirm that I am beginning this work order. I acknowledge that I cannot undo this later.", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Confirm", style: .default, handler: { _ in
            
            //Load the blur view to disable user interaction.
            DispatchQueue.main.async {
                SelectRouteTableViewController.refreshControlRef?.endRefreshing()
                if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                    if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                        selectVC.loadBlurView()
                    }
                }
            }
            self.confirmBeginWorkOrder()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(confirmAction)
        self.present(alert, animated: true)
    }
    
    
    // Called when user confirms alert after tapping "Begin Work" button
    @objc func confirmBeginWorkOrder() {
        
        // Post work service call
    
        do {
            currentMeterRead.mmrStatus = "ON-SITE"
            if !AppDelegate.hasMockData {
                let fetchedEntities = try LocalStoreManager.ShareInstance?.updateOperation(type: RouteType.OFFCYCLE.rawValue, operation: MeterRead.mapTo_Operation(meterRead: currentMeterRead, withCompletionCode: ""))
                print("Fetched From UpdateEntity Function Call:\(String(describing: fetchedEntities?.0)),\(String(describing: fetchedEntities?.1))")
                LogHelper.shared?.info("Change Status to ON-SITE successfully with work ID:" + self.currentMeterRead.jobID, loggerObject: self.logger)
            }
        } catch {
            print("Update Operation Failed: \(error)")
            LogHelper.shared?.error("Change Status to ON-SITE failed with work ID:" + self.currentMeterRead.jobID, loggerObject: self.logger)
        }
        
        LocalStoreManager.ShareInstance?.submitOffCycleRoute{error in
            print(error as Any)
            //Load the blur view to disable user interaction.
            OperationQueue.main.addOperation {
                if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                    if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                        OperationQueue.main.addOperation {
                            selectVC.unloadBlurView()
                        }
                    }
                }
            }
        }
        currentMeterRead.status = .inProgress
        
        // Set UI to enable all fields/buttons
        ServiceRequestScreenViewController.delegate?.populateCustomerInformation(onDashboard: false, hideLine: false, newRead: currentMeterRead!, buttonsEnabled: true)
        troubleCodeButton.isEnabled = true
        specialInstructionsButton.isEnabled = true
        troubleCodeButton.backgroundColor = GlobalConstants.GRAY
        specialInstructionsButton.backgroundColor = GlobalConstants.GRAY
        enableButton()
        meterReadBar.enableMeterReadBar()
        if serviceRequestType == "ZB" {
            secondMeterReadBar.enableMeterReadBar()
        } else {
            hideSecondMeterReadBar()
        }
        beginWorkOrderButton.backgroundColor = GlobalConstants.LIGHT_YELLOW
        beginWorkOrderButton.isEnabled = false
        beginWorkOrderButton.setTitle("Work in Progress", for: .normal)
        Persistence.shared.saveRoutes()
        
    }
}
