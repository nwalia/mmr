//
//  PeriodicDashboardViewController+UITableView.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-26.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Table View Functions

extension PeriodicDashboardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sequenceFlag = Model.sharedInstance.routes[Model.selectedRouteKey]?.resequence
        let sequenceList = Model.sharedInstance.routes[Model.selectedRouteKey]?.resequencedList
        let normalMeterList = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads
        
        if isFiltering() {
            return filteredMeterReads.count
        }else if sequenceFlag! && sequenceList != nil && sequenceList?.count != 0 {
            return sequenceList!.count
        }else {
            return (normalMeterList?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var meterList = [MeterRead]()
        let sequenceFlag = Model.sharedInstance.routes[Model.selectedRouteKey]?.resequence
        let sequenceList = Model.sharedInstance.routes[Model.selectedRouteKey]?.resequencedList
        let normalMeterList = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads
        
        if isFiltering() {
            meterList = filteredMeterReads
        }else if sequenceFlag! && sequenceList != nil && sequenceList?.count != 0 {
            meterList = sequenceList!
        }else {
            meterList = normalMeterList!
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeriodicMeterReadTableViewCell") as! PeriodicMeterReadTableViewCell
        
        let readForCell = meterList[indexPath.row]
        
        if readForCell.newFullMeterId != "" {
            cell.meterNumberLabel.text = readForCell.newFullMeterId
        } else {
            cell.meterNumberLabel.text = readForCell.fullMeterId
        }
        cell.mruLabel.text = "MRU : \(readForCell.routeId )"
        cell.locationCodeLabel.text = "LOC: \(readForCell.locationCode.id )"
        if let est_Skips = readForCell.skips {
            cell.estimationLabel.text = "Est: \(est_Skips)"
        }
        cell.nameLabel.text = readForCell.nameLastFirstComma
        cell.addressLine1Label.text = readForCell.fullAddress
        cell.addressLine2Label.text = readForCell.addressLine2
        cell.troubleCodeIconStackView.isHidden = (readForCell.troubleCodes.count ) == 0
        cell.troubleCodeString1.text = readForCell.troubleCodeString
        cell.keyIconStackView.isHidden = readForCell.keyNumber == nil || (readForCell.keyNumber ?? "").isEmpty
        cell.instructionIconStackView.isHidden = readForCell.specialInstructions == nil || readForCell.specialInstructions == ""
        
        let bgView = UIView()
        cell.selectionStyle = .none
        bgView.backgroundColor = GlobalConstants.SELECTED_CELL
        cell.selectedBackgroundView = bgView
        
        cell.defaultColor = (readForCell.status == .completed || readForCell.status == .pending) ? GlobalConstants.COMPLETED_CELL : .white

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var meterList = [MeterRead]()
        let sequenceFlag = Model.sharedInstance.routes[Model.selectedRouteKey]?.resequence
        let sequenceList = Model.sharedInstance.routes[Model.selectedRouteKey]?.resequencedList
        let normalMeterList = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads
        
        if isFiltering() {
            meterList = filteredMeterReads
        }else if sequenceFlag! && sequenceList != nil && sequenceList?.count != 0 {
            meterList = sequenceList!
        }else {
            meterList = normalMeterList!
        }
        
        let readForCell = meterList[indexPath.row]
        
        PeriodicDashboardViewController.delegate?.populateCustomerInformation(onDashboard: true, hideLine: true, newRead: readForCell, buttonsEnabled: true)
        selectedRead = readForCell
        
        setupMapView()
        
        
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
       Model.sharedInstance.moveItem(at: sourceIndexPath.row, to: destinationIndexPath.row)
    }

}

// MARK: - Table View Drag Extension

extension PeriodicDashboardViewController: UITableViewDragDelegate {
    
    
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        return Model.sharedInstance.dragItems(for: indexPath)
    }
    
}

// MARK: - Table View Drop Extension

extension PeriodicDashboardViewController: UITableViewDropDelegate {
    
    /**
     Ensure that the drop session contains a drag item with a data representation
     that the view can consume.
     */
    func tableView(_ tableView: UITableView, canHandle session: UIDropSession) -> Bool {
        return Model.sharedInstance.canHandle(session)
    }
    
    /**
     A drop proposal from a table view includes two items: a drop operation,
     typically .move or .copy; and an intent, which declares the action the
     table view will take upon receiving the items. (A drop proposal from a
     custom view does includes only a drop operation, not an intent.)
     */
    func tableView(_ tableView: UITableView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UITableViewDropProposal {
        // The .move operation is available only for dragging within a single app.
        if tableView.hasActiveDrag {
            if session.items.count > 1 {
                return UITableViewDropProposal(operation: .cancel)
            } else {
                return UITableViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
            }
        } else {
            return UITableViewDropProposal(operation: .copy, intent: .insertAtDestinationIndexPath)
        }
    }
    
    /**
     This delegate method is the only opportunity for accessing and loading
     the data representations offered in the drag item. The drop coordinator
     supports accessing the dropped items, updating the table view, and specifying
     optional animations. Local drags with one item go through the existing
     `tableView(_:moveRowAt:to:)` method on the data source.
     */
    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
        let destinationIndexPath: IndexPath
        
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            // Get last index path of table view.
            let section = tableView.numberOfSections - 1
            let row = tableView.numberOfRows(inSection: section)
            destinationIndexPath = IndexPath(row: row, section: section)
        }
        
        coordinator.session.loadObjects(ofClass: NSString.self) { items in
            // Consume drag items.
            let stringItems = items as! [String]
            
            var indexPaths = [IndexPath]()
            for (index, _) in stringItems.enumerated() {
                let indexPath = IndexPath(row: destinationIndexPath.row + index, section: destinationIndexPath.section)
                //                self.model.addItem(item, at: indexPath.row)
                indexPaths.append(indexPath)
            }
            
            tableView.insertRows(at: indexPaths, with: .automatic)
        }
    }
}
