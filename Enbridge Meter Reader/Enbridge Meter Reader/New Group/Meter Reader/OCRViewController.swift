//
//  ViewController.swift
//  Meter Reader
//
//  Created by Shahzada, Kaspar (CA - Toronto) on 2018-05-31.
//  Copyright © 2018 Shahzada, Kaspar (CA - Toronto). All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import CoreML

class OCRViewController: UIViewController {
    // MARK: - Segue Properties
    var numDigits: Int? = nil
    var highValue: Int? = nil
    var lowValue: Int? = nil
    var valRead: Int? = -1
    
    // MARK: - Screen Properties
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var detectNumberLabel: UILabel!
    @IBOutlet weak var readingLabel: UILabel!
    
    var readCount: Int = 0
    
    var session = AVCaptureSession()
    
    var windowRect: CGRect? = nil
    
    var OCR: OCRUtility =  OCRUtility()
    
    var isInitalized: Bool = false
    
    var shouldPerformSegue = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        if  !isInitalized {
            isInitalized = true
            initialize()
        }
        imageView.layer.sublayers?[0].bounds = imageView.bounds
    }

    func initialize(){
        startVideoFeed()
        addMaskOverlay()
        
        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],
                                                                mediaType: AVMediaType.video,
                                                                position: AVCaptureDevice.Position.back)
        let captureDevice = discoverySession.devices.first
        if let formatDescription = captureDevice?.activeFormat.formatDescription {
            let dimensions = CMVideoFormatDescriptionGetDimensions(formatDescription)
            let resolution = CGSize(width: CGFloat(dimensions.width), height: CGFloat(dimensions.height))
            OCR = OCRUtility(windowSize: self.view.bounds, ROI: self.windowRect!, resolution: resolution, numDigits: self.numDigits, highValue: self.highValue, lowValue: self.lowValue)
        } else {
            print("Camera Error")
            performSegue(withIdentifier: "exitSegue", sender: self)
        }
        
    }
    
    func startVideoFeed() {
        // set capture device to rear camera
        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],
                                                                mediaType: AVMediaType.video,
                                                                position: AVCaptureDevice.Position.back)
        let captureDevice = discoverySession.devices.first
        
        // configure video settings/devices
        let deviceInput = try! AVCaptureDeviceInput(device: captureDevice!)
        let deviceOutput = AVCaptureVideoDataOutput()
        deviceOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_32BGRA)]
        deviceOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: DispatchQoS.QoSClass.default))
        
        // initialize video session
        session.sessionPreset = AVCaptureSession.Preset.high
        session.addInput(deviceInput)
        session.addOutput(deviceOutput)
        
        // Link UI Image to camera feed
        let imageLayer = AVCaptureVideoPreviewLayer(session: session)
        imageLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        imageLayer.frame = imageView.bounds
        imageView.layer.addSublayer(imageLayer) // first index
        session.startRunning()
    }

    // add mask
    func addMaskOverlay(){
        // Calculate dimensions
        let width = imageView.bounds.width
        let height = imageView.bounds.height
        
        let cornerRadius: CGFloat = 10.0
        let windowWidth: CGFloat = 240.0
        let windowHeight: CGFloat = 60.0
        let windowX: CGFloat = width/2 - windowWidth/2
        let windowY: CGFloat = height/4 + windowHeight/2
        windowRect = CGRect(x: windowX, y: windowY, width: windowWidth, height: windowHeight)
        // linked back to global to provide ROI
        
        // setup drawing
        let fill = UIBezierPath(rect: imageView.bounds)
        let window = UIBezierPath(roundedRect: windowRect!, cornerRadius: cornerRadius)
        
        // draw
        let drawing = fill
        drawing.append(window)
        
        let mask = CAShapeLayer()
        mask.frame = imageView.bounds
        mask.fillRule = CAShapeLayerFillRule.evenOdd
        mask.fillColor = UIColor.black.cgColor
        mask.path = drawing.cgPath
        mask.strokeColor = UIColor.black.cgColor
        mask.lineWidth = 1.0
        mask.opacity = 0.75
        
        // add to screen
        imageView.layer.addSublayer(mask) // second index
    }
    
    
    
    @IBAction func handleTap(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "exitSegue", sender: self)
    }
}


//MARK: - CoreML & Vision Logic
extension OCRViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        OCR.startOCRRequest(sampleBuffer: sampleBuffer, window: self.view.bounds)
        
        // clear current highlights
        DispatchQueue.main.async() {
            self.imageView.layer.sublayers?.removeSubrange(2...)
        }
        
        // add new highlights if they exist
        if let textOutline = OCR.getTextOutline(){
            DispatchQueue.main.async {
                if let textOutline = textOutline.toCGRect(windowSize: self.imageView.bounds, padding: 20){
                    let outline = CALayer()
                    outline.frame = textOutline
                    outline.borderWidth = 2.0
                    outline.cornerRadius = 5
                    outline.borderColor = UIColor(red: 247/255.0, green: 207/255.0, blue: 76/255.0, alpha: 1.0).cgColor
                    //                     print(self.imageView.frame)
                    self.imageView.layer.addSublayer(outline)
                }
            }
            //update dots
        }
        
        // get OCR guess
        if let (guess, count) = OCR.getBestGuess() {
            print(String(guess) + ", " + String(count))
            // if the best guess is above lower threshold, display it
            if count > 1 {
                valRead = Int(guess)!
                DispatchQueue.main.async() {
                    self.detectNumberLabel.text = guess
                    // if best guess is above locked threshold, return
                    if (self.shouldPerformSegue) {
                        self.shouldPerformSegue = false
                        self.performSegue(withIdentifier: "exitSegue", sender: self)
                    }
                }
            }
        }

    }
}

