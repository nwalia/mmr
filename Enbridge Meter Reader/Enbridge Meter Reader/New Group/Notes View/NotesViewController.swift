//
//  CompletionCodeNotesViewController.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2017-12-01.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit

class NotesViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var charactersRemainingLabel: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    
    var note: String!
    var noteMandatory = false
    var maxCharacters = 132
    var specialInstructionsPassed: String? = nil
    var placeholderText: String = "" // had Insert note as its previous value.
    var image: UIImage?
    var didSubmitCode: Bool? = false
    var selectedCode: TroubleCode!
    
    var currentMeter: MeterRead?
    
    
    static var caller: UIViewController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notesTextView.keyboardDistanceFromTextField = -20
        
        notesTextView.delegate = self
        // If there are no special instructions passed, then show placeholder text
        if specialInstructionsPassed == nil {
            notesTextView.text = placeholderText
            notesTextView.textColor = UIColor.lightGray
            charactersRemainingLabel.text = String(maxCharacters)
            
        } else {
            notesTextView.text = specialInstructionsPassed
            let charactersRemaining = maxCharacters - notesTextView.text.count
            charactersRemainingLabel.text = String(charactersRemaining)
        }
        charactersRemainingLabel.textColor = UIColor(red:0.2, green:0.2, blue:0.2, alpha:1)
        
        if self.title == "Special Instructions" {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(submitNote))
            
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(submitNote))
        }
        
        navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : GlobalConstants.YELLOW], for: .normal)
        
        // If the text view contains the placeholder, then no text has been entered.
        // If the note is mandatory and no text has been entered, disable submit button.
        if noteMandatory && notesTextView.text == placeholderText {
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
        
    }
    
    // Uncomment if you want the text field automatically selected.
    //    override func viewDidAppear(_ animated: Bool) {
    //        notesTextView.becomeFirstResponder()
    //    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (textView.textColor == UIColor.lightGray){
            
            if specialInstructionsPassed == nil {
                textView.text = ""
            } else {
                textView.text = specialInstructionsPassed
            }
            textView.textColor = UIColor.black
        }
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            self.setBottomConstraintMultiplier(1.5)
        })
        
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            self.setBottomConstraintMultiplier()
        })
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= maxCharacters
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let charactersRemaining = maxCharacters - textView.text.count
        charactersRemainingLabel.text = String(charactersRemaining)
        
        if textView.text.count == 0 && noteMandatory {
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
        else {
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text.isEmpty){
            if specialInstructionsPassed == nil {
                textView.text = placeholderText
            } else {
                textView.text = specialInstructionsPassed
            }
            textView.textColor = UIColor.lightGray
        }
    }
    
    // Called when user clicks Submit button
    @objc func submitNote(){
        
        // Boolean used to select or unselect trouble code from table
        didSubmitCode = true
        
        if self.title == "Special Instructions" {
            currentMeter?.specialInstructions = notesTextView.text ?? ""
        }
        
        if let _ = NotesViewController.caller as? SelectCompletionCodeViewController {
            Utilities.currentMeter.mmrActivityText = notesTextView.text ?? ""
            Utilities.showAlertOn(self, segueName: "unwindToHome")
        }
        else {
            self.performSegue(withIdentifier: "unwindToHome", sender: self)
            NotesViewController.caller = nil
        }
        
    }
    
    func configureKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(aNotification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(aNotification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setBottomConstraintMultiplier(_ multiplier: CGFloat? = 1.0){
        
        let newConstraint = self.bottomConstraint.constraintWithMultiplier(multiplier!)
        self.view!.removeConstraint(bottomConstraint)
        self.bottomConstraint = newConstraint
        self.view!.addConstraint(self.bottomConstraint)
        self.view!.layoutIfNeeded()
    }
    
    @objc func keyboardWasShown(aNotification:NSNotification) {
        let info = aNotification.userInfo
        let infoNSValue = info![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue
        let kbSize = infoNSValue.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)
        notesTextView.contentInset = contentInsets
        notesTextView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillBeHidden(aNotification:NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        notesTextView.contentInset = contentInsets
        notesTextView.scrollIndicatorInsets = contentInsets
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let newNote = notesTextView.text {
            note = newNote != placeholderText ? newNote : ""
        }
        
        if segue.destination is SelectTroubleCodeViewController {
            let destinationVC = segue.destination as? SelectTroubleCodeViewController
            destinationVC?.didSubmitCode = didSubmitCode
            // Send trouble message 1 or 2
        }
        
    }
    
    
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}


