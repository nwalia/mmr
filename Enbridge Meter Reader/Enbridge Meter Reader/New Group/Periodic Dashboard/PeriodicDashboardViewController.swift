//
//  PeriodicDashboardViewController.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-25.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import AudioToolbox

// MARK: - Protocols
protocol PeriodicDashboardDelegate: class {
    func populateCustomerInformation(onDashboard: Bool, hideLine: Bool, newRead: MeterRead, buttonsEnabled: Bool)
}

class PeriodicDashboardViewController: UIViewController,CLLocationManagerDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var progressBar: ProgressBarView!
    @IBOutlet weak var timeElapsedBar: TimeElapsedView!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var scrollButton: UIButton!
    @IBOutlet weak var enterMeterInformation: UIButton!
    @IBOutlet weak var containerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var meterReadTableView: UITableView!
    @IBOutlet weak var customerInformation: CustomerInformation!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var timeElapsedHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var troubleCodeButton: UIButton!
    @IBOutlet weak var searchBarContainerConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderDetailCellTableViewHeight: NSLayoutConstraint!
    
    
    // MARK: - Vars
    var currentMeterRead: MeterRead!
    var firstTroubleCode: TroubleCode?
    var secondTroubleCode: TroubleCode?
    static var delegate: PeriodicDashboardDelegate?
    static var currentMeterLocation: LocationCode?
    var bottomContainerViewIsExpanded = true
    var toggleEdit = false
    var hasEnteredTroubleCodes: Bool = false
    
    var firstTroubleCodeNumber: String?
    var secondTroubleCodeNumber: String?

    let invisibleTextField = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var filterPicker: UIPickerView!
    var pickerOptions: [String] = []
    var filter: ((MeterRead) -> Bool) = {(MeterRead) -> Bool in return true}
    var filterAssigned = false
    
    var filteredMeterReads = [MeterRead]()
    var selectedRead: MeterRead? {
        willSet(newValue) {
            PeriodicDashboardViewController.currentMeterLocation = newValue?.locationCode
        }
    }
    
    let regionRadius: CLLocationDistance = 100
    let locationManager = CLLocationManager()
    let mapManager = MapViewManager()
    
    internal var editButtonToggle : Bool = false
    
    // MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.timeElapsedBar.isHidden = Model.selectedRouteKey.contains(RouteType.OUTCARD.rawValue) || Model.selectedRouteKey == RouteType.OFFCYCLE.rawValue

        meterReadTableView.delegate = self
        meterReadTableView.dataSource = self
        searchBar.delegate = self
        
        // --- Code for white background search bar for iOS 13 --- //
        if #available(iOS 13.0, *) {
           searchBar.searchTextField.backgroundColor = UIColor.white
        }
        
        self.mapView.delegate = self

        progressBar.populate(routeKey: Model.selectedRouteKey, type: .periodic)
        timeElapsedBar.populate(routeId: Model.selectedRouteKey)

        containerTopConstraint.constant = mapView.bounds.height * -1
        //view.layoutIfNeeded()
        scrollButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        
        // Let LocationCodeViewController know that the previous screen was this one
        ChangeMeterLocationViewController.previous = .periodicOutcard
        
        
        // Invisible text field, to facilitate popping up the UIPickerView
        invisibleTextField.isHidden = true
        invisibleTextField.inputAssistantItem.leadingBarButtonGroups.removeAll()
        invisibleTextField.inputAssistantItem.trailingBarButtonGroups.removeAll()
        self.view.addSubview(invisibleTextField)
        
        // Update user location in map view
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        // assign filter options based on whether the route is periodic or outcard
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .PERIODIC {
            pickerOptions = Array(FilterOptions.periodic.keys).sorted()
        }
        else {
            pickerOptions = Array(FilterOptions.outcard.keys).sorted()

        }
        
        
    }
    
    
    
    @objc func popToRootViewcontroller(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.timeElapsedBar.isHidden ? (self.containerTopConstraint.constant = -507.0) : (self.containerTopConstraint.constant = -457.0)
        
        meterReadTableView.reloadData()
        
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.completionTime != nil {
            timeElapsedBar.stopTimer()
        }
        
        
        if selectedRead == nil || selectedRead?.status == .completed /*|| (selectedRead?.status == .pending)*/{
            selectFirstNonComplete()
        }
        
        // adding New Service button to top bar if current route is periodic
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .PERIODIC {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "New Service", style: .plain, target: self, action: #selector(newServiceButtonPressed))
            progressBar.populate(routeKey: Model.selectedRouteKey, type: .periodic)
        }
        else {
            self.navigationItem.rightBarButtonItem = nil
            progressBar.populate(routeKey: Model.selectedRouteKey, type: .outcard)
        }
        
        self.setupMapView()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !Networking.isConnectedToNetwork() && AppDelegate.counter == 1 {
            
            AudioServicesPlaySystemSound(1315);
            let alert = UIAlertController(title: "Device Offline", message: "Map not available.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            AppDelegate.counter = 0
        }
        
        Persistence.shared.saveRoutes()
    }
    
    
    
    func setupMapView() {
        mapView.removeOverlays(mapView.overlays)
        mapView.removeAnnotations(mapView.annotations)
        
        //Set the source and destinatin for the route
       
        guard let deviceLatitude = locationManager.location?.coordinate.latitude else { return }
        guard let deviceLongitude = locationManager.location?.coordinate.longitude else { return }
        guard selectedRead != nil else { return }
        
        let source = Location.source(latitude: deviceLatitude, longitude: deviceLongitude)
        let destination = Location.destination(latitude: selectedRead!.latitude ?? deviceLatitude, longitude: selectedRead!.longitude ?? deviceLongitude)
        
        // Generate marker to plot on the mapView
        let mapItem = mapManager.generateMarkerFor(source: source, destination: destination)
        
        let sourceMapItem = mapItem.0
        let destinationMapItem = mapItem.1
        
        //get directions from source to destination
        mapManager.addRouteToMap(sourceMapItem: sourceMapItem, destinationMapItem: destinationMapItem, mapView: self.mapView)
        
        //set annoataion title and display on the mapView.
        let annotation = mapManager.setAnnotation(sourceTitle: "Current Location", destinationTitle: "Nathan Phillips Square")
        
        self.mapView.showAnnotations([annotation.sourceAnnotation,annotation.destinationAnnotation], animated: true )
        
        //create custom view for source and destination, navigate to the Apple Maps apps from here.
        //let artworkSource = Artwork(title: "Current Location" , coordinate: CLLocationCoordinate2D(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!))
        
        let artworkDestination = Artwork(title: (selectedRead!.nameLastFirstComma != "" ? selectedRead!.nameLastFirstComma : "Selected Customer"), subtitle: selectedRead!.fullAddress, coordinate: CLLocationCoordinate2D(latitude: selectedRead!.latitude ?? deviceLatitude, longitude: selectedRead!.longitude ?? deviceLongitude))
        
        mapView.addAnnotations([artworkDestination])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func scrollButtonClicked(_ sender: UIButton) {
        switch bottomContainerViewIsExpanded {
        case true:
            
            searchBar.resignFirstResponder()
            
            // animate downward
            UIView.animate(withDuration: 0.5, animations: { [weak self] in
                self?.containerTopConstraint.constant = 0
                self?.bottomContainerViewIsExpanded = false
                self?.view.layoutIfNeeded()
                }, completion: nil)
            sender.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            break
            
        case false:
            // animate upward
            UIView.animate(withDuration: 0.5, animations: { [weak self] in
                (self?.timeElapsedBar.isHidden)! ? (self?.containerTopConstraint.constant = ((self?.mapView.bounds.height)! + (self?.timeElapsedBar.frame.height)!) * -1) :(self?.containerTopConstraint.constant = (self?.mapView.bounds.height)! * -1)
                self?.bottomContainerViewIsExpanded = true
                self?.view.layoutIfNeeded()
                }, completion: nil)
            sender.imageView?.transform = CGAffineTransform(rotationAngle: 0)
        }
    }
    
    // scrolls the tableView to the first item that isn't completed.
    func selectFirstNonComplete(){
        if meterReadTableView.numberOfSections > 0 && meterReadTableView.numberOfRows(inSection: 0) > 0 {
            let meterReads = isFiltering() ? filteredMeterReads : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads
            var firstNonCompleteIndex = 0
            for (currentIndex,currentItem) in (meterReads?.enumerated())! {
                if currentItem.status != .completed {
                    firstNonCompleteIndex = currentIndex
                    break
                }
            }
        
            let path = IndexPath(row: firstNonCompleteIndex, section: 0)
            if firstNonCompleteIndex < meterReadTableView.numberOfRows(inSection: 0) {
                meterReadTableView.scrollToRow(at: path, at: .top, animated: true)
                selectItemInModel(firstNonCompleteIndex)
            }
        }
        
    }
    
    @objc
    func newServiceButtonPressed(){
        NewMeter_SAP_NewService.currentMRU = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads.first?.routeId
        let vc = UIStoryboard(name: "NewMeter", bundle: nil).instantiateInitialViewController()
        self.show(vc!, sender: self)
    }
    
    @IBAction func reconfigureButtonClicked(_ sender: UIButton) {
        Model.sharedInstance.routes[Model.selectedRouteKey]?.resequencedList = (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads)!
        !toggleEdit ? enableDragDrop(sender): disableDragDrop(sender)
        
        Persistence.shared.saveRoutes()
        toggleEdit ? (self.orderDetailCellTableViewHeight.constant = 800.0) : (self.orderDetailCellTableViewHeight.constant = 432.0)
        self.changeHeight(toggleEdit)
    }
    
    func changeHeight(_ toggle:Bool) {
        switch toggle{
        case true:
            UIView.animate(withDuration: 0.5, animations: {
                self.customerInformation.isHidden = true
                self.enterMeterInformation.isHidden = true
                self.searchBarContainerConstraint.constant = -325.0
                self.bottomContainerView.layoutSubviews()
            }, completion: nil)
        default:
            UIView.animate(withDuration: 0.5, animations: {
                self.customerInformation.isHidden = false
                self.enterMeterInformation.isHidden = false
                self.searchBarContainerConstraint.constant = 30.0
                self.bottomContainerView.layoutSubviews()
            }, completion: nil)
        }
    }
    
    @IBAction func filterButtonPressed(_ sender: UIButton) {
        // TODO: Refactor this and the Offcycle dashboard filterButtonPressed() into a xib.
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 45))
        toolbar.barStyle = .default
        
        var items = [UIBarButtonItem]()
        
        let backButton = UIBarButtonItem(title: "<", style: .plain, target: self, action: #selector(backToolbarButton))
        let forwardButton = UIBarButtonItem(title: ">", style: .plain, target: self, action: #selector(forwardToolbarButton))
        
        if let font = UIFont(name: "Arial", size: 24) {
            backButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
            forwardButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
            backButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .highlighted)
            forwardButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .highlighted)
        }
        
        
        items.append(backButton)
        items.append(forwardButton)
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil) )
        items.append(UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hidePickerView)) )
        
        toolbar.items = items
        
        filterPicker = UIPickerView(frame: CGRect(x:0, y:0, width: UIScreen.main.bounds.width, height: 220))
        
        filterPicker.delegate = self
        filterPicker.dataSource = self
        filterPicker.backgroundColor = .white
        invisibleTextField.inputView = filterPicker
        invisibleTextField.inputAccessoryView = toolbar
        
        invisibleTextField.becomeFirstResponder()
    }
    
    @objc func backToolbarButton(){
        let selectedRow = filterPicker.selectedRow(inComponent: 0)
        
        if selectedRow > 0 {
            filterPicker.selectRow(selectedRow - 1, inComponent: 0, animated: true)
        }
    }
    
    @objc func forwardToolbarButton(){
        let selectedRow = filterPicker.selectedRow(inComponent: 0)
        
        if selectedRow < filterPicker.numberOfRows(inComponent: 0) - 1 {
            filterPicker.selectRow(selectedRow + 1, inComponent: 0, animated: true)            
        }
    }
    
    @objc func hidePickerView(){
        invisibleTextField.resignFirstResponder()
        meterReadTableView.scrollToFirstRow()
        
        let selectedRow = filterPicker.selectedRow(inComponent: 0)
        let possibleFilters = Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .PERIODIC ? FilterOptions.periodic : FilterOptions.outcard
        if possibleFilters[pickerOptions[selectedRow]] != nil {
            filter = possibleFilters[pickerOptions[selectedRow]]!
        }
        else {
            filter = {(MeterRead) -> Bool in return true}
        }
        
        filterAssigned = pickerOptions[selectedRow] != "All Meters"
        if filterAssigned {
            filterButton.setTitle("Filtering", for: .normal)
            filterButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
        }
        else {
            filterButton.setTitle("Filter", for: .normal)
            filterButton.setTitleColor(.white, for: .normal)
        }
        
        filterContentForSearchText(searchBar.text!)
    }
    
    func selectItemInModel(_ index: Int){
        let meterReads = isFiltering() ? filteredMeterReads : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads
        guard meterReads != nil && index < (meterReads?.count ?? -1) else { return }
        
        selectedRead = meterReads![index]
        PeriodicDashboardViewController.delegate?.populateCustomerInformation(onDashboard: true, hideLine: true, newRead: selectedRead!, buttonsEnabled: true)
    }
    
    func enableDragDrop(_ sender:UIButton) {
        searchBar.isUserInteractionEnabled = false
        filterButton.isEnabled = false
        filterButton.borderColor = .lightGray
        searchBar.alpha = 0.65
        self.meterReadTableView.dragDelegate = self
        self.meterReadTableView.dropDelegate = self
        self.meterReadTableView.dragInteractionEnabled = true
        sender.setTitle("Done", for: .normal)
        self.toggleEdit = true
    }
    
    func disableDragDrop(_ sender:UIButton) {
        searchBar.isUserInteractionEnabled = true
        filterButton.isEnabled = true
        filterButton.borderColor = .white
        searchBar.alpha = 1
        
        self.meterReadTableView.dragDelegate = nil
        self.meterReadTableView.dropDelegate = nil
        self.meterReadTableView.dragInteractionEnabled = false
        sender.setTitle("Edit", for: .normal)
        self.toggleEdit = false
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? PeriodicOutcardScreenViewController {
            destinationVC.currentMeterRead = selectedRead
            destinationVC.previousDelegate = PeriodicDashboardViewController.delegate
            
            if isFiltering() {
                destinationVC.filteredMeterReads = filteredMeterReads
            }
        }
//        else if let sourceVC = segue.destination as? SelectTroubleCodeViewController {
//            
//            troubleCodeButton.setTitle((sourceVC.formattedCodeString ?? "Trouble Code"), for: UIControlState.normal)
//            
//            if sourceVC.returnedCodes?.count != 0 {
//                currentMeterRead.troubleCodes = sourceVC.returnedCodes!
//                currentMeterRead.updateReadDate()
//                hasEnteredTroubleCodes = true
//                troubleCodeButton.setTitleColor(GlobalConstants.YELLOW, for: UIControlState.normal)
//                if sourceVC.returnedCodes?.count == 1 {
//                    firstTroubleCode = sourceVC.returnedCodes?[0]
//                    convertTroubleCodes()
//                    secondTroubleCode = nil
//                }
//                else if sourceVC.returnedCodes?.count == 2 {
//                    firstTroubleCode = sourceVC.returnedCodes?[0]
//                    secondTroubleCode = sourceVC.returnedCodes?[1]
//                    convertTroubleCodes()
//                }
//            } else {
//                currentMeterRead.troubleCodes = [TroubleCode]()
//                hasEnteredTroubleCodes = false
//                troubleCodeButton.setTitleColor(.white, for: UIControlState.normal)
//                firstTroubleCode = nil
//                secondTroubleCode = nil
//                currentMeterRead.updateReadDate()
//            }
//        }
//    }
//    
//    func convertTroubleCodes () {
//        if ((firstTroubleCode?.number)!) < 10 && ((secondTroubleCode?.number)! > 10) {
//            firstTroubleCodeNumber = "TC" + "0" + "\((firstTroubleCode?.number)!)"
//            secondTroubleCodeNumber = "TC" + "\((secondTroubleCode?.number)!)"
//        }
//        else if ((firstTroubleCode?.number)! < 10) && ((secondTroubleCode?.number)! < 10) {
//            firstTroubleCodeNumber = "TC" + "0" + "\((firstTroubleCode?.number)!)"
//            secondTroubleCodeNumber = "TC" + "0" + "\((secondTroubleCode?.number)!)"
//        }
//        else if ((firstTroubleCode?.number)! > 10) && ((secondTroubleCode?.number)! < 10) {
//            firstTroubleCodeNumber = "TC" + "\((firstTroubleCode?.number)!)"
//            secondTroubleCodeNumber = "TC" + "0" + "\((secondTroubleCode?.number)!)"
//        } else {
//            firstTroubleCodeNumber = "TC" + "\((firstTroubleCode?.number)!)"
//            secondTroubleCodeNumber = "TC" + "\((secondTroubleCode?.number)!)"
//        }
    }
 
}

extension PeriodicDashboardViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        //print("render periodic")
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? Artwork else {return nil}
        
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Artwork
        let launchOptions = [MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
}
