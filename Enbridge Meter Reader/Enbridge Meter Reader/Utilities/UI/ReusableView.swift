//
//  ReusableView.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-03.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation

import UIKit

@IBDesignable
class ReusableView: UIView {
    
    var contentView: UIView?
    @IBInspectable var nib_Name: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        contentView?.prepareForInterfaceBuilder()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else {return}
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
        guard let nibName = self.nib_Name else {return nil}
        let bundle = Bundle(for: type(of: self))
        //print(bundle)
        let nib = UINib(nibName: nibName, bundle: bundle)
        //print(nib)
        
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
}
