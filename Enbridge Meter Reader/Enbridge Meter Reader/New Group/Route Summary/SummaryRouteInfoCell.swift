//
//  SummaryRouteInfoCell.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-03-16.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

class SummaryRouteInfoCell: UITableViewCell {
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var MRULabel: UITextView!
    @IBOutlet weak var insideLabel: UILabel!
    @IBOutlet weak var insideSkipsLabel: UILabel!
    @IBOutlet weak var insideMissesLabel: UILabel!
    
    @IBOutlet weak var locationDescriptionLabel: UILabel!
    @IBOutlet weak var outsideLabel: UILabel!
    @IBOutlet weak var outsideSkipsLabel: UILabel!
    @IBOutlet weak var outsideMissesLabel: UILabel!
    
    
    func populate(){
        
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.type == .PERIODIC {
            
            // Periodic meter
            
            NSLayoutConstraint(item: MRULabel as Any, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 37).isActive = true
            MRULabel.isScrollEnabled = false
            MRULabel.text = "MRU: \(Model.selectedRouteKey)"
            
            // TODO: Change location description label
            
            var insideMisses = 0
            var outsideMisses = 0
            var insideSkips = 0
            var outsideSkips = 0
            
            for read in (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads)! {
                if read.locationCode.isOutside {
                    if read.skipCode != nil {
                        outsideSkips += 1
                    }
                    if !read.completed && read.skipCode == nil {
                        outsideMisses += 1
                    }
                }
                else {
                    if read.skipCode != nil {
                        insideSkips += 1
                    }
                    if !read.completed && read.skipCode == nil {
                        insideMisses += 1
                    }
                }
            }
            
            insideLabel.text = "Inside:"
            outsideLabel.text = "Outside:"
            insideSkipsLabel.text = "\(insideSkips) Skips"
            insideMissesLabel.text = "\(insideMisses) Misses"
            outsideSkipsLabel.text = "\(outsideSkips) Skips"
            outsideMissesLabel.text = "\(outsideMisses) Misses"
            
        } else {
            
            // Outcard meter
            
            // Create string with all outcard MRU's
            var MRUListWithCommas = ""
            var uniqueMRUList = [""]
            for read in (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads)! {
                let newMRU = read.routeId
                
                for _ in uniqueMRUList {
                    if !uniqueMRUList.contains(newMRU) {
                        uniqueMRUList.append(newMRU)
                        MRUListWithCommas += "\(newMRU), "
                    }
                }
        
            }
            // Remove last comma and space
            let MRUList = MRUListWithCommas.dropLast(2)

            NSLayoutConstraint(item: MRULabel as Any, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 75).isActive = true
            MRULabel.isScrollEnabled = true
            MRULabel.text = "MRU: \(MRUList)"

            // TODO: Change location description label
            
            // Variables for three or four + consequtive estimates (skips)
            var threeUnread = 0
            var threeRead = 0
            var fourUnread = 0
            var fourRead = 0
            
            for read in (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads)! {
                if read.skips != nil {
                    if read.skips == 3 {
                        if read.completed {
                            threeRead += 1
                        } else {
                            threeUnread += 1
                        }
                    }
                    else if read.skips! > 3 {
                        if read.completed {
                            fourRead += 1
                        } else {
                            fourUnread += 1
                        }
                    }
                }
            }
            
            insideLabel.text = "Estimates of 3:"
            outsideLabel.text = "Estimates of 4+:"
            insideSkipsLabel.text = "\(threeRead) Read"
            insideMissesLabel.text = "\(threeUnread) Unread"
            outsideSkipsLabel.text = "\(fourRead) Read"
            outsideMissesLabel.text = "\(fourUnread) Unread"
 
        }

        locationLabel.text = "\(Model.sharedInstance.routes[Model.selectedRouteKey]?.region ?? "")"
        
    }
    
}
