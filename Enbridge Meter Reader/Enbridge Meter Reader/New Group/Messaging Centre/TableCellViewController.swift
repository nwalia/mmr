//
//  TableCellViewController.swift
//  Enbridge Meter Reader
//
//  Created by Abramsky, Lauren (CA - Toronto) on 2018-01-11.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit

class TableCellViewController: UITableViewCell {
 
    @IBOutlet weak var notificationCircle: UIView!
    @IBOutlet weak var messageFrom: UILabel!
    @IBOutlet weak var messageSubject: UILabel!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var messageBody: UILabel!
    var message: Message?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Set radius and margins
        notificationCircle.layer.cornerRadius = 6
        self.layoutMargins = UIEdgeInsets(top: 10, left: 30, bottom: 10, right: 10)
    }
    
    func populateCell() {
        messageFrom.text = message?.from ?? "No Sender"
        messageSubject.text = message?.subject ?? "No Subject"
        messageTime.text = message?.time ?? "No Time"
        messageBody.text = message?.body ?? "Empty Message"
    }
        
}
