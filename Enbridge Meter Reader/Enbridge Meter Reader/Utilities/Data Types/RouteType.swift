//
//  RouteType.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-03-13.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation

enum RouteType: String, Codable {
    case OFFCYCLE
    case PERIODIC
    case OUTCARD
}
