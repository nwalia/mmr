//
//  SelectTroubleCodeTableViewCell.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2017-12-07.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit

class SelectTroubleCodeTableViewCell: UITableViewCell {

    @IBOutlet weak var iconContainer1: UIView!
    @IBOutlet weak var iconContainer2: UIView!
    @IBOutlet weak var iconImageContainer1: UIView!
    @IBOutlet weak var iconImageContainer2: UIView!
    @IBOutlet weak var iconLabel1: UILabel!
    @IBOutlet weak var iconLabel2: UILabel!
    @IBOutlet weak var codeNumberLabel: UILabel!
    @IBOutlet weak var codeDescriptionLabel: UILabel!
    @IBOutlet weak var iconImage1: UIImageView!
    @IBOutlet weak var iconImage2: UIImageView!
    
    // will either be white or GlobalConstants.SELECTED_CELL based on whether that code has been selected.
    var defaultColor = UIColor.white
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        iconImageContainer1.backgroundColor = .black
        iconImageContainer1.layer.cornerRadius = 5
        
        iconImageContainer2.backgroundColor = .black
        iconImageContainer2.layer.cornerRadius = 5
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        iconImageContainer1.backgroundColor = .black
        iconImageContainer2.backgroundColor = .black
        
        // Set color to yellow if code has been selected
        let selectedBackground = UIView()
        selectedBackground.frame = self.frame
        selectedBackground.backgroundColor = GlobalConstants.SELECTED_CELL
        self.selectedBackgroundView = selectedBackground

    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        iconImageContainer1.backgroundColor = .black
        iconImageContainer2.backgroundColor = .black
        
        // Set color to yellow if code has been selected
        let selectedBackground = UIView()
        selectedBackground.frame = self.frame
        selectedBackground.backgroundColor = GlobalConstants.SELECTED_CELL
        self.selectedBackgroundView = selectedBackground

    }

}
