//
//  Message.swift
//  Enbridge Meter Reader
//
//  Created by Abramsky, Lauren (CA - Toronto) on 2018-01-15.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

class Message {
    let from: String
    let subject: String
    let date: String
    let time: String
    var body: String
    var opened: Bool
    
    init(from: String, subject: String, date: String, time: String, body: String, opened: Bool){
        self.from = from
        self.subject = subject
        self.date = date
        self.time = time
        self.body = body
        self.opened = false
    }
}
