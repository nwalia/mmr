//
//  MeterRead.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-09.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import SAPOData
import proxyclasses

enum MeterReadStatus: String, Codable {
    case available // has not been started (default value, matters in offcycle routes)
    case inProgress // has been started (offcycle)
    case missed // missed (and uploaded in the case of outcard and periodic)
    case pending // completed, but no network connection (offcycle)
    case completed // completed (and uploaded in the case of offcycle)
}

enum MeterReadNetworkStatus : String, Codable {
    case SCHD = "SCHD"
    case DISP = "DISP"
    case ACK = "ACK"
    case EN_Route = "EN-ROUTE"
    case ON_SITE = "ON-SITE"
    case MISSED = "MISSED"
    case FIELD_COMPLETE = "FIELD COMPLETE"
    case CANCEL = "CANCEL"
}

class MeterRead: Equatable, Codable {
    
    static func ==(lhs: MeterRead, rhs: MeterRead) -> Bool {
        return lhs.jobID == rhs.jobID
    }
    
    static let shared = MeterRead()
    
    
    // Required
    var firstName: String
    var lastName: String
    var locationCode: LocationCode
    var addressLine1: String
    var addressLine2: String
    var meterNumber: String
    var jobID: String
    var numberID: Int
    var gridNumber:String
    var selected: Bool
    var phoneNumber: String
    var sizeCode: String
    var numberOfDials = Int()
    var cancelled: Bool // Only for dunning redlock
    var unscheduled: Bool
    var routeId: String
    var cycleNo: String
    var completed: Bool
    var pending: Bool
    
    var status: MeterReadStatus
    var networkStatus : MeterReadNetworkStatus
    
    var hasErrorPhoto: Bool
    
    // Optional
    var type: String? // Offcycle, Periodic, or Outcard
    var typeCode: String? // Typecode only for offcycle
    var mmrNotificationCode: String?
    var timeStart:  String?
    var timeEnd: String?
    var keyNumber: String?
    var hasKey: Bool?
    var specialInstructions: String?
    var skipCode: SkipCode? // Skip codes only for periodic
    var skips: Int? // Skips only for periodic
    var lowParameter: Int?
    var rawReadDate: Date? // Tracks last time object was modified - used to create mmrReadDate when uploading back to SAP
    var highParameter: Int?
    // Only if meter exchange
    var newMeterNumber: String?  // Only if meter exchange
    var newSizeCode: String?
    var auditReadCount: Int? // Audit only for periodic with high/low parameters
    var houseNumber: String?
    var unitNumber: String?
    var latitude: Double?
    var longitude: Double?
    var rawLatitude: Int?
    var rawLongitude: Int?
    var permServNote: String?
    var requiredDate: String?
    var preReading: Int?
    var postReading: Int?
    var completionNote: String?
    var newMeterNumberOfDials: Int?
    var postalCode: String?
    
    // Codes
    var completionCode: CompletionCode?
    var troubleCodes = [TroubleCode]()
    
    // Track changes (used for determining if work order is FIELD COMPLETE or MISSED)
    var originalLocation: LocationCode?
    var originalSpecialMessage: String?
    var didChangeLocation: Bool = false
    var didChangeSpecialMessage: Bool = false
    
    // Computed
    
    var firstTroubleCodeString: String {
        let temp = ""
        if (troubleCodes.count <= 2) && (troubleCodes.count > 0){
            if troubleCodes[0].number < 10 {
                return "TC" + "0" + "\(troubleCodes[0].number)"
            } else {
                return "TC" + "\(troubleCodes[0].number)"
            }
        }
        else {
            return temp
        }
    }
    
    var secondTroubleCodeString: String {
        let temp = ""
        if troubleCodes.count == 2 {
            if troubleCodes[1].number < 10 {
                return "TC" + "0" + "\(troubleCodes[1].number)"
            } else {
                return "TC" + "\(troubleCodes[1].number)"
            }
        }
        else {
            return temp
        }
    }
    
    var troubleCodeString: String {
        get {
            let temp = ""
            if firstTroubleCodeString == temp && secondTroubleCodeString == temp {
                return temp
            }
            else if secondTroubleCodeString == temp {
                return firstTroubleCodeString
            }
            else {
                return
                    """
                    \(firstTroubleCodeString)
                    \(secondTroubleCodeString)
                    """
            }
        }
    }
    
    var critical: Bool {
        get {
            return self.skips != nil && self.skips! >= 4
        }
    }
    
    var nameLastFirstComma: String {
        get {
            if lastName.count > 0 {
                return "\(lastName), \(firstName)"
            }
            else {
                return firstName
            }
        }
    }
    
    var fullAddress: String {
        get {
            var temp = ""
            if houseNumber != nil {
                temp += houseNumber!
            }
            if addressLine1.count > 0 {
                if addressLine1.first == " " {
                   temp = temp + addressLine1
                } else {
                   temp = temp + " " + addressLine1
                }
            }
            
            if unitNumber != nil && unitNumber!.count > 0 {
                temp = temp + " " + unitNumber!
            }
            
            return temp
        }
    }
    
    var fullMeterId: String {
        get {
            return self.sizeCode + self.meterNumber
        }
    }
    
    var newFullMeterId: String {
        get {
            if newSizeCode != nil && newSizeCode != "" && newMeterNumber != nil && newMeterNumber != "" {
                return self.newSizeCode! + self.newMeterNumber!
            } else {
                return ""
            }
        }
    }
    
    var reading: Int?
//    var reading: Int? {
//        get { return preReading ?? 0 }
//        set(newValue) { preReading = newValue }
//    }
    
    //mapping the Click object
    var troubleCodeMessage1 : String?
    var troubleCodeMessage2 : String?
    var appointmentStartTime : String?
    var appointmentEndTime : String?
    var city : String?
    var mmrActivityText : String?
    var mmrImageURL : String?
    var isOffcycle: Bool?
    var isPeriodic : Bool?
    var isOutCard : Bool?
    var newMeterRead : String?
    var mmrNewSequenceNum : String?
    var mmrNewSizeCode : String?
    var mmrReadCode : String?
    var mmrReadDate : String?
    var mmrReadMethod : String?
    var mmrReadReason : String?
    var mmrReadTime : String?
    var mmrResequence : Bool?
    var mmrRiserDist : String?
    var mmrRiserFromWall : String?
    var mmrRiserOnWall : String?
    var mmrSequenceNum : String?
    var mmrTotalMeters : String?
    var mmrTroubleCode1 : String?
    var mmrTroubleCode2 : String?
    var mmrWorkType : String?
    var number : Int?
    var region : String?
    var mmrStatus : String?
    var street : String?
    var wamsCommentsLong : String?
    var customerWorkPhone : String?
    var wamsJobPlan : String?
    var wamsPSN : String?
    var wamsTaskTypeSub : String?
    var mmrRedLockType: String?
    var assignmentStart : String?
    var assignmentFinish : String?

//    if let apptStartTime = meterRead.appointmentStartTime {
//        var startDate = apptStartTime
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        //let startDateNoT = startDate.remove(at: startDate.index(of: "T"))
//        let date = dateFormatter.date(from:startDate)
//        
//        do {
//            meterInfoOperationObject.object?.task?.appointmentStart = try LocalDateTime.from(utc: (date)!, in: TimeZone.current)
//        } catch {
//            //print("LocalDateTime Conversion Error: \(error)")
//        }
//    }
//    if let apptEndTime = meterRead.appointmentEndTime {
//        let endDate = apptEndTime
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        let date = dateFormatter.date(from:endDate)
//        
//        do {
//            meterInfoOperationObject.object?.task?.appointmentFinish = try LocalDateTime.from(utc: date!, in: TimeZone.current)
//        } catch {
//            //print("LocalDateTime Conversion Error: \(error)")
//        }
//    }
    
    // Appt time only for some offcycle
    var appointmentTime: String? {
        get {
            if let startTime = appointmentStartTime {
                
                let startDateFormatter = startTime
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let startDate = dateFormatter.date(from:startDateFormatter)
                
                if startDate != nil {
                    let calendar = Calendar.current
                    let hour = calendar.component(.hour, from: startDate!)
                    
                    if hour >= 9 && hour < 12 {
                        return "9AM-12PM"
                    } else if hour >= 12 && hour < 16 {
                        return "12PM-4PM"
                    } else if hour >= 16 && hour <= 21 {
                        return "4PM-9PM"
                    }
                }
           }
           return ""
        }
    }
    
    init(){
        self.firstName = ""
        self.lastName = ""
        self.locationCode = LocationCode()
        self.addressLine1 = ""
        self.addressLine2 = ""
        self.meterNumber = ""
        self.jobID = ""
        self.gridNumber = ""
        self.selected = false
        self.phoneNumber = ""
        self.sizeCode = ""
        self.numberOfDials = 6
        self.cancelled = false
        self.unscheduled = false
        self.routeId = ""
        self.cycleNo = ""
        self.completed = false
        self.pending = false
        self.status = .available
        self.networkStatus = .SCHD
        self.numberID = -1
        self.hasErrorPhoto = false
        self.cycleNo = ""
    }
    
    init(firstName: String, lastName: String, locationCode: LocationCode, meterNumber: String, typeCode: String? = nil, sizeCode: String, addressLine1: String, addressLine2: String, jobID: String, gridNumber: String, phoneNumber: String, keyNumber: String? = nil, skips: Int? = 0, appointmentTime: String? = nil, numberOfDials: Int, lowParameter: Int? = nil, highParameter: Int? = nil, newMeterNumber: String? = nil, newSizeCode: String? = nil, auditReadCount: Int? = nil) {
        
        self.firstName = firstName
        self.lastName = lastName
        self.locationCode = locationCode
        self.meterNumber = meterNumber
        self.typeCode = typeCode
        self.sizeCode = sizeCode
        self.addressLine1 = addressLine1
        self.addressLine2 = addressLine2
        self.selected = false
        self.gridNumber = gridNumber
        self.jobID = jobID
        self.phoneNumber = phoneNumber
        self.skips = skips
//        self.appointmentTime = appointmentTime
        self.numberOfDials = numberOfDials
        self.lowParameter = lowParameter
        self.highParameter = highParameter
        self.newMeterNumber = newMeterNumber
        self.newSizeCode = newSizeCode
        self.cancelled = false
        self.unscheduled = false
        self.auditReadCount = auditReadCount
        self.routeId = "NONE"
        self.cycleNo = ""
        self.completed = false
        self.pending = false
        self.numberID = -1
        self.hasErrorPhoto = false
        
        // Set type based on data given
        if typeCode != nil {
            self.type = "Offcycle"
        } else {
            self.type = "Periodic"
        }
        
        // Set key based on data given
        if keyNumber != nil {
            self.keyNumber = keyNumber
            self.hasKey = true
        } else {
            self.hasKey = false
        }
        
        self.status = .available
        self.networkStatus = .SCHD
        self.cycleNo = ""
    }
    
    func getTimeIconColor() -> UIColor {
        // stub method - gets the color for the "time" icon in the table view cell
        // TODO: conditionally return more colors based on time range
        return .black
    }
    
    func updateReadDate(){
        rawReadDate = Date()
//        //print("DEBUG: Updating date to \(String(describing: mmrReadDate))")
    }
    
    func checkForWorkOrderType(_ meterRead : MeterRead) -> String {
        switch true {
        case meterRead.isOffcycle:
            return "O"
        default:
            return "P"
        }
    }
    
    
}

extension MeterRead {
    static func mapTo_MeterRead(_ entity: M1Operation) -> MeterRead{
        let tempMeterRead = MeterRead()
        
        if let names = entity.object?.task?.customer?.components(separatedBy: ", "){
            if names.count == 2 {
                tempMeterRead.firstName = names[1]
                tempMeterRead.lastName = names[0]
            }
            else {
                tempMeterRead.firstName = entity.object?.task?.customer ?? ""
            }
        }
        tempMeterRead.jobID = entity.callID ?? ""
        tempMeterRead.numberID = entity.number ?? -1
        
        
        tempMeterRead.routeId = entity.object?.task?.mmrMru ?? ""
        tempMeterRead.cycleNo = entity.object?.task?.mmrCycleNumber ?? ""
        tempMeterRead.gridNumber = entity.object?.task?.wamsGrid ?? ""
        tempMeterRead.specialInstructions = entity.object?.task?.mmrDevLocNote ?? ""
        tempMeterRead.meterNumber = entity.object?.task?.mmrLocationMeterNum ?? ""
        tempMeterRead.locationCode = LocationCode((entity.object?.task?.serviceSummary ?? ""))
        tempMeterRead.originalLocation = LocationCode((entity.object?.task?.serviceSummary ?? ""))
        tempMeterRead.mmrNotificationCode = entity.object?.task?.mmrNotificationCode ?? ""
        
        let temp = Int(entity.object?.task?.mmrNumberOfDials ?? "0")
        if temp == 0 {
            tempMeterRead.numberOfDials = 6
        }
        else {
            if let dials = temp {
                tempMeterRead.numberOfDials = dials
            }else {
                tempMeterRead.numberOfDials = 6
            }
        }
        
        tempMeterRead.houseNumber = entity.object?.task?.mmrStreetAddrsNumber ?? ""
        tempMeterRead.unitNumber = entity.object?.task?.mmrStreetAddrsUnitNumber ?? ""
        tempMeterRead.addressLine1 = entity.object?.task?.mmrStreetAddrs ?? ""
        
        //Create address line 2 (City, Postal)
        let postcode = entity.object?.task?.postcode ?? ""
        let city = entity.object?.task?.city ?? ""
        if city == "" {
            tempMeterRead.addressLine2 = postcode
        } else {
            tempMeterRead.addressLine2 = "\(city), \(postcode)"
        }
        //tempMeterRead.addressLine2 = entity.object?.task?.postcode ?? ""
        tempMeterRead.phoneNumber = entity.object?.task?.wamsCustomerHomePhone ?? entity.object?.task?.wamsCustomerWorkPhone ?? ""
        //print("! tempMeterRead.phoneNumber: \(tempMeterRead.phoneNumber)")
        
        tempMeterRead.meterNumber = entity.object?.task?.mmrLocationMeterNum ?? ""
        tempMeterRead.region = entity.object?.task?.region ?? "" //entity.object?.task?.region ?? ""
        //print("! tempMeterRead.region: \(String(describing: tempMeterRead.region))")
        
        if let highParamAsInt = Int((entity.object?.task?.mmrHigh1 ?? "").replacingOccurrences(of: ",", with: "")){
            tempMeterRead.highParameter = highParamAsInt
        }
        if let lowParamAsInt = Int((entity.object?.task?.mmrLow1 ?? "").replacingOccurrences(of: ",", with: "")){
            tempMeterRead.lowParameter = lowParamAsInt
        }
        if let skipsAsInt = Int(entity.object?.task?.mmrNoConsecEstimates ?? ""){
            tempMeterRead.skips = skipsAsInt
        }
        
        if (entity.object?.task?.mmrIsOffCycle)! {
            tempMeterRead.type = "Offcycle"
            GlobalConstants.CurrentRoute = "OFFCYCLE"
            MeterReadManager.shared.off_cycle_CallId.append(tempMeterRead.jobID)
        }else if (entity.object?.task?.mmrIsPeriodic)!{
            tempMeterRead.type = "Periodic"
            GlobalConstants.CurrentRoute = "PERIODIC"
            MeterReadManager.shared.periodicData_CallId.append(tempMeterRead.jobID)
        }else if  (entity.object?.task?.mmrIsOutCard)! {
            tempMeterRead.type = "Outcard"
            GlobalConstants.CurrentRoute = "OUTCARD"
            MeterReadManager.shared.outcardData_CallId.append(tempMeterRead.jobID)
        }
        tempMeterRead.mmrStatus = entity.object?.task?.status
        tempMeterRead.typeCode = entity.object?.task?.mmrNotificationType ?? ""
        tempMeterRead.mmrNotificationCode = entity.object?.task?.mmrNotificationCode ?? ""
        /** Adding the RedLockType **/
        tempMeterRead.mmrRedLockType = entity.object?.task?.mmrRedLockType ?? ""

        
        if entity.object?.task?.latitude != nil {
            if let lat = entity.object?.task?.latitude {
                tempMeterRead.rawLatitude = lat
                let arrayOfNumbers = Array(String(lat))
                if arrayOfNumbers.count > 1 {
                    let arraySlice = arrayOfNumbers[0..<2]
                    let remainder = arrayOfNumbers[2..<arrayOfNumbers.count]
                    let formattedString =  String("\(String(arraySlice)).\(String(remainder))")
                    tempMeterRead.latitude = Double(formattedString)
                }else{
                    tempMeterRead.latitude = Double(String(lat))
                }
                
            }
        }
        if entity.object?.task?.longitude != nil {
            if let long = entity.object?.task?.longitude {
                tempMeterRead.rawLongitude = long
                if Array(String(long)).first == "-" {
                    let arrayOfNumbers = Array(String(long))
                    if arrayOfNumbers.count > 2 {
                        let signedArraySlice = arrayOfNumbers[0...2]
                        let remainderOfSlice = Array(String(long))[3..<arrayOfNumbers.count]
                        let formattedLongitudeString =  String("\(String(signedArraySlice)).\(String(remainderOfSlice))")
                        tempMeterRead.longitude = Double(formattedLongitudeString) ?? Double(long)
                    }else {
                        tempMeterRead.longitude = Double(long)
                    }
                }else {
                    let arrayOfNumbers = Array(String(long))
                    if arrayOfNumbers.count > 1 {
                        let unignedArraySlice = Array(String(long))[0..<2]
                        let remainderOfSlice = Array(String(long))[2..<arrayOfNumbers.count]
                        let formattedLongitudeString =  String("\(String(unignedArraySlice)).\(remainderOfSlice)")
                        tempMeterRead.longitude = Double(formattedLongitudeString) ?? Double(long)
                    }else {
                        tempMeterRead.longitude = Double(long)
                    }
                }
            }
        }
        
        if let date = entity.object?.task?.dueDate {
            tempMeterRead.requiredDate = date.toString()
        }
        //tempMeterRead.requiredDate = String(describing: entity.object?.task?.dueDate)
        //ask prady difference btwn special instructionsvs special message. both have the same value of mmrDevLocNote.
        //tempMeterRead.specialInstructions = entity.object?.task?.mmrDevLocNote ?? ""
        tempMeterRead.permServNote = entity.object?.task?.wamsPsn ?? ""
        tempMeterRead.sizeCode = entity.object?.task?.mmrLocationMeterSize ?? ""
        
        tempMeterRead.assignmentStart = String(describing: entity.object!.task!.start)
        tempMeterRead.assignmentFinish = String(describing: entity.object!.task!.finish)

        tempMeterRead.appointmentStartTime = String(describing: entity.object!.task!.appointmentStart!)
        tempMeterRead.appointmentEndTime = String(describing: entity.object!.task!.appointmentFinish!)
        tempMeterRead.city = entity.object?.task?.city
        tempMeterRead.mmrActivityText = entity.object?.task?.mmrActivityText
        tempMeterRead.auditReadCount = entity.object?.task?.mmrAudCnt
        if tempMeterRead.completionCode == nil {
            tempMeterRead.completionCode = CompletionCode(number: Int (entity.object?.task?.mmrCompletionCode ?? "0"), description: "", hasNoteIcon: false, type: "")
        }
        tempMeterRead.mmrImageURL = entity.object?.task?.mmrImageURL
        tempMeterRead.isOffcycle = entity.object?.task?.mmrIsOffCycle
        tempMeterRead.isOutCard = entity.object?.task?.mmrIsOutCard
        tempMeterRead.isPeriodic = entity.object?.task?.mmrIsPeriodic
        tempMeterRead.keyNumber = entity.object?.task?.mmrKeyNumber
        tempMeterRead.reading = Int(entity.object?.task?.mmrMeterRead ?? "0")
//        tempMeterRead.locationCode.number = entity.object?.task?.mmrNewLocCode ?? ""
        tempMeterRead.newMeterRead = entity.object?.task?.mmrNewMeterRead
        tempMeterRead.newMeterNumber = entity.object?.task?.mmrNewMeterNo
        tempMeterRead.numberOfDials = Int((entity.object?.task?.mmrNumberOfDials)!) ?? 4
        tempMeterRead.mmrNewSequenceNum = entity.object?.task?.mmrNewSequenceNo
        tempMeterRead.newSizeCode = entity.object?.task?.mmrNewSizeCode
        tempMeterRead.mmrReadCode = entity.object?.task?.mmrReadCode
        tempMeterRead.mmrReadDate = String(describing: entity.object?.task?.mmrReadDate)
        tempMeterRead.mmrReadMethod = entity.object?.task?.mmrReadMethod
        tempMeterRead.mmrReadReason = entity.object?.task?.mmrReadReason
        tempMeterRead.mmrReadTime = String(describing: entity.object?.task?.mmrReadTime)
        tempMeterRead.mmrResequence = entity.object?.task?.mmrResequence
        tempMeterRead.mmrRiserOnWall = entity.object?.task?.mmrRiserOnWall
        tempMeterRead.mmrRiserFromWall = entity.object?.task?.mmrRiserFromWall
        tempMeterRead.mmrRiserDist = entity.object?.task?.mmrRiserDist
        tempMeterRead.mmrSequenceNum = entity.object?.task?.mmrSequenceNum
        tempMeterRead.mmrTotalMeters = entity.object?.task?.mmrTotalMeters
        tempMeterRead.mmrTroubleCode1 = String(describing: entity.object?.task?.mmrTroubleCode1)
        tempMeterRead.mmrTroubleCode2 = String(describing: entity.object?.task?.mmrTroubleCode2)
        //tempMeterRead.skipCode = SkipCode(id: (entity.object?.task?.mmrReadReason) ?? "" , description: (entity.object?.task?.mmrReadReason) ?? "")
        tempMeterRead.troubleCodeMessage1 = entity.object?.task?.mmrTroubleMessage1
        tempMeterRead.troubleCodeMessage2 = entity.object?.task?.mmrTroubleMessage2
        tempMeterRead.mmrWorkType = entity.object?.task?.mmrWorkType
        tempMeterRead.number = entity.object?.task?.number
        tempMeterRead.region = entity.object?.task?.region
        //Need to check local persistance for status
        tempMeterRead.networkStatus = MeterReadNetworkStatus(rawValue: (entity.object?.task?.status)!)!
        //End assign status
        tempMeterRead.street = entity.object?.task?.street
        tempMeterRead.wamsCommentsLong = entity.object?.task?.wamsCommentsLong
        tempMeterRead.phoneNumber = entity.object?.task?.wamsCustomerHomePhone ?? ""
        tempMeterRead.customerWorkPhone = entity.object?.task?.wamsCustomerWorkPhone
        tempMeterRead.wamsJobPlan = entity.object?.task?.wamsJobPlan
        tempMeterRead.wamsPSN = entity.object?.task?.wamsPsn
        tempMeterRead.wamsTaskTypeSub = entity.object?.task?.wamsTaskTypeSub
        
        return tempMeterRead
    }
}

extension MeterRead {
    static func mapTo_Operation(meterRead:MeterRead, withCompletionCode : String = "") -> M1Operation {
        var meterInfoOperationObject = M1Operation()
        //meterInfoOperationObject.mmrEngineerID = Utilities.sharedInstance.userName!
        meterInfoOperationObject.callID = meterRead.jobID
        meterInfoOperationObject.action = "Update"
        meterInfoOperationObject.number = meterRead.numberID
        
        //Object/Task Structure in operation
        meterInfoOperationObject.object = M1Object()
        meterInfoOperationObject.object?.task = M1Task()
        
        // apptStartTime & apptEndTime
        meterInfoOperationObject = MeterRead.populateIconForStartEndTime(meterRead,operation: meterInfoOperationObject)

        meterInfoOperationObject.object?.task?.callID = meterRead.jobID
        meterInfoOperationObject.object?.task?.city = meterRead.city
        meterInfoOperationObject.object?.task?.customer = "\(meterRead.firstName) \(meterRead.lastName)"
        
        if let date = meterRead.requiredDate {
            let isoDate = date + "+0000"
            let dateFormatter = ISO8601DateFormatter()
            let date = dateFormatter.date(from:isoDate)!
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year, .month, .day, .hour], from: date)
            let finalDate = calendar.date(from:components)
            meterInfoOperationObject.object?.task?.dueDate = LocalDateTime.from(utc: finalDate ?? Date())
        }
       
        meterInfoOperationObject.object?.task?.longitude = meterRead.rawLongitude!
        meterInfoOperationObject.object?.task?.latitude = meterRead.rawLatitude!
        meterInfoOperationObject.object?.task?.mmrActivityText = meterRead.mmrActivityText ?? ""
        meterInfoOperationObject.object?.task?.mmrAudCnt = meterRead.auditReadCount
        meterInfoOperationObject.object?.task?.mmrCompletionCode = withCompletionCode
        meterInfoOperationObject.object?.task?.mmrDevLocNote = meterRead.specialInstructions
        if let high1 = meterRead.highParameter {
            meterInfoOperationObject.object?.task?.mmrHigh1 = String(high1)
        } else {
            meterInfoOperationObject.object?.task?.mmrHigh1 = ""
        }
        meterInfoOperationObject.object?.task?.mmrImageURL = meterRead.mmrImageURL
        meterInfoOperationObject.object?.task?.mmrIsOffCycle = meterRead.isOffcycle
        meterInfoOperationObject.object?.task?.mmrIsOutCard = meterRead.isOutCard
        meterInfoOperationObject.object?.task?.mmrIsPeriodic = meterRead.isPeriodic
        meterInfoOperationObject.object?.task?.mmrKeyNumber = meterRead.keyNumber
        meterInfoOperationObject.object?.task?.mmrLocationMeterNum = meterRead.meterNumber
        meterInfoOperationObject.object?.task?.mmrLocationMeterSize = meterRead.sizeCode
        if let low1 = meterRead.lowParameter {
            meterInfoOperationObject.object?.task?.mmrLow1 = String(low1)
        } else {
            meterInfoOperationObject.object?.task?.mmrLow1 = ""
        }
        meterInfoOperationObject.object?.task?.mmrMru = meterRead.routeId
        meterInfoOperationObject.object?.task?.mmrCycleNumber = meterRead.cycleNo
        if let reading = meterRead.reading {
            meterInfoOperationObject.object?.task?.mmrMeterRead = String(reading)
        } else {
            meterInfoOperationObject.object?.task?.mmrMeterRead = ""
        }
        
        meterInfoOperationObject.object?.task?.mmrNewLocCode = meterRead.didChangeLocation ? meterRead.locationCode.id : ""
        meterInfoOperationObject.object?.task?.mmrNewMeterNo = meterRead.newMeterNumber
        if let meterType = meterRead.typeCode {
            if meterType == "ZB" {
                if let postReading = meterRead.postReading {
                    meterInfoOperationObject.object?.task?.mmrNewMeterRead = String(postReading)
                }
            } else {
                meterInfoOperationObject.object?.task?.mmrNewMeterRead = String(describing:meterRead.newMeterRead!)
            }
        }
        meterInfoOperationObject.object?.task?.mmrNumberOfDials = String(describing: meterRead.numberOfDials)
        meterInfoOperationObject.object?.task?.mmrNewSequenceNo = meterRead.mmrNewSequenceNum
        meterInfoOperationObject.object?.task?.mmrNewSizeCode = meterRead.newSizeCode
        if let skips = meterRead.skips {
            meterInfoOperationObject.object?.task?.mmrNoConsecEstimates = String(skips)
        } else {
            meterInfoOperationObject.object?.task?.mmrNoConsecEstimates = ""
        }
        meterInfoOperationObject.object?.task?.mmrNotificationType = meterRead.typeCode
        meterInfoOperationObject.object?.task?.mmrNotificationCode = meterRead.mmrNotificationCode
        
            /** Adding the RedLockType **/
        meterInfoOperationObject.object?.task?.mmrRedLockType = meterRead.mmrRedLockType
        
        if let newDials = meterRead.newMeterNumberOfDials {
            meterInfoOperationObject.object?.task?.mmrNewNumOfDials = String(newDials)
        } else {
            meterInfoOperationObject.object?.task?.mmrNewNumOfDials = ""
        }
        // should read date & time only be set when status is field complete?
        meterInfoOperationObject.object?.task?.mmrReadDate = meterRead.rawReadDate != nil ? LocalDateTime.from(utc: meterRead.rawReadDate!, in: TimeZone.current) : LocalDateTime.now()
        meterInfoOperationObject.object?.task?.mmrReadMethod = meterRead.mmrReadMethod
        meterInfoOperationObject.object?.task?.mmrReadReason = meterRead.mmrReadReason
        meterInfoOperationObject.object?.task?.mmrReadTime = meterRead.rawReadDate != nil ? LocalDateTime.from(utc: meterRead.rawReadDate!, in: TimeZone.current) : LocalDateTime.now() //LocalDateTime.castOptional(StringValue.of(optional: meterRead.mmrReadTime))
        meterInfoOperationObject.object?.task?.mmrResequence = meterRead.mmrResequence
        meterInfoOperationObject.object?.task?.mmrRiserDist = meterRead.mmrRiserDist
        meterInfoOperationObject.object?.task?.mmrRiserFromWall = meterRead.mmrRiserFromWall
        meterInfoOperationObject.object?.task?.mmrRiserOnWall = meterRead.mmrRiserOnWall
        meterInfoOperationObject.object?.task?.mmrSequenceNum = meterRead.mmrSequenceNum
        meterInfoOperationObject.object?.task?.mmrStreetAddrs = meterRead.addressLine1
        meterInfoOperationObject.object?.task?.mmrStreetAddrsNumber = meterRead.houseNumber
        meterInfoOperationObject.object?.task?.mmrStreetAddrsUnitNumber = meterRead.unitNumber
        meterInfoOperationObject.object?.task?.mmrTotalMeters = meterRead.mmrTotalMeters
        if let number = meterRead.troubleCodes.first?.number {
            meterInfoOperationObject.object?.task?.mmrTroubleCode1 = String(number)
        }else {
            meterInfoOperationObject.object?.task?.mmrTroubleCode1 = ""
        }
        meterInfoOperationObject.object?.task?.mmrTroubleMessage1 = meterRead.troubleCodes.first?.note
        if meterRead.troubleCodes.count > 1 {
            meterInfoOperationObject.object?.task?.mmrTroubleCode2 = String(describing: meterRead.troubleCodes[1].number)
            meterInfoOperationObject.object?.task?.mmrTroubleMessage2 = meterRead.troubleCodes[1].note
        } else {
            meterInfoOperationObject.object?.task?.mmrTroubleCode2 = ""
            meterInfoOperationObject.object?.task?.mmrTroubleMessage2 = ""
        }
        meterInfoOperationObject.object?.task?.mmrReadReason = meterRead.skipCode?.id
        // N if meter read was entered. Should be S if skip code is entered. Should be blank if meter is missed
        if meterRead.skipCode != nil{
            meterInfoOperationObject.object?.task?.mmrReadCode = "S"
        }else if meterRead.reading != nil {
            meterInfoOperationObject.object?.task?.mmrReadCode = "N"
        }else{
            meterInfoOperationObject.object?.task?.mmrReadCode = ""
        }
        
        
        meterInfoOperationObject.object?.task?.mmrWorkType = meterRead.mmrWorkType
        meterInfoOperationObject.object?.task?.number = meterRead.number
        meterInfoOperationObject.object?.task?.postcode = meterRead.postalCode
        meterInfoOperationObject.object?.task?.region = meterRead.region
        meterInfoOperationObject.object?.task?.serviceSummary = meterRead.originalLocation?.id ?? ""
        meterInfoOperationObject.object?.task?.status = meterRead.mmrStatus
        meterInfoOperationObject.object?.task?.street = meterRead.street
        meterInfoOperationObject.object?.task?.wamsCommentsLong = meterRead.wamsCommentsLong
        meterInfoOperationObject.object?.task?.wamsCustomerHomePhone = meterRead.phoneNumber
        meterInfoOperationObject.object?.task?.wamsCustomerWorkPhone = meterRead.customerWorkPhone
        meterInfoOperationObject.object?.task?.wamsGrid = meterRead.gridNumber
        meterInfoOperationObject.object?.task?.wamsJobPlan = meterRead.wamsJobPlan
        meterInfoOperationObject.object?.task?.wamsPsn = meterRead.wamsPSN
        meterInfoOperationObject.object?.task?.wamsTaskTypeSub = meterRead.wamsTaskTypeSub
        
        return meterInfoOperationObject
    }
    
    private static func populateIconForStartEndTime (_ meterRead: MeterRead,operation: M1Operation) -> M1Operation {
        if let apptStartTime = meterRead.appointmentStartTime {
            let startDate = apptStartTime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            //let startDateNoT = startDate.remove(at: startDate.index(of: "T"))
            let date = dateFormatter.date(from:startDate)
            
            do {
                if let capturedDate = date {
                     operation.object?.task?.appointmentStart = LocalDateTime.from(utc: capturedDate, in: TimeZone.current)
                }
            }
        }
        if let apptEndTime = meterRead.appointmentEndTime {
            let endDate = apptEndTime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let date = dateFormatter.date(from:endDate)
            
            do {
                if let capturedDate = date {
                    operation.object?.task?.appointmentFinish = LocalDateTime.from(utc: capturedDate, in: TimeZone.current)
                }
            }
        }
        return operation
    }
}

class MeterReadManager {
    static let shared = MeterReadManager()
    var off_cycle_CallId = [MeterRead().jobID]
    var periodicData_CallId = [MeterRead().jobID]
    var outcardData_CallId = [MeterRead().jobID]
    
    var off_cycle_CallId_ACK = [MeterRead().jobID : Bool()]
    var periodicData_CallId_ACK = [MeterRead().jobID : Bool()]
    var outcardData_CallId_ACK = [MeterRead().jobID : Bool()]
    
    var off_cycle_CallId_EnRoute = [MeterRead().jobID : Bool()]
    
    var periodicData_RouteID = [String]()
    
    var curentOffCycleRouteStatus = "DISP"
    var curentOutCardRouteStatus = "DISP"
        
}
