//
//  NewMeter+SAP_NewService.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-03-07.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import Foundation
import SAPFoundation
import SAPOData
import SAPFiori
import SAPCommon
import SAPOfflineOData
import proxyclasses

class NewMeter_SAP_NewService: NewMeterViewController {
    static let sharedInstance = NewMeter_SAP_NewService()
    
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    //MARK: - XML Parsing Code
    
    func getToken(userId:String = "",password:String = "" ,url: String = "", viewController : UIViewController, meterNumber:String, meterSizeCode : String, numberOfDials : String, enterMeterNumber : String, addressText:String) {

        ///////////////////////////////////////////////
        // if you change this, DO NOT COMMIT!
        let userId = "<ENTER YOUR SID HERE>"
        let password = "<ENTER YOUR PASSWORD HERE>"
        ////////////////////////////////////////////////
        
        let url = NSURL(string: "\(newServiceURL)/gw/odata/SAP/POST_NEWSERVICE;v=1/$metadata")
        let request = NSMutableURLRequest(url: url! as URL)
        
        var token : String?
        
        let config = URLSessionConfiguration.default
        let userPasswordString = "\(userId):\(password)"
        let userPasswordData = userPasswordString.data(using: String.Encoding.utf8)! as NSData
        let base64EncodedCredential = userPasswordData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let authString = "Basic \(base64EncodedCredential)"
        config.httpAdditionalHeaders = ["Authorization" : authString, "x-csrf-token": "FETCH"]
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request as URLRequest) {[weak self] (data, response, error) -> Void in
            
            guard error == nil else {
                //print(error?.localizedDescription as String!)
                return
            }
            
            guard let _ = data else {
                return
            }
            
            DispatchQueue.main.async() {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    token = httpResponse.allHeaderFields["X-CSRF-Token"] as? String
                    //print(token as String!)
                    self?.sendNewService(token:token!,
                                         viewController: viewController,
                                         meterNumber:meterNumber,
                                         meterSizeCode : meterSizeCode,
                                         numberOfDials : numberOfDials,
                                         enterMeterNumber : enterMeterNumber,
                                         addressText:addressText)
                }
            }
        }
        task.resume()
        
    }
    
    func sendNewService(userId:String = "",password:String = "" ,metadata:String = "", token:String,  viewController: UIViewController, meterNumber : String, meterSizeCode : String, numberOfDials : String, enterMeterNumber : String, addressText:String) {
        

        ///////////////////////////////////////////////
        // if you change this, DO NOT COMMIT!
        let login = "<ENTER YOUR SID HERE>"
        let password = "<ENTER YOUR PASSWORD HERE>"
        ////////////////////////////////////////////////        

        let url = NSURL(string: "\(newServiceURL)/gw/odata/SAP/POST_NEWSERVICE;v=1/ZDM_NEW_SERVICE_COLL")
        let request = NSMutableURLRequest(url: url! as URL)
        let config = URLSessionConfiguration.default
        let userPasswordString = "\(login):\(password)"
        let userPasswordData = userPasswordString.data(using: String.Encoding.utf8)! as NSData
        let base64EncodedCredential = userPasswordData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let authString = "Basic \(base64EncodedCredential)"
        config.httpAdditionalHeaders = ["Authorization" : authString, "x-csrf-token" : token,"Content-Type" : "application/xml"]
        let session = URLSession(configuration: config)
        request.httpMethod = "POST"
//        request.setValue("application/xml", forHTTPHeaderField: "Content-Type")
        
        
        let postString = """
<entry xmlns="http://www.w3.org/2005/Atom" xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" xmlns:d="http://schemas.microsoft.com/ado/2007/08/dataservices" xml:base="http://sapmd1ci.egd.enbridge.com:8033/sap/opu/odata/sap/ZDM_NEW_SERVICE_SRV/">
 <content type="application/xml">
  <m:properties xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" xmlns:d="http://schemas.microsoft.com/ado/2007/08/dataservices">
        <d:Equnr>\(meterNumber)</d:Equnr>
        <d:Matnr>\(meterSizeCode)</d:Matnr>
   <d:Ableinh>010101ME</d:Ableinh>
        <d:Dials>\(numberOfDials)</d:Dials>
        <d:VZwstand>\(enterMeterNumber)</d:VZwstand>
        <d:Address>\(addressText)</d:Address>
  </m:properties>
 </content>
</entry>
"""
        
        let post: NSString = postString as NSString
        let postData:NSData = post.data(using: String.Encoding.utf8.rawValue)! as NSData
        request.httpBody = postData as Data
        
        //print(request.httpBody)
        
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            guard error == nil else {
                //print(error?.localizedDescription as String!)
                return
            }
            
            guard let _ = data else {
                return
            }
            
            DispatchQueue.main.async() {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 201 {
                    OperationQueue.main.addOperation({
                        viewController.showToast(message:"Done")
                    })
                    
                    let location = httpResponse.allHeaderFields["Location"] as? String
                    print(location as Any)
                    
                    OperationQueue.main.addOperation({
                        viewController.dismiss(animated: true){
                            viewController.showToast(message:"Done")
                        }
                    })
                    
                }else{
                    //print("Some thing went wrong");
                    //print("/n\(String(describing: response?.description))")
                }
            }
        }
        task.resume()
    }
    
    func postNewService() {
        
        //create the url with NSURL
        let url = URL(string: "\(newServiceURL)/gw/odata/SAP/POST_NEWSERVICE;v=1/$metadata")! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        let request = URLRequest(url: url)
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            //print(data.description)
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
   
    /** using SAP ios SDK **/
    //MARK: SAP SDK
    
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private var zdmnewservicesrvEntities: ZDM<OnlineODataProvider> {
        return self.appDelegate.zdmnewservicesrvEntities
    }
//    private var newMeterSP: ZDMNEWSERVICESRVEntities<OfflineODataProvider> {
//        return LocalStoreNewMeter.returnSP()
//    }
    private let logger = Logger.shared(named: "ZdmNewServiceCollMasterViewControllerLogger")

    private var validity = [String: Bool]()
    
    static var address: String?
    static var meterNumber: String?
    static var meterSizeCode:String?
    static var numberOfDials: String?
    static var enterMeterNumber:String?
    static var currentMRU:String?
    
    private var _entity: ZDMZdmNewService?
    var entity: ZDMZdmNewService {
        get {
            if self._entity == nil {
                self._entity = ZDMZdmNewService(withDefaults: true) //self.createEntityWithDefaultValues()
                self._entity?.equnr = NewMeter_SAP_NewService.meterNumber
                self._entity?.matnr = NewMeter_SAP_NewService.meterSizeCode
                self._entity?.ableinh = NewMeter_SAP_NewService.currentMRU
                self._entity?.vZwstand = NewMeter_SAP_NewService.enterMeterNumber
                self._entity?.address = NewMeter_SAP_NewService.address
                self._entity?.dials = NewMeter_SAP_NewService.numberOfDials
            }
            return self._entity!
        }
        set {
            self._entity = newValue
        }
    }

    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")

    @objc func createEntity(viewController: NewMeterViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        logger.logLevel = .debug
        // self.view.endEditing(true)
        
        LogHelper.shared?.info("Creating entity in backend.", loggerObject: self.logger)
        
        let headers = HTTPHeaders()
        headers.setHeader(withName:"OfflineOData.RemoveAfterUpload", value:"true" )
        let newMeterLP = LocalStoreNewMeter.offlineLS
        
        //Use offline
        LocalStoreNewMeter.offlineSP?.createEntity(self.entity,headers:headers) { error in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if let error = error {
                self.logger.error("Create entry failed. Error: \(error)", error: error)
                return
            }
            self.logger.info("Create entry finished successfully.")
            newMeterLP?.uploadToDB()
            OperationQueue.main.addOperation({
                viewController.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                }
            })
        }
        
         LogHelper.shared?.info("Create entity finished successfully.", loggerObject: self.logger)
    }
}

