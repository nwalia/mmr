//
//  PeriodicDashboardViewController+Search.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-29.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

extension PeriodicDashboardViewController: UISearchBarDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerOptions[row]
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(searchBar.text!)
        meterReadTableView.scrollToFirstRow()
        meterReadTableView.reloadData()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchBar.text?.isEmpty ?? true
    }
    
    fileprivate func characterMatch(_ read: (MeterRead), text: String) -> Bool {
        if text.count == 0 { return true }
        return read.addressLine1.lowercased().contains(text.lowercased())
            || read.addressLine2.lowercased().contains(text.lowercased())
            || read.firstName.lowercased().contains(text.lowercased())
            || read.lastName.lowercased().contains(text.lowercased())
            || read.locationCode.id.lowercased().contains(text.lowercased())
            || read.fullMeterId.lowercased().contains(text.lowercased())
            || (read.typeCode != nil && (read.typeCode?.lowercased().contains(text.lowercased()))!)
            || read.fullAddress.lowercased().contains(text.lowercased())
            || (read.typeCode != nil && ServiceRequestColorStyle(code: read.typeCode!).description.lowercased().contains(text.lowercased()))
            || read.jobID.lowercased().contains(text.lowercased())
            || read.gridNumber.lowercased().contains(text.lowercased())
            || read.routeId.lowercased().contains(text.lowercased())
            || read.troubleCodeString.lowercased().contains(text.lowercased())

    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredMeterReads = (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads.filter({(read: MeterRead) -> Bool in
            if characterMatch(read, text: searchText) && filter(read)
            {
                return true
            }
            else {
                return false
            }
        }))!
        
        meterReadTableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        // potential additional criteria for isFiltering: searchBar.isFocused
        let filtersActive = !searchBarIsEmpty() || filterAssigned
        editButton.isEnabled = !filtersActive
        return filtersActive
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
