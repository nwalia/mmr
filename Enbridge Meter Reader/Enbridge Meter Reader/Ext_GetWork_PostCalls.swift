//
//  Ext_GetWork_PostCalls.swift
//  Enbridge Meter Reader
//
//  Created by Jerry on 2018-12-03.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import SAPFoundation
import SAPOData
import SAPFiori
import SAPCommon
import SAPOfflineOData
import proxyclasses

extension GetWorkOrderFromBackEnd_SAP_Click {
    func postACKStatus()  {
        if !uploading {
            uploadSucceed = true
            uploading = true
            uploadCounter = 0
            totalUpload = 0
            RouteKeyList.removeAll()
            MRUList.removeAll()
            GetWorkOrderFromBackEnd_SAP_Click.byteSent = 0
            for (key, route) in Model.sharedInstance.routes {
                var uploading = false
                if let type = route.type {
                    switch type {
                    case .OFFCYCLE:
                        uploading = postACKOffCycle()
                    case .OUTCARD:
                        uploading = postACKOutcard(cycleNo:route.cycleNo!)
                    case .PERIODIC:
                        uploading = postACKPeriodic()
                    }
                    if uploading {
                        RouteKeyList.insert(key)
                    }
                }           
            }
            
            //Upload ACK for both outcard/periodic, note: ACK for offcycle already being uploaded in postACKOffCycle
            self.postStatus()
        }
    }
    
    private func postACKPeriodic() -> Bool {
        let args = LocalStoreManager.ShareInstance?.returnOperationWithStatus(type: GlobalConstants.PERIODIC_ROUTE_ID, status: "DISP")
        
        if let optSet = args?.1 {
            if optSet.count > 0 {
                // var mruString = ""
                optSet.forEach{ [weak self] operation in
                    if let mru = operation.object?.task?.mmrMru {
                        self?.MRUList.insert(mru)
                    }
                }
                return true
            }
            return false
        }
        return false
        
    }
    
    private func postACKOutcard(cycleNo:String) -> Bool {
        let args = LocalStoreManager.ShareInstance?.returnOperationWithStatus(type: GlobalConstants.OUTCARD_ROUTE_ID, status: "DISP")
        
        if let optSet = args?.1 {
            if optSet.count > 0 && optSet.first!.object?.task?.mmrCycleNumber == cycleNo{
                optSet.forEach{ [weak self] operation in
                    if let mru = operation.object?.task?.mmrMru {
                        self?.MRUList.insert(mru)
                    }
                }
                return true
            }
            return false
        }
        return false
    }
    
    private func postACKOffCycle() -> Bool{
        let args = LocalStoreManager.ShareInstance?.returnOperationWithStatus(type: GlobalConstants.OFFCYCLE_ROUTE_ID, status: "DISP")
        print("STATUS UPDATE- <<<<ACK>>>")
        var res = false
        args?.1.forEach{ [weak self] operation in
            do {
                operation.action = "Update"
                operation.object?.task?.status = "ACK"
                _ = Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.meterReads.filter{ meterRead in
                    if (meterRead.jobID == operation.callID) {
                        meterRead.mmrStatus = "ACK"
                        return true
                    }else {
                        return false
                    }
                }
                let result = try LocalStoreManager.ShareInstance?.updateOperation(type: GlobalConstants.OFFCYCLE_ROUTE_ID, operation: operation)
                
                if result?.0 == 200 {
                    LogHelper.shared?.info("Change Status to ACK successfully with work ID:" + operation.callID!, loggerObject: (self?.logger)!)
                }else{
                    LogHelper.shared?.error("Change Status to ACK failed with work ID:" + operation.callID!, loggerObject: (self?.logger)!)
                }
            } catch {
                print("Update Operation Failed: \(String(describing: self?.error))")
                LogHelper.shared?.error("Change Status to ACK failed with work ID:" + operation.callID!, loggerObject: (self?.logger)!)
            }
            res = true
        }
        if ((LocalStoreManager.ShareInstance?.checkOffcycleStatus().1)!){
            return self.changeStatusToENROUTE() || res
        }else{
            return res
        }
    }
    
    private func changeStatusToENROUTE() -> Bool{
        let args = LocalStoreManager.ShareInstance?.returnOperationWithStatus(type: GlobalConstants.OFFCYCLE_ROUTE_ID, status: "ACK")
        var res = false
        args?.1.forEach{ [weak self] operation in
            do {
                operation.action = "UPDATE"
                operation.object?.task?.status = "EN-ROUTE"
                
                _ = Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.meterReads.filter { meterRead in
                    if (meterRead.jobID == operation.callID) {
                        meterRead.mmrStatus = "EN-ROUTE"
                        return true
                    }else {
                        return false
                    }
                }
                
                print("STATUS UPDATE- <<<<EN-ROUTE>>>")
                let result = try LocalStoreManager.ShareInstance?.updateOperation(type: GlobalConstants.OFFCYCLE_ROUTE_ID, operation: operation)
                
                if result?.0 == 200 {
                    LogHelper.shared?.info("Change Status to ON-ROUTE successfully with work ID:" + operation.callID!, loggerObject: (self?.logger)!)
                }else{
                    LogHelper.shared?.error("Change Status to ON-ROUTE failed with work ID:" + operation.callID!, loggerObject: (self?.logger)!)
                }
            } catch {
                print("Update Operation Failed: \(String(describing: self?.error))")
                LogHelper.shared?.error("Change Status to ON-ROUTE failed with work ID:" + operation.callID!, loggerObject: (self?.logger)!)
            }
            res = true
        }
        
        return res
    }
    
    private func insertMRUIntoTable(){
        do{
            if !MRUList.isEmpty {
                let entry = MassUpdateTasks()
                let engineer = Engineer()
                let mruList = MRUs()
                let task = Task()
                
                engineer.id = Utilities.sharedInstance.userName
                
                mruList.mru = MRUList.joined(separator: ",")
                
                task.status = "ACK"
                task.mrUs = mruList
                     
                entry.engineer = engineer
                entry.task = task
                entry.mru = MRUList.joined(separator: ",")
                
                try LocalStoreUpdateStatusServ.shareInstance!.createEntryInTable(entry: entry)
            }
        }catch {
            print(error)
        }
    }
    
    private func setUploadFailed(){
        for key in RouteKeyList {
            if let route = Model.sharedInstance.routes[key] {
                route.blockCell = true
                route.uploadText = GlobalConstants.ROUTE_UPLOADING_FAILED_MESSAGE
                route.uploading = false
            }
        }
    }
    
    private func setUploadSucced(){
        for key in RouteKeyList {
            if let route = Model.sharedInstance.routes[key] {
                route.blockCell = false
            }
        }
    }
    
    private func postStatus() {
        //block offcycle cell
        var submitOffcycle = false
        var submitOutPer = false
        
        
        for key in RouteKeyList {
            Model.sharedInstance.routes[key]?.uploading = true
            Model.sharedInstance.routes[key]?.blockCell = true
            Model.sharedInstance.routes[key]?.uploadText = GlobalConstants.ROUTE_UPLOADING_MESSAGE + "(0Bytes)"
            
            if key == GlobalConstants.OFFCYCLE_ROUTE_ID {
                submitOffcycle = true
            }else{
                submitOutPer = true
            }
        }
        
        if submitOffcycle && submitOutPer {
            totalUpload = 2
            LocalStoreManager.ShareInstance?.submitOffCycleRoute{error in
                if error != nil {
                    self.uploadSucceed = false
                    Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.blockCell = true
                    Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.uploadText = GlobalConstants.ROUTE_UPLOADING_FAILED_MESSAGE
                }else {
                    Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.blockCell = false
                }
                 Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.uploading = false
                 self.uploadCounter = self.uploadCounter + 1
            }
            
            // Submitting perioidc&outcard
            insertMRUIntoTable()
            LocalStoreUpdateStatusServ.shareInstance?.uploadToDB(){ error in
                if error != nil {
                    self.uploadSucceed = false
                    self.setUploadFailed()
                }else{
                    self.setUploadSucced()
                }
                self.uploadCounter = self.uploadCounter + 1
            }
            
        }else if submitOffcycle {
            totalUpload = 1
            LocalStoreManager.ShareInstance?.submitOffCycleRoute{ error in
                if error != nil {
                    self.uploadSucceed = false
                    Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.blockCell = true
                    Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.uploadText = GlobalConstants.ROUTE_UPLOADING_FAILED_MESSAGE
                }else {
                    Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.blockCell = false
                }
                Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.uploading = false
                self.uploadCounter = self.uploadCounter + 1
            }
            
        }else if submitOutPer {
            //loop RouteKeyList, block the cell for outcar/periodic new service, unlock the cell after submission.
            totalUpload = 1
            insertMRUIntoTable()
            LocalStoreUpdateStatusServ.shareInstance?.uploadToDB(){ error in
                if error != nil {
                    self.uploadSucceed = false
                    self.setUploadFailed()
                }else{
                    self.setUploadSucced()
                }
                self.uploadCounter = self.uploadCounter + 1
            }
        }else{
            totalUpload = 0
            uploadCounter = 0
        }
     
        //refresh the table view
        OperationQueue.main.addOperation({
            if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                    selectVC.tableView.reloadData()
                }
            }
        })
        
    }
}
