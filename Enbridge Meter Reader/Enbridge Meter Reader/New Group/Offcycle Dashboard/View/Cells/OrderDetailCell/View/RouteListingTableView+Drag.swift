//
//  RouteListingTableView+Drag.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-09.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

extension OrderDetailCell: UITableViewDragDelegate {
    // MARK: - UITableViewDragDelegate
    
    /**
     The `tableView(_:itemsForBeginning:at:)` method is the essential method
     to implement for allowing dragging from a table.
     */
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        return Model.sharedInstance.dragItems(for: indexPath)
    }
    
//    func tableView(_ tableView: UITableView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
//        return model.dragItems(for: indexPath)
//    }
}
