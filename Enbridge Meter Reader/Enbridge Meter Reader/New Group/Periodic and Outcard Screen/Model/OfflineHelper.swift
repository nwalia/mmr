//
//  OfflineHelper.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-07-05.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation


class OfflineHelper {
    func uploadtoOfflineStore(currentMeter: MeterRead) {
        if AppDelegate.hasMockData { return }
        do {
            var res: (Int,String)? = nil
        switch true {
        case currentMeter.isOutCard:
            let operation = MeterRead.mapTo_Operation(meterRead: currentMeter, withCompletionCode: "")
            res = try LocalStoreManager.ShareInstance?.updateOperation(type:RouteType.OUTCARD.rawValue,operation:operation)
            if res != nil && res?.0 == 200 {
                LocalStoreManager.ShareInstance?.submitOutcardRoute{ Error in
                    //log
                }
            }else{
                //log
            }
        case currentMeter.isOffcycle:
            let operation = MeterRead.mapTo_Operation(meterRead: currentMeter, withCompletionCode: String(describing: currentMeter.completionCode!.number ?? 0))
            res = try LocalStoreManager.ShareInstance?.updateOperation(type:RouteType.OFFCYCLE.rawValue,operation:operation)
            if res != nil && res?.0 == 200 {
                LocalStoreManager.ShareInstance?.submitOffCycleRoute{ Error in
                    //log
                }
            }
            else{
                //
            }
        case currentMeter.isPeriodic:
            let operation = MeterRead.mapTo_Operation(meterRead: currentMeter, withCompletionCode: "")
            res = try LocalStoreManager.ShareInstance?.updateOperation(type:RouteType.PERIODIC.rawValue,operation:operation)
            if res != nil && res?.0 == 200 {
                LocalStoreManager.ShareInstance?.submitPeriodicRoute(){ Error in
                    //log, maybe show pop
                }
            }else{
                //log
            }
        default:
            res = (0,"Failed to update local store in outcard/periodic view")
        }
        } catch {
            print (error)
        }
    }
}
