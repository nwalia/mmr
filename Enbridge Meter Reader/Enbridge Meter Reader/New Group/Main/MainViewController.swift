//
//  ViewController.swift
//  Enbridge Meter Reader
//
//  Created by Johann Wentzel on 2017-11-29.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit
import MapKit

class MainViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var skipCodeButton: UIButton!
    @IBOutlet weak var troubleCodeButton: UIButton!
    var hasEnteredTroubleCodes: Bool!
    let locationManager = CLLocationManager()
    var selectedSkipCode: SkipCode?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // seek authorization
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        // Do any additional setup after loading the view, typically from a nib.
        navigationController?.navigationBar.tintColor = UIColor(red:1, green:0.72, blue:0.11, alpha:1)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        
        hasEnteredTroubleCodes = false
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? SelectSkipCodeViewController {
            destinationVC.confirmedCode = selectedSkipCode
        }
    }

    @IBAction func troubleCodeButtonClicked(_ sender: UIButton) {
        if (hasEnteredTroubleCodes){
            let alert = UIAlertController(title: "Make a Mistake?", message: "You can either go back to update a photo and edit your trouble message or remove the code entirely.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Remove", style: .default, handler: {(alert: UIAlertAction!) in self.removeTroubleCodes()}))
            alert.addAction(UIAlertAction(title: "Update", style: .default, handler: {(alert: UIAlertAction!) in self.updateTroubleCodes()}))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            updateTroubleCodes()
        }
    }
    
    @objc func removeTroubleCodes(){
        hasEnteredTroubleCodes = false
        troubleCodeButton.titleLabel?.text = "Enter Trouble Code"
    }
    
    @objc func updateTroubleCodes(){
        performSegue(withIdentifier: "toTroubleCodeScreen", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func unwindToHome(segue: UIStoryboardSegue){
        if let sourceVC = segue.source as? SelectTroubleCodeViewController {
            troubleCodeButton.titleLabel?.text = sourceVC.formattedCodeString ?? "Select Trouble Code"
            hasEnteredTroubleCodes = true
        }
        
        if let sourceVC = segue.source as? SelectSkipCodeViewController {
            selectedSkipCode = sourceVC.selectedCode
            skipCodeButton.titleLabel?.text = sourceVC.formattedCodeString ?? "Select Skip Code"
        }
    }
    
    @IBAction func openMessagesCentre (_ sender: UIButton) {
        //let splitVC = SplitViewController()
        //self.present(splitVC, animated: true, completion: nil)
        
        let split = UIStoryboard(name: "MessagingCentre", bundle: nil).instantiateViewController(withIdentifier: "Split") as? UISplitViewController
        split?.preferredDisplayMode = UISplitViewController.DisplayMode.allVisible
        view.window?.rootViewController = split!
        //self.show(split!, sender: self)

    }
    @IBAction func launchMainNav(_ sender: UIButton) {
        let nav = UIStoryboard(name: "MainNav", bundle: nil).instantiateInitialViewController() as? UITabBarController
        
        view.window?.rootViewController = nav
    }
}


