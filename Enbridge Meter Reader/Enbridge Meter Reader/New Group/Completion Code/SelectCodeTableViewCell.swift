//
//  SelectCodeTableViewCell.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2017-11-30.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit

class SelectCodeTableViewCell: UITableViewCell {
    

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var iconContainerView: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
