//
//  OrderDetail.swift
//  MapView
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2017-12-22.
//  Copyright © 2017 Deshpande, Ankush (CA - Toronto). All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class Visit: ReusableView {
    
    // MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLine1Label: UILabel!
    @IBOutlet weak var addressLine2Label: UILabel!
    @IBOutlet weak var meterNumberLabel: UILabel!
    @IBOutlet weak var locationCodeLabel: UILabel!
    @IBOutlet weak var estimationLabel: UILabel!
    @IBOutlet weak var typeViewBackground: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var jobIDLabel: UILabel!
    @IBOutlet weak var timeIcon: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var keyIconStackView: UIStackView!
    @IBOutlet weak var instructionIconStackView: UIStackView!
    
    // MARK: - Vars
    
    var name: String?
    var addressLine1: String?
    var addressLine2: String?
    var houseNumber: String?
    var meterNumber: String?
    var newMeterNumber: String?
    var locationCode: String?
    var estimation: String?
    var jobID: String?
    var gridNumber: String?
    var readType: String?
    var notificationCode: String?
    var redLockType: String?
    
    func lateInit(read: MeterRead){
        self.name = read.nameLastFirstComma
        self.addressLine1 = read.fullAddress
        self.addressLine2 = read.addressLine2
        self.houseNumber = read.houseNumber
        self.meterNumber = read.fullMeterId
        self.newMeterNumber = read.newFullMeterId
        self.locationCode = read.locationCode.id
        self.estimation = "\(read.skips ?? 0)"
        self.jobID = read.jobID
        self.gridNumber = read.gridNumber
        self.readType = read.typeCode ?? ""
        self.notificationCode = read.mmrNotificationCode ?? ""
        self.redLockType = read.mmrRedLockType ?? ""
        
        timeIcon.isHidden = false
        timeLabel.isHidden = false
        
        switch read.appointmentTime {
        case "9AM-12PM"?:
            self.timeIcon.backgroundColor = UIColor.enbridgeYellow
            self.timeLabel.text = read.appointmentTime
        case "12PM-4PM"?:
            self.timeIcon.backgroundColor = UIColor.clementine
            self.timeLabel.text = read.appointmentTime
        case "4PM-9PM"?:
            self.timeIcon.backgroundColor = UIColor.dusk
            self.timeLabel.text = read.appointmentTime
        default:
            timeIcon.isHidden = true
            timeLabel.isHidden = true
        }
        
        keyIconStackView.isHidden = read.keyNumber == nil || (read.keyNumber ?? "").isEmpty
        instructionIconStackView.isHidden = read.specialInstructions == nil || read.specialInstructions == ""
        
        updateUI()
        
        // When work order is cancelled, square should read "WORK ORDER CANCELLED"
        if read.cancelled {
            self.typeLabel.text = "WORK ORDER"
            self.jobIDLabel.text = "CANCELLED"
        }
        
        // When work order is cancelled, square should read "WORK ORDER CANCELLED"
        if read.unscheduled {
            self.typeLabel.text = "WORK ORDER"
            self.jobIDLabel.text = "UNSCHEDULED"
        }
        
        if read.type == "Periodic" || read.type == "Outcard" {
            estimationLabel.isHidden = false
        }else{
            estimationLabel.isHidden = true
        }
        
    }
    
    private func updateUI(){
        
        if nameLabel == nil {
            //print("Setting while nil")
            return
        }
        
        nameLabel.text = name ?? "No name"
        addressLine1Label.text = addressLine1 ?? "No address"
        addressLine2Label.text = addressLine2 ?? "No address"
        locationCodeLabel.text = "LOC: \(locationCode ?? "None")"
        estimationLabel.text = "Est: \(estimation ?? "")"
        
        if newMeterNumber != nil && newMeterNumber != "" {
            meterNumberLabel.text = newMeterNumber 
        } else {
            meterNumberLabel.text = meterNumber ?? "No number"
        }
        

        
        let style = ServiceRequestColorStyle(code: readType!)
        jobIDLabel.text = "#\(jobID ?? "000000") / \(gridNumber ?? "C05")"
        typeLabel.text = "\(readType ?? "") - \(notificationCode ?? "")\((redLockType?.isEmpty)! ? "" : " - \(String(describing: redLockType ?? ""))" )"
        typeViewBackground.backgroundColor = style.backgroundColor
        typeViewBackground.borderColor = style.borderColor
        typeViewBackground.borderWidth = CGFloat(style.borderWidth)
        jobIDLabel.textColor = style.textColor
        typeLabel.textColor = style.textColor
        
        
        
        
    }
}
