//
//  MasterViewController.swift
//  Enbridge Meter Reader
//
//  Created by Abramsky, Lauren (CA - Toronto) on 2018-01-10.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit

var messages: [Message] = [] //Declared outside the class so messages can be added from AppDelegate

class MasterViewController: UITableViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
                
        // Make messages, temporary until integrations are ready
        // messages = makeMessages()
        
        // Set navigation bar styling
        navigationController?.navigationBar.tintColor = UIColor(red:1, green:0.72, blue:0.11, alpha:1)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        
        tableView.rowHeight = 106
        tableView.delegate = self
        tableView.dataSource = self
        
        // Set up notification observer so AppDelegate can reload messages table when new message comes in
        NotificationCenter.default.addObserver(self, selector: #selector(loadTable), name: NSNotification.Name(rawValue: "newWork"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadTable), name: NSNotification.Name(rawValue: "reloadTables"), object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Update messages badge count and icon
        updateBadgeCount()
        appDelegate.updateMessagesBadge()
    }
    
    // Function called by notification in AppDelegate to reload messages table when new message comes in
    @objc func loadTable() {
        tableView.reloadData()
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "openMessage" {
            if let cell = sender as? TableCellViewController {
                let indexPath = self.tableView.indexPath(for: cell)!
                let detailVC = segue.destination as! DetailViewController
                detailVC.message = cell.message
                messages[indexPath.row].opened = true
            }
        }
    }
        
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // Set number of rows
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableCellViewController
        let messageForCell = messages[indexPath.row]
        
        cell.message = messageForCell
        
        if messageForCell.opened {
            cell.notificationCircle.isHidden = true
        } else {
            cell.notificationCircle.isHidden = false
        }
        
        // Force data population in message cells
        cell.populateCell()
        
        return cell
    }
    
    // Update count for messages tab and app badge
    func updateBadgeCount() {
        
        var unopenedMessagesCount = 0
        for message in messages {
            if message.opened == false {
                unopenedMessagesCount += 1
            }
        }
        GlobalVariables.badgeCount = unopenedMessagesCount
        
        // Update app badget icon
        UIApplication.shared.applicationIconBadgeNumber = GlobalVariables.badgeCount
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Update messages tab and app badge count and icon
        updateBadgeCount()
        appDelegate.updateMessagesBadge()
        
    }
    
    func makeMessages() -> [Message] {
        return [
            Message(from: "Michael Scott", subject: "Threat Level Midnight", date: "15 January 2018", time: "9:00 AM", body: "I enjoy having breakfast in bed. I like waking up to the smell of bacon, sue me. And since I don't have a butler, I have to do it myself. So, most nights before I go to bed, I will lay six strips of bacon out on my George Foreman Grill. Then I go to sleep. When I wake up, I plug in the grill, I go back to sleep again. Then I wake up to the smell of crackling bacon. It is delicious, it's good for me. It's the perfect way to start the day. Today I got up, I stepped onto the grill and it clamped down on my foot... that's it. I don't see what's so hard to believe about that.", opened: false),
            Message(from: "Michael Scott", subject: "Little Kid Lover", date: "15 January 2018", time: "11:11 AM", body: "Yes, it is true. I, Michael Scott, am signing up with an online dating service. Thousands of people have done it, and I am going to do it. I need a username. And... I have a great one. Little kid lover. That way, people will know exactly where my priorities are at.", opened: false),
            Message(from: "Dwight Schrute", subject: "My Perfect Crime", date: "15 January 2018", time: "3:00 PM", body: "What is my perfect crime? I break into Tiffany's at midnight. Do I go for the vault? No, I go for the chandelier. It's priceless. As I'm taking it down, a woman catches me. She tells me to stop. It's her father's business. She's Tiffany. I say no. We make love all night. In the morning, the cops come and I escape in one of their uniforms. I tell her to meet me in Mexico, but I go to Canada. I don't trust her. Besides, I like the cold. Thirty years later, I get a postcard. I have a son and he's the chief of police. This is where the story gets interesting. I tell Tiffany to meet me in Paris by the Trocadero. She's been waiting for me all these years. She's never taken another lover. I don't care. I don't show up. I go to Berlin. That's where I stashed the chandelier.", opened: false)
        ]
    }

}
