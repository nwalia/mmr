//
//  ExchangeMeterViewController.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2017-12-05.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit

class ExchangeMeterViewController: UIViewController, MeterReadBarDelegate {
    

    @IBOutlet weak var meterNumberField: ValidatedTextField!
    @IBOutlet weak var meterSizeCodeField: ValidatedTextField!
    @IBOutlet weak var numberOfDialsField: ValidatedTextField!
    @IBOutlet weak var submitNewMeterButton: UIButton!
    @IBOutlet weak var enterMeterNumber: MeterReadBarView!
    
    var currentMeterRead: MeterRead?
    var returnMeterNumber: String?
    var returnMeterSizeCode: String?
    var returnNumberOfDials: String?
    var returnMeterReadEntry: Int?
    var meterReadEntryValid: Bool = false
    var submitButtonEnabled: Bool!
    var ocrReading: Int = 0
    
    // Image containers
    var meterImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enterMeterNumber?.delegate = self as MeterReadBarDelegate
        enterMeterNumber?.awakeFromNib()
        enterMeterNumber?.setUp(enterMeterNumber, "firstMeterReadBar", 6)
        
        // If came from camera, ocrReading will exist, populate with ocrReading
        if ocrReading != 0 {
            enterMeterNumber?.populateWithValues(ocrReading)
        }
        
        submitNewMeterButton.layer.cornerRadius = 10
        submitNewMeterButton.layer.masksToBounds = true
        submitNewMeterButton.setTitleColor(UIColor(red:0.2, green:0.2, blue:0.2, alpha:1), for: .normal)
        submitNewMeterButton.setTitleColor(UIColor.gray, for: .disabled)
        
        submitButtonEnabled = false
                
        meterNumberField.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        meterSizeCodeField.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        numberOfDialsField.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkAllValid()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkAllValid(){
        // Check if all the fields are valid. If so, enable the submit button.
        
        if meterNumberField.getValidity()
            && meterSizeCodeField.getValidity()
            && numberOfDialsField.getValidity()
            && meterReadEntryValid
        {
            // enable button
            if !(submitButtonEnabled) {
                submitButtonEnabled = true
                submitNewMeterButton.isEnabled = true
                submitNewMeterButton.backgroundColor = UIColor(red:1, green:0.72, blue:0.11, alpha:1)
            }
        }
        else {
            // disable button
            submitButtonEnabled = false
            submitNewMeterButton.isEnabled = false
            submitNewMeterButton.backgroundColor = UIColor(red:1, green:0.8, blue:0.38, alpha:1)
        }
        
    }
    
    @IBAction func didPressSubmitNewMeter(_ sender: UIButton) {
        
        // Save all new fields locally for segueing to other screens
        returnMeterNumber = meterNumberField.text
        returnMeterSizeCode = meterSizeCodeField.text
        returnNumberOfDials = numberOfDialsField.text
        returnMeterReadEntry = enterMeterNumber.getMeterReadEntry() ?? 0
        
        // Save all new fields to current meter data for post service
        Utilities.currentMeter.newMeterNumber = returnMeterNumber
        Utilities.currentMeter.mmrReadCode = "N"
        Utilities.currentMeter.updateReadDate()
        Utilities.currentMeter.newSizeCode = returnMeterSizeCode
        if (4...6).contains(Int(returnNumberOfDials ?? "0") as Int? ?? 0) {
            // enable button
            if !(submitButtonEnabled){
                submitButtonEnabled = true
                submitNewMeterButton.isEnabled = true
                submitNewMeterButton.backgroundColor = GlobalConstants.YELLOW
            }
            Utilities.currentMeter.newMeterNumberOfDials = Int(returnNumberOfDials!)
            if let typeCode = Utilities.currentMeter.typeCode {
                if typeCode == "ZB" {
                    Utilities.currentMeter.postReading = enterMeterNumber.getMeterReadEntry()
                }else {
                    Utilities.currentMeter.newMeterRead = enterMeterNumber.getMeterReadEntryString()
                }
            }
            Utilities.showAlertOn(self, segueName: "unwindToHome")
        }else {
            let alertController = UIAlertController(title:"Invalid Input", message: "Number of dials should be between 4-6", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default))
            OperationQueue.main.addOperation({
                // Present the alertController
                self.present(alertController, animated: true, completion: nil)
            })
        }
    }
    
    //MARK: - Navigation
    @IBAction func unwindToHome(segue: UIStoryboardSegue){
//        if let sourceVC = segue.source as? CameraCaptureViewController{
//            print("Unwound from camera")
//            if let imageFromView = sourceVC.image {
//                print("assigned image to slot 1")
//                meterImage = imageFromView
//            }
//            print("came from camera")
//        }
        if let sourceVC = segue.source as? OCRViewController {
            if let val_Read = sourceVC.valRead {
                if val_Read > -1 {
                    enterMeterNumber.clearMeterReadEntry()
                    ocrReading = val_Read
                    enterMeterNumber.populateWithValues(val_Read)
                    enterMeterNumber.validateFields()
                    barDidValidate(enterMeterNumber.meterReadBarValid, "")
                }
            }
        }
        
        
    }
    
    func barDidValidate(_ isValid: Bool, _ title: String) {
        if isValid {
            meterReadEntryValid = true
        } else {
            meterReadEntryValid = false
        }
        checkAllValid()
    }
    
    func tappedCameraButton(_ title: String) {
        performSegue(withIdentifier: "toOCR", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toOCR" {
            let destinationVC = segue.destination as? OCRViewController
            destinationVC?.lowValue = currentMeterRead?.lowParameter
            destinationVC?.highValue = currentMeterRead?.highParameter
            destinationVC?.numDigits = (self.numberOfDialsField.text?.isEmpty)! ?  currentMeterRead?.numberOfDials : Int(self.numberOfDialsField.text ?? "4")
        }
    }
    
    @IBAction func didPressCancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    

}

