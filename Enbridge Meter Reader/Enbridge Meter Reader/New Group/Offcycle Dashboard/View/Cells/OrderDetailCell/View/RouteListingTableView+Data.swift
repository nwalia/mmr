//
//  ORouteListingTableView+Data.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-09.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

extension OrderDetailCell: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering() ? filteredMeterReads!.count : (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads.count) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.routeListingTableView.dequeueReusableCell(withIdentifier: "LocationInformationCell") as! LocationInformationCell
        
        let readForCell = isFiltering() ? filteredMeterReads![indexPath.row] : Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID/*Model.selectedRouteKey*/]?.meterReads[indexPath.row]
        
        guard readForCell != nil else { return UITableViewCell() }

        
        cell.selectionStyle = .none
        // save color to re-assign on de-selection
        cell.defaultColor = (readForCell!.status == .completed || readForCell!.status == .pending) ? GlobalConstants.COMPLETED_CELL : .white
        
        // Populate cell with values
        
        cell.populate(read: readForCell!)
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        Model.sharedInstance.moveItem(at: sourceIndexPath.row, to: destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? LocationInformationCell
        let readForCell = isFiltering() ? filteredMeterReads![indexPath.row] : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads[indexPath.row]
        toggleSelected(selected: true, read: readForCell!, cell: cell!)
        OrderDetailCell.delegate?.didSelectNewMeter(readForCell!)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? LocationInformationCell else {return}
        let readForCell = isFiltering() ? filteredMeterReads![indexPath.row] : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads[indexPath.row]
        toggleSelected(selected: false, read: readForCell!, cell: cell)
    }
        
    func toggleSelected(selected: Bool, read: MeterRead, cell: LocationInformationCell){
        if selected {
            read.selected = true

        }
        else {
            read.selected = false
            cell.backgroundColor = (read.status == .completed || read.status == .pending) ? GlobalConstants.COMPLETED_CELL : .white

            cell.visit.backgroundColor = (read.status == .completed || read.status == .pending) ? GlobalConstants.COMPLETED_CELL : .white
        }
        
    }
}
